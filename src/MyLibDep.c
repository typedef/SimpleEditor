#define LINUX_PLATFORM
#define ENGINE_DEBUG
#define SSL_IMPLEMENTATION
#include "MyLibDep.h"

void
_vbreak(const char* msg, const char* file, int line, const char* condition)
{
    printf("[ASSERT] %s:%d condition: %s is false!\n", file, line, condition);
    volatile int* cptr = NULL;
    *cptr = 0;
}
