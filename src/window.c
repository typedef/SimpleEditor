#include <X11/X.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
//#include <X11/Xlib.h>
//#include <X11/Xutil.h> // for XSizeHints

#define SWL_X11_BACKEND 1
#define SWL_IMPLEMENTATION
#include "SimpleWindowLibrary.h"

typedef int32_t i32;
typedef uint64_t u64;

/*
  TODO:
  1. Impl xkey conversion to ssw (swl) keys
  2. Create good short name for lib

 */

/*
  NOTE: Needs to have simple api as front
  as back we need Xlib, Win64 Api, glfw (as an option for Wayland)
*/

#include <dirent.h>
#include <string.h>

static i32 gAppRun = 1;

void
my_event_callback(SwlEvent* pEvent)
{
    if (pEvent->Type == SwlEventType_MousePress
	|| pEvent->Type == SwlEventType_MouseRelease)
    {
	SwlMousePressEvent* pMouseKey = (SwlMousePressEvent*) pEvent;
	printf("IsHandled: %d Type: %d Key: %d\n",
	       pEvent->IsHandled, pEvent->Type, pMouseKey->Key);
    }

    if (pEvent->Type == SwlEventType_KeyPress)
    {
	SwlKeyPressEvent* pKeyPress = (SwlKeyPressEvent*) pEvent;
	if (pKeyPress->Key == SwlKey_Escape)
	{
	    gAppRun = 0;
	}

	if (pKeyPress->Key == SwlKey_W)
	{
	    printf("W\n");
	}
    }
}

i32
main()
{

#if 1

    swl_init((SwlInit) {
	    .MemoryAllocate = malloc,
	    .MemoryFree = free,
	});

    SwlWindowSettings set = {
	.pName = "Swl Window test",
	.Position = {.X = 150, .Y = 170},
	.Size = {
	    .Width = 600,
	    .Height = 600,
	    .MinHeight = 150,
	    .MinWidth = 150
	},
    };

    SwlWindow swlWindow = swl_window_create(set);
    swl_window_set_background(&swlWindow, 10,10,10);
    swl_window_set_event_call(&swlWindow, my_event_callback);

    while (gAppRun)
    {
	swl_update(&swlWindow);
    }


#else

    const char* pDiplayName = "Display 0";
    Display* pDisplay = XOpenDisplay(NULL);
    if (pDisplay == NULL)
    {
	printf("Display creation error!\n");
	return 1;
    }

    i32 defaultScreen = DefaultScreen(pDisplay);
    Window windowRoot = RootWindow(pDisplay, defaultScreen);
    GC graphicsContext = DefaultGC(pDisplay, defaultScreen);

    {
	// get information about the display
	i32 width = DisplayWidth(pDisplay, defaultScreen);
	i32 height = DisplayHeight(pDisplay, defaultScreen);
	// useless information :/
	printf("Display[%d]: %s, w:%d h:%d\n", defaultScreen, DisplayString(pDisplay), width, height);
    }

    // todo: extend in future
    u64 attribute_mask = CWEventMask | CWBackPixel | CWBorderPixel;
    u64 event_mask = ExposureMask
	| ButtonPressMask | ButtonReleaseMask
	| KeyPressMask | KeyReleaseMask
	| FocusChangeMask
	| EnterWindowMask | LeaveWindowMask
	| VisibilityChangeMask /*It just maximized*/
	| StructureNotifyMask
	/* | ResizeRedirectMask  do *NOT* handle it*/
	| StructureNotifyMask | PropertyChangeMask | PointerMotionMask /* for minify, maximize-hor, max-ver, */
	;

    XSetWindowAttributes attributes = {
	.event_mask = event_mask,
	.border_pixel = BlackPixel(pDisplay, defaultScreen),
	.background_pixel = WhitePixel(pDisplay, defaultScreen),
    };

    Visual* pVisual = CopyFromParent;
    i32 someWeirdFlagFromX = InputOutput;
    i32 x = 50, y = 50, width = 500, height = 500, borderWidth = 120;
    Window window = XCreateWindow(
	pDisplay, windowRoot, x, y, width, height,
	borderWidth, CopyFromParent, someWeirdFlagFromX, pVisual,
	attribute_mask, &attributes);

    XSizeHints sizeHints = {
	.x = x,
	.y = y,
	.width = width,
	.height = height,
	.min_width = 50,
	.min_height = 50,
	//.max_width = 1000,
	//.max_height = 1000,
	.base_width = width,
	.base_height = height,
	.flags = USPosition | USSize | PMinSize | /* PMaxSize | */ PBaseSize,
    };
    XSetWMNormalHints(pDisplay, window, &sizeHints);

    char* pWindowName = "Window Name X - Weird";
    char* pAppClass = "example_class";
    XClassHint xClassHints = {
	.res_class = pAppClass,
	.res_name = pWindowName,
    };
    XStoreName(pDisplay, window, pWindowName);
    XSetClassHint(pDisplay, window, &xClassHints);

    XWMHints windowManagerHints = {
	.flags = InputHint | StateHint,
	.initial_state = NormalState,
	.input = 1,
    };
    XSetWMHints(pDisplay, window, &windowManagerHints);

    // note: Diplay window
    XMapRaised(pDisplay, window);
    XFlush(pDisplay);

    if (1)
    {
	SwlInit swlInit = {
	    .MemoryAllocate = malloc,
	    .MemoryFree = free,
	};
	swl_init(swlInit);

	SwlX11Backend x11Back = {
	    .pDisplay = pDisplay,
	    .Window = window,
	};

	SwlWindow swlWindow = {
	    .pBackend = &x11Back
	};

	//swl_window_set_above(&swlWindow);
	//swl_window_set_maximized(&swlWindow);
	//swl_window_set_fullscreen(&swlWindow);
	//swl_window_set_wo_header(&swlWindow);

	/* XColor color = { */
	/*     .red   = 65535/2, // 1 - 65535 */
	/*     .green = 65535/4, // 0 - 0 */
	/*     .blue  = 65535/4, */
	/* }; */

	/* XAllocColor(pDisplay, DefaultColormap(pDisplay,0), &color); */
	/* XSetWindowBackground(pDisplay, window, color.pixel); */

	swl_window_set_background(&swlWindow, 255/2, 255/4, 255/4);

    }

    // note: Handle events
    i32 appRun = 1;


    // Additional vars
    Atom aFocused = XInternAtom(pDisplay, "_NET_WM_STATE_FOCUSED", 1);
    Atom aMaxH = XInternAtom(pDisplay, "_NET_WM_STATE_MAXIMIZED_HORZ", 1);
    Atom aMaxV = XInternAtom(pDisplay, "_NET_WM_STATE_MAXIMIZED_VERT", 1);
    Atom aHidden = XInternAtom(pDisplay, "_NET_WM_STATE_HIDDEN", 1);
    Atom aWindowManager = XInternAtom(pDisplay, "_NET_WM_STATE", 1);

    for ()
    {

    }

    while (appRun)
    {
	XEvent event, pevent;
	static i32 fi = 0;

	// mb delete
	XPending(pDisplay);

	while (XQLength(pDisplay))
	{
	    XNextEvent(pDisplay, &event);

	    // note: process event
	    if (event.type == Expose)
	    {
		/* static i32 sy = 50; */
		/* XDrawString(pDisplay, window, graphicsContext, 50, 50, "hello, world", 12); */
		/* sy += 50; */
		/* sy = sy % height; */

		static i32 sy = 50;
		XDrawString(pDisplay, window, graphicsContext, 50, sy, "hello, world", 12);
		sy += 50;
		sy = sy % height;


		XDrawLine(pDisplay, window, graphicsContext, 10, 10, 100, 10);
		XDrawPoint(pDisplay, window, graphicsContext, 30, 30);
		XRectangle rects[1] = {
		    [0] = {
			.x = 170,
			.y = 70,
			.width = 110,
			.height = 170
		    },
		};
		XDrawRectangles(pDisplay, window, graphicsContext, rects, 1);
		XFlush(pDisplay);
	    }
	    else if (event.type == KeyPress)
	    {
		printf("KeyPress: %d\n", event.xkey.keycode);

		if (event.xkey.keycode == 9)
		{
		    appRun = 0;
		    continue;
		}
		/* else if (event.xkey.keycode == 0xff80) */
		/* { */
		/*     printf("SPACE!\n"); */
		/* } */
	    }
	    else if (event.type == KeyRelease)
	    {
		// Release_A Press_A Release_A Press_A
		printf("KeyRelease: %d\n", event.xkey.keycode);

		XEvent nevent;
		XPeekEvent(pDisplay, &nevent);

		if (nevent.type == KeyPress
		    && nevent.xkey.time == event.xkey.time
		    && nevent.xkey.keycode == event.xkey.keycode)
		{
		    printf("Key repeat %d ms!\n", event.xkey.time);
		}

	    }

	    if (event.type == ButtonPress)
	    {
		// no extra buttons
		printf("ButtonPress: %d\n", event.xbutton.button);
	    }
	    else if (event.type == ButtonRelease)
	    {
		// no extra buttons
		printf("ButtonRelease: %d\n", event.xbutton.button);
	    }

	    if (event.type == FocusIn)
	    {
		// we can use this as un-hidden event
		//printf("Window focused!\n");
	    }
	    else if (event.type == FocusOut)
	    {
		//printf("Window unfocused!\n");
	    }

	    if (event.type == EnterNotify)
	    {
		//printf("Enter window\n");
	    }
	    else if (event.type == LeaveNotify)
	    {
		//printf("Leave window\n");
	    }

	    if (event.type == VisibilityNotify)
	    {
		//printf("Visibility changed!\n");
	    }

	    if (event.type == PropertyNotify)
	    {
		XPropertyEvent ptyEvent = event.xproperty;
		Atom atom = ptyEvent.atom;

		if (atom == aWindowManager)
		{
		    char* pAtomName = XGetAtomName(pDisplay, atom);
		    printf("Atom: %s [%d]\n", pAtomName, fi);

		    swl_u64 after = 1;
		    do {
			// note: should be 4
			Atom xaAtom = XInternAtom(pDisplay, "XA_ATOM", 1);
			printf("XA_ATOM: %s\n", XGetAtomName(pDisplay, 4));
			Atom type;
			swl_i32 format;
			swl_u64 length;
			swl_u8* dp;
			Status status = XGetWindowProperty(
			    pDisplay, window, atom, 0, after, 0,
			    4, &type, &format, &length, &after, &dp);

			if (status == Success && type == 4
			    && dp
			    && format == 32 /*atom for type of the property*/)
			{
			    swl_i32 maxH = 0, maxV = 0;
			    for (int i = 0; i < length; i++)
			    {
				Atom prop = ((Atom*)dp)[i];

				if (prop == aFocused)
				    printf("Focused by atom!\n");
				if (prop == aMaxH)
				{
				    printf("MaxH!\n");
				    maxH = 1;
				}
				if (prop == aMaxV)
				{
				    printf("MaxV!\n");
				    maxV = 1;
				}
				if (prop == aHidden)
				    printf("Hidden!\n");

				if (maxH && maxV)
				{
				    printf("Maximized\n");
				}

			    }
			}
			else {
			    printf("False!\n");
			}

		    }
		    while (after);

		}

	    }


	    if (event.type == MotionNotify)
	    {
		//printf("MotionNotify %d\n", fi);
	    }

	    if (event.type == ConfigureNotify)
	    {
		printf("Resize good!\n");
		XConfigureEvent conf = event.xconfigure;
		printf("p(%d, %d) s(%d, %d)\n", conf.x, conf.y, conf.width, conf.height);
	    }

	    if (event.type == ResizeRequest)
	    {
		printf("Resize!\n");

		XResizeRequestEvent resizeRequest = event.xresizerequest;
		int type;
		unsigned long serial;	/* # of last request processed by server */
		Bool send_event;	/* true if this came from a SendEvent request */
		Display *display;	/* Display the event was read from */
		Window window;
		int width, height;


		/* Display* display  */
		/* Window	/\* w *\/, */
		/* int			/\* x *\/, */
		/* int			/\* y *\/, */
		/* unsigned int	/\* width *\/, */
		/* unsigned int	/\* height *\/ */

		long someVal = 0;
		XSizeHints xSizeHints;
		XGetWMNormalHints(resizeRequest.display,
				  resizeRequest.window,
				  &xSizeHints,
				  &someVal);

		XMoveResizeWindow(resizeRequest.display, resizeRequest.window, xSizeHints.x, xSizeHints.y, resizeRequest.width, resizeRequest.height);
		XFlush(pDisplay);
	    }


	    ++fi;
	    if (fi > 2000000000)
		fi = 0;

	    pevent = event;
	}
	XFlush(pDisplay);


    }

    XCloseDisplay(pDisplay);

#endif


    return 0;
}
