#ifndef KEY_CODES_H
#define KEY_CODES_H

typedef enum ActionType
{
    ActionType_Release = 0,
    ActionType_Press = 1,
    ActionType_Repeat = 2,
    ActionType_Count
} ActionType;

typedef enum KeyType
{
    KeyType_Unknown = -1,

    KeyType_Space = 32,
    KeyType_Apostrophe = 39,  /* ' */
    KeyType_Comma = 44,  /* , */
    KeyType_Minus = 45,  /* - */
    KeyType_Period = 46,  /* . */
    KeyType_Slash = 47,  /* / */

    KeyType_0 = 48,
    KeyType_1 = 49,
    KeyType_2 = 50,
    KeyType_3 = 51,
    KeyType_4 = 52,
    KeyType_5 = 53,
    KeyType_6 = 54,
    KeyType_7 = 55,
    KeyType_8 = 56,
    KeyType_9 = 57,

    KeyType_Semicolon = 59,  /* ; */
    KeyType_Equal = 61,  /* = */

    KeyType_A = 65,
    KeyType_B = 66,
    KeyType_C = 67,
    KeyType_D = 68,
    KeyType_E = 69,
    KeyType_F = 70,
    KeyType_G = 71,
    KeyType_H = 72,
    KeyType_I = 73,
    KeyType_J = 74,
    KeyType_K = 75,
    KeyType_L = 76,
    KeyType_M = 77,
    KeyType_N = 78,
    KeyType_O = 79,
    KeyType_P = 80,
    KeyType_Q = 81,
    KeyType_R = 82,
    KeyType_S = 83,
    KeyType_T = 84,
    KeyType_U = 85,
    KeyType_V = 86,
    KeyType_W = 87,
    KeyType_X = 88,
    KeyType_Y = 89,
    KeyType_Z = 90,

    KeyType_LeftBracket = 91,  /* [ */
    KeyType_BACKSLASH = 92,  /* \ */
    KeyType_RightBracket = 93,  /* ] */
    KeyType_GraveAccent = 96,  /* ` */
    // Useless ??
    KeyType_World1 = 161, /* non-US #1 */
    KeyType_World2 = 162, /* non-US #2 */

    KeyType_Escape = 256,
    KeyType_Enter = 257,
    KeyType_Tab = 258,
    KeyType_Backspace = 259,
    KeyType_Insert = 260,
    KeyType_Delete = 261,

    KeyType_Right = 262,
    KeyType_Left = 263,
    KeyType_Down = 264,
    KeyType_Up = 265,

    KeyType_PageUp = 266,
    KeyType_PageDown = 267,
    KeyType_Home = 268,
    KeyType_End = 269,
    KeyType_CapsLock = 280,
    KeyType_ScrollLock = 281,
    KeyType_NumLock = 282,
    KeyType_PrintScreen = 283,
    KeyType_Pause = 284,

    KeyType_F1 = 290,
    KeyType_F2 = 291,
    KeyType_F3 = 292,
    KeyType_F4 = 293,
    KeyType_F5 = 294,
    KeyType_F6 = 295,
    KeyType_F7 = 296,
    KeyType_F8 = 297,
    KeyType_F9 = 298,
    KeyType_F10 = 299,
    KeyType_F11 = 300,
    KeyType_F12 = 301,
    KeyType_F13 = 302,
    KeyType_F14 = 303,
    KeyType_F15 = 304,
    KeyType_F16 = 305,
    KeyType_F17 = 306,
    KeyType_F18 = 307,
    KeyType_F19 = 308,
    KeyType_F20 = 309,
    KeyType_F21 = 310,
    KeyType_F22 = 311,
    KeyType_F23 = 312,
    KeyType_F24 = 313,
    KeyType_F25 = 314,

    KeyType_KP_0 = 320,
    KeyType_KP_1 = 321,
    KeyType_KP_2 = 322,
    KeyType_KP_3 = 323,
    KeyType_KP_4 = 324,
    KeyType_KP_5 = 325,
    KeyType_KP_6 = 326,
    KeyType_KP_7 = 327,
    KeyType_KP_8 = 328,
    KeyType_KP_9 = 329,
    KeyType_KP_DECIMAL = 330,
    KeyType_KP_DIVIDE = 331,
    KeyType_KP_MULTIPLY = 332,
    KeyType_KP_SUBTRACT = 333,
    KeyType_KP_ADD = 334,
    KeyType_KP_ENTER = 335,
    KeyType_KP_EQUAL = 336,
    KeyType_LeftShift = 340,
    KeyType_LeftControl = 341,
    KeyType_LeftAlt = 342,
    KeyType_LeftSuper = 343,
    KeyType_RightShift = 344,
    KeyType_RightControl = 345,
    KeyType_RightAlt = 346,
    KeyType_RightSuper = 347,
    KeyType_Menu = 348
} KeyType;

typedef enum MouseButtonType
{
    MouseButtonType_Last = 7,
    MouseButtonType_Left = 0,
    MouseButtonType_Right = 1,
    MouseButtonType_Middle = 2,
} MouseButtonType;

typedef enum ModType
{
    ModType_Shift = 0x0001,
    ModType_Control = 0x0002,
    ModType_Alt = 0x0004,
    ModType_Super = 0x0008,
    ModType_CapsLock = 0x0010,
    ModType_NumLock = 0x0020
} ModType;

typedef enum CursorMode
{
    CursorMode_Normal = 0,
    CursorMode_Hidden = 1,
    CursorMode_Locked = 2,
    CursorMode_Count
} CursorMode;

#define RELEASE                0
#define PRESS                  1
#define REPEAT                 2

#define HAT_CENTERED           0
#define HAT_UP                 1
#define HAT_RIGHT              2
#define HAT_DOWN               4
#define HAT_LEFT               8
#define HAT_RIGHT_UP           (HAT_RIGHT | HAT_UP)
#define HAT_RIGHT_DOWN         (HAT_RIGHT | HAT_DOWN)
#define HAT_LEFT_UP            (HAT_LEFT  | HAT_UP)
#define HAT_LEFT_DOWN          (HAT_LEFT  | HAT_DOWN)
#define KEY_UNKNOWN            -1
#define KEY_SPACE              32
#define KEY_APOSTROPHE         39  /* ' */
#define KEY_COMMA              44  /* , */
#define KEY_MINUS              45  /* - */
#define KEY_PERIOD             46  /* . */
#define KEY_SLASH              47  /* / */
#define KEY_0                  48
#define KEY_1                  49
#define KEY_2                  50
#define KEY_3                  51
#define KEY_4                  52
#define KEY_5                  53
#define KEY_6                  54
#define KEY_7                  55
#define KEY_8                  56
#define KEY_9                  57
#define KEY_SEMICOLON          59  /* ; */
#define KEY_EQUAL              61  /* = */
#define KEY_A                  65
#define KEY_B                  66
#define KEY_C                  67
#define KEY_D                  68
#define KEY_E                  69
#define KEY_F                  70
#define KEY_G                  71
#define KEY_H                  72
#define KEY_I                  73
#define KEY_J                  74
#define KEY_K                  75
#define KEY_L                  76
#define KEY_M                  77
#define KEY_N                  78
#define KEY_O                  79
#define KEY_P                  80
#define KEY_Q                  81
#define KEY_R                  82
#define KEY_S                  83
#define KEY_T                  84
#define KEY_U                  85
#define KEY_V                  86
#define KEY_W                  87
#define KEY_X                  88
#define KEY_Y                  89
#define KEY_Z                  90
#define KEY_LEFT_BRACKET       91  /* [ */
#define KEY_BACKSLASH          92  /* \ */
#define KEY_RIGHT_BRACKET      93  /* ] */
#define KEY_GRAVE_ACCENT       96  /* ` */
#define KEY_WORLD_1            161 /* non-US #1 */
#define KEY_WORLD_2            162 /* non-US #2 */
#define KEY_ESCAPE             256
#define KEY_ENTER              257
#define KEY_TAB                258
#define KEY_BACKSPACE          259
#define KEY_INSERT             260
#define KEY_DELETE             261
#define KEY_RIGHT              262
#define KEY_LEFT               263
#define KEY_DOWN               264
#define KEY_UP                 265
#define KEY_PAGE_UP            266
#define KEY_PAGE_DOWN          267
#define KEY_HOME               268
#define KEY_END                269
#define KEY_CAPS_LOCK          280
#define KEY_SCROLL_LOCK        281
#define KEY_NUM_LOCK           282
#define KEY_PRINT_SCREEN       283
#define KEY_PAUSE              284
#define KEY_F1                 290
#define KEY_F2                 291
#define KEY_F3                 292
#define KEY_F4                 293
#define KEY_F5                 294
#define KEY_F6                 295
#define KEY_F7                 296
#define KEY_F8                 297
#define KEY_F9                 298
#define KEY_F10                299
#define KEY_F11                300
#define KEY_F12                301
#define KEY_F13                302
#define KEY_F14                303
#define KEY_F15                304
#define KEY_F16                305
#define KEY_F17                306
#define KEY_F18                307
#define KEY_F19                308
#define KEY_F20                309
#define KEY_F21                310
#define KEY_F22                311
#define KEY_F23                312
#define KEY_F24                313
#define KEY_F25                314
#define KEY_KP_0               320
#define KEY_KP_1               321
#define KEY_KP_2               322
#define KEY_KP_3               323
#define KEY_KP_4               324
#define KEY_KP_5               325
#define KEY_KP_6               326
#define KEY_KP_7               327
#define KEY_KP_8               328
#define KEY_KP_9               329
#define KEY_KP_DECIMAL         330
#define KEY_KP_DIVIDE          331
#define KEY_KP_MULTIPLY        332
#define KEY_KP_SUBTRACT        333
#define KEY_KP_ADD             334
#define KEY_KP_ENTER           335
#define KEY_KP_EQUAL           336
#define KEY_LEFT_SHIFT         340
#define KEY_LEFT_CONTROL       341
#define KEY_LEFT_ALT           342
#define KEY_LEFT_SUPER         343
#define KEY_RIGHT_SHIFT        344
#define KEY_RIGHT_CONTROL      345
#define KEY_RIGHT_ALT          346
#define KEY_RIGHT_SUPER        347
#define KEY_MENU               348
#define KEY_LAST               KEY_MENU

#define MOD_SHIFT           0x0001
#define MOD_CONTROL         0x0002
#define MOD_ALT             0x0004
#define MOD_SUPER           0x0008
#define MOD_CAPS_LOCK       0x0010
#define MOD_NUM_LOCK        0x0020

#define MOUSE_BUTTON_1         0
#define MOUSE_BUTTON_2         1
#define MOUSE_BUTTON_3         2
#define MOUSE_BUTTON_4         3
#define MOUSE_BUTTON_5         4
#define MOUSE_BUTTON_6         5
#define MOUSE_BUTTON_7         6
#define MOUSE_BUTTON_8         7
#define MOUSE_BUTTON_LAST      MOUSE_BUTTON_8
#define MOUSE_BUTTON_LEFT      MOUSE_BUTTON_1
#define MOUSE_BUTTON_RIGHT     MOUSE_BUTTON_2
#define MOUSE_BUTTON_MIDDLE    MOUSE_BUTTON_3

#define JOYSTICK_1             0
#define JOYSTICK_2             1
#define JOYSTICK_3             2
#define JOYSTICK_4             3
#define JOYSTICK_5             4
#define JOYSTICK_6             5
#define JOYSTICK_7             6
#define JOYSTICK_8             7
#define JOYSTICK_9             8
#define JOYSTICK_10            9
#define JOYSTICK_11            10
#define JOYSTICK_12            11
#define JOYSTICK_13            12
#define JOYSTICK_14            13
#define JOYSTICK_15            14
#define JOYSTICK_16            15
#define JOYSTICK_LAST          JOYSTICK_16

#define GAMEPAD_BUTTON_A               0
#define GAMEPAD_BUTTON_B               1
#define GAMEPAD_BUTTON_X               2
#define GAMEPAD_BUTTON_Y               3
#define GAMEPAD_BUTTON_LEFT_BUMPER     4
#define GAMEPAD_BUTTON_RIGHT_BUMPER    5
#define GAMEPAD_BUTTON_BACK            6
#define GAMEPAD_BUTTON_START           7
#define GAMEPAD_BUTTON_GUIDE           8
#define GAMEPAD_BUTTON_LEFT_THUMB      9
#define GAMEPAD_BUTTON_RIGHT_THUMB     10
#define GAMEPAD_BUTTON_DPAD_UP         11
#define GAMEPAD_BUTTON_DPAD_RIGHT      12
#define GAMEPAD_BUTTON_DPAD_DOWN       13
#define GAMEPAD_BUTTON_DPAD_LEFT       14
#define GAMEPAD_BUTTON_LAST            GAMEPAD_BUTTON_DPAD_LEFT

#define GAMEPAD_BUTTON_CROSS       GAMEPAD_BUTTON_A
#define GAMEPAD_BUTTON_CIRCLE      GAMEPAD_BUTTON_B
#define GAMEPAD_BUTTON_SQUARE      GAMEPAD_BUTTON_X
#define GAMEPAD_BUTTON_TRIANGLE    GAMEPAD_BUTTON_Y

#define GAMEPAD_AXIS_LEFT_X        0
#define GAMEPAD_AXIS_LEFT_Y        1
#define GAMEPAD_AXIS_RIGHT_X       2
#define GAMEPAD_AXIS_RIGHT_Y       3
#define GAMEPAD_AXIS_LEFT_TRIGGER  4
#define GAMEPAD_AXIS_RIGHT_TRIGGER 5
#define GAMEPAD_AXIS_LAST          GAMEPAD_AXIS_RIGHT_TRIGGER

#endif // KEY_CODES_H
