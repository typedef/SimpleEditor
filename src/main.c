#include <stdio.h>
#include <stdlib.h>

#include "raylib.h"

#include "MyLibDep.h"
#include "KeyCodes.h"

#include <stdint.h>

typedef struct SgeFile
{
    char* pContent;
    size_t Size;
    // temp
    char** apLines;
} SgeFile;

typedef struct SgeEditor
{
    SgeFile CurrentFile;
} SgeEditor;

static SgeEditor gEditor;

void
sge_editor_file_open(SgeEditor* pEditor, const char* pPath)
{
    size_t fileLength = 0;
    char* pContent = file_read_string_ext(pPath, &fileLength);

    // preprocess lines
    char** apLines = NULL;
    char* astr = NULL;
    for (char* ptr = pContent; *ptr != '\0'; ++ptr)
    {
	char c = *ptr;
	if (c == '\n')
	{
	    string_builder_appendc(astr, '\0');
	    char* nstr = string(astr);
	    array_push(apLines, nstr);
	}
	else
	{
	    string_builder_appendc(astr, c);
	}
    }

    SgeFile file = {
	.pContent = pContent,
	.Size = fileLength,
	.apLines = apLines,
    };

    pEditor->CurrentFile = file;
}

void
sge_editor_draw_current_buffer(SgeEditor* pEditor)
{
    SgeFile file = pEditor->CurrentFile;
    i64 linesCnt = array_count(file.apLines);
    i32 currentLine = 0;

    i32 xstart = 20, ystart = 5;


    i32 xoffset = linesCnt % 10;


    Color lineCounterColor = (Color) { 111, 111, 111, 255 };

    // draw lines
    char buf[64] = {};
    for (i32 i = 0; i < 700/20; ++i)
    {
	i32 y = ystart + i*20;
	memset(buf, 0, 64);
	snprintf(buf, 64, "%d", i);
	DrawText(buf, xstart, y, 20, lineCounterColor);
    }

    for (i32 i = 0; i < linesCnt; ++i)
    {
	i32 x = xstart + xoffset, y = ystart + i*20;
	DrawText(file.apLines[i], x, y, 20, (Color) {177, 177, 177, 255});
    }
}

i32 main()
{
    i32 width = 1000;
    i32 height = 700;

    Color bgColor = {25, 25, 25, 255};
    InitWindow(width, height, "NewWindow");
    SetTargetFPS(60);

    const char* pFileContent = "\n"
	"\n\n\n\n\n"
	"             Nothing here             "
	"\n\n\n\n\n\n";
    char** apLines = NULL;
    array_push(apLines, "                        ");
    array_push(apLines, "                        ");
    array_push(apLines, "         NOTHING        ");
    array_push(apLines, "           IS           ");
    array_push(apLines, "          HERE          ");
    array_push(apLines, "                        ");
    array_push(apLines, "                        ");
    array_push(apLines, "                        ");

    SgeFile defaultFile = {
	.pContent = pFileContent,
	.Size = string_length(pFileContent),
	.apLines = apLines,
    };

    gEditor.CurrentFile = defaultFile;

    //Font font = LoadFont("resources/fonts/NotoSans.ttf");

    while (!WindowShouldClose())
    {
	BeginDrawing();
	ClearBackground(bgColor);

	//DrawText("Congrats! You created your first window!", 50, 50, 20, (Color) {177, 177, 177, 255});

	if (IsKeyPressed(KeyType_F1))
	{
	    sge_editor_file_open(&gEditor, "src/main.c");
	    array_foreach(gEditor.CurrentFile.apLines, printf("%s\n", item););

	}

	sge_editor_draw_current_buffer(&gEditor);
	//DrawText("#include <stdio.h>");

	EndDrawing();

	/* SwapScreenBuffer(); */
	/* PollInputEvents(); */
    }

    CloseWindow();

    printf("hello world\n");
    return 0;
}
