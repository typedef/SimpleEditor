/*

		 #####################################
		 #####################################
			       Types.h
		 #####################################
		 #####################################

*/
#ifndef TYPES_H
#define TYPES_H

//#warning "Should replace Types from Utils/Types.h -> Core/Types.h"

#include <stdint.h>
#include <unistd.h>

//#if !defined(_STDIO_H)
//int printf(const char * format, ... );
//#endif //_STDIO_H

#define BASE_ASSET_PATH "assets"
#define asset(path) BASE_ASSET_PATH path
#define asset_shader(shader) asset("/shaders/") shader
#define asset_texture(texture) asset("/textures/") texture
#define asset_cubemap(cubemap) asset("/cubemaps/") cubemap
#define asset_font(font) asset("/fonts/") font
#define asset_model(model) asset("/models/") model
#define asset_sound(sound) asset("/sounds/") sound
#define asset_scene(scene) asset("/scenes/") scene
#define asset_db(db) asset("/databases/") db
#define asset_localization(local) asset("/localization/") local

#define BASE_RESOURCE_PATH "Resources"
#define resource(path) BASE_RESOURCE_PATH path
#define resource_shader(path) resource("/Shaders/") path
#define resource_font(path) resource("/Fonts/") path
#define resource_texture(path) resource("/Textures/") path
#define resource_model(path) resource("/Models/") path

#define DEBUG_WO_FORCE_INLINE 0
#if DEBUG_WO_FORCE_INLINE == 1
#warning "Slow mode enabled!\n #define DEBUG_WO_FORCE_INLINE 1"
#endif

#if defined(WIN32) || defined(_WIN32) || DEBUG_WO_FORCE_INLINE
 #define force_inline static
#if defined(WINDOWS_PLATFORM) && !defined(DEBUG_WO_FORCE_INLINE)
   #define WINDOWS_PLATFORM
 #endif
#else
 #define force_inline static inline __attribute((always_inline))
 #ifndef LINUX_PLATFORM
  #define LINUX_PLATFORM
 #endif
#endif

#define IfNullThen(x, t)			\
    ({						\
	__typeof__((x)) xv = (x);		\
	xv != NULL ? xv : (t);			\
    })

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef float f32;
typedef double f64;
typedef u64 size_t;
#define i8(x)  ((i8)(x))
#define i16(x) ((i16)(x))
#define i32(x) ((i32)(x))
#define i32_ptr(x) ((i32*)(x))
#define i32_ptr_assign(x) *((i32*) (x))
#define i32_abs(x) ({ i32 temp = (x); temp < 0 ? -temp : temp; })
#define i64(x) ((i64)(x))
#define u8(x)  ((u8)(x))
#define u16(x) ((u16)(x))
#define u32(x) ((u32)(x))
#define u64(x) ((u64)(x))
#define f32(x) ((f32)(x))
#define f32_abs(x)				\
    ({						\
	union {f32 a; u32 b; } r;		\
	r.a = x;				\
	r.b &= 0x7fffffff;			\
	r.a;					\
    })
#define f64(x) ((f64)(x))

#define sing_equal(x, y)			\
    ({						\
	__typeof__(x) mulv = (x * y);		\
	(mulv >= 0) ? 1 : 0;			\
    })

#include <stddef.h>
typedef wchar_t wchar;
typedef struct WideString
{
    size_t Length;
    wchar* Buffer;
} WideString;

typedef struct IWideStringPool
{
    WideString** Pool;
} IWideStringPool;

#define void_to_i32(ptr) ((ptr) ? (*((i32*)ptr)) : ({assert(0 && "Wrong argument void_to_i32!"); 0; }))
#define void_to_f32(ptr) (*((f32*)ptr))
#define void_to_string(ptr) ((const char*)ptr)
#define i32_to_void_ptr(i32v) ((void*)(&(i32v)))

#define I8_MAX 127
#define I16_MAX 32767
#define I32_MAX 2147483647
#define I32_MAX_HALF 1123241323
#define I64_MAX (9223372036854775807LL)
#define U8_MAX  255
#define U16_MAX 65535
#define U32_MAX 4294967295
#define U64_MAX (18446744073709551615ULL) /*18 446 744 073 709 551 615*/
#define F32_MAX (340282346638528859811704183484516925440.000000)

#ifndef NULL
#define NULL ((void*)0)
#endif

typedef enum ResultType
{
    ResultType_Error = 0,
    ResultType_Success = 1,
    ResultType_Warning = 2
} ResultType;

#define EMPTY(type) (type){0}

//NOTE(typedef): we using gcc ext here for now
#define Align(byte) __attribute__((aligned(byte)))
#define TYPE_REVERSE(x) (x = !(x))
#define OffsetOf(Type, Field) ( (size_t) (&(((Type*)0)->Field)) )
#define AlignmentOf(Type) ((u64)&(((struct{char c; Type i;}*)0)->i))
#define AlignOf(Type) ((u64)&(((struct{char c; Type i;}*)0)->i))
#define ARRAY_LENGTH(x) (sizeof(x) / sizeof(x[0]))
#define ARRAY_COUNT(x) (sizeof(x) / sizeof(x[0]))
#define TO_STRING(x) #x
#define ToString(x) #x
#define FMOD(x, v) (f32) (((i32)(x)) % v + ((x) - (i32)(x)))
#define KB(x) ((i64)1024 * (i64)x)
#define MB(x) ((i64)1024 * KB(x))
#define GB(x) ((i64)1024 * MB(x))
#define I32T(t, h) t##h
#define I32M(m, t, h) m##t##h
#define I32B(b, m, t, h) b##m##t##h
#define TOKB(x) (((f64) x) / 1024)
#define TOMB(x) (((f64) TOKB(x)) / 1024)
#define TOGB(x) (((f64) TOMB(x)) / 1024)
#define ABS(x)					\
    ({						\
	((x) > 0) ? (x) : -(x);			\
    })

#define K(x) (1000 * (x))

#define MAX(x, y)				\
    ({						\
	__typeof__(x) l = x;			\
	__typeof__(y) r = y;			\
	(l > r) ? l : r;			\
    })
#define MIN(x, y)				\
    ({						\
	__typeof__(x) l = x;			\
	__typeof__(y) r = y;			\
	(l > r) ? r : l;			\
    })
#define MINMAX(x, min, max)				\
    ({							\
	__typeof__(x) l = x;				\
	__typeof__(min) mn = min;			\
	__typeof__(max) mx = max;			\
	((l > mx) ? mx : (l < mn ? mn : l));		\
    })
#define SWAP(x, y)				\
    ({						\
	__typeof__(x) temp = x;			\
	x = y;					\
	y = temp;				\
    })

#define Max(x,y) MAX(x,y)
#define Min(x,y) MIN(x,y)
#define MinMax(x, min, max) MINMAX(x, min, max)
#define MinMaxV2(x, v2v) MINMAX(x, v2v.Min, v2v.Max)
#define Swap(x,y) SWAP(x,y)

#define IS_BIG_ENDIAN (!*(unsigned char *)&(uint16_t){1})
#define IS_LITTLE_ENDIAN (!(IS_BIG_ENDIAN))

typedef struct R
{
    u8 R;
} R;

typedef struct RG
{
    u8 R;
    u8 G;
} RG;

typedef struct RGB //((r << 16) | (g << 8) | (b))
{
    u8 R;
    u8 G;
    u8 B;
} RGB;

typedef struct RGBA //((r << 24) | (g << 16) | (b << 8) | (a))
{
    u8 R;
    u8 G;
    u8 B;
    u8 A;
} RGBA;

#define R(r) ((u32) (((RGBA*) (r))->R))
#define G(g) ((u32) (((RGBA*) (g))->G))
#define B(b) ((u32) (((RGBA*) (b))->B))
#define A(a) ((u32) (((RGBA*) (a))->A))
#define RGBA(r, g, b, a) ((a << 24) | (b << 16) | (g << 8) | (r))

typedef f32 v3a[3];
typedef f32 v4a[4];
typedef v3a m3a[3];
typedef v4a m4a[4];

typedef struct v2
{
    union
    {
	struct
	{
	    f32 X;
	    f32 Y;
	};
	struct
	{
	    f32 Width;
	    f32 Height;
	};
	struct
	{
	    f32 Min;
	    f32 Max;
	};

	f32 V[2];
    };
} v2;

typedef struct v2i
{
    union
    {
	struct
	{
	    i32 X;
	    i32 Y;
	};
	struct
	{
	    i32 Width;
	    i32 Height;
	};

	i32 V[2];
    };
} v2i;

typedef struct v3
{
    union
    {
	struct
	{
	    f32 X;
	    f32 Y;
	    f32 Z;
	};
	struct
	{
	    f32 R;
	    f32 G;
	    f32 B;
	};
	struct
	{
	    f32 Hue;
	    f32 Saturation;
	    f32 Value;
	};
	struct
	{
	    f32 Pitch;
	    f32 Yaw;
	    f32 Roll;
	};

	v2 XY;
	f32 V[3];

    };
} v3;

typedef struct v4
{
    union
    {
	struct
	{
	    f32 X;
	    f32 Y;
	    f32 Z;
	    f32 W;
	};
	struct
	{
	    f32 R;
	    f32 G;
	    f32 B;
	    f32 A;
	};
	f32 V[4];
    };
} v4;

typedef struct v4i
{
    union
    {
	struct
	{
	    i32 X;
	    i32 Y;
	    i32 Z;
	    i32 W;
	};

	struct
	{
	    i32 R;
	    i32 G;
	    i32 B;
	    i32 A;
	};

	i32 V[4];
    };

} v4i;

typedef struct m3
{
    union
    {
	struct
	{
	    f32 M00;
	    f32 M01;
	    f32 M02;

	    f32 M10;
	    f32 M11;
	    f32 M12;

	    f32 M20;
	    f32 M21;
	    f32 M22;
	};
	v3 V[3];
	m3a M;
    };
} m3;

typedef struct m4
{
    union
    {
	struct
	{
	    f32 M00;
	    f32 M01;
	    f32 M02;
	    f32 M03;

	    f32 M10;
	    f32 M11;
	    f32 M12;
	    f32 M13;

	    f32 M20;
	    f32 M21;
	    f32 M22;
	    f32 M23;

	    f32 M30;
	    f32 M31;
	    f32 M32;
	    f32 M33;
	};

	v4 V[4];
	m4a M;
    };
} m4;

typedef struct quat
{
    union
    {
	struct
	{
	    f32 X;
	    f32 Y;
	    f32 Z;
	    f32 W;
	};

	v4 V4;

	f32 V[4];
    };
} quat;

typedef struct Aabb
{
    v3 Min;
    v3 Max;
} Aabb;

typedef struct Arena
{
    i64 Id;
    size_t Offset;
    size_t Size;
    i32 Line;
    const char* File;
    void* Data;
} Arena;

typedef struct IString
{
    i32 Length;
    char* Buffer;
} IString;

typedef struct IStringPool
{
    IString** Strings;
} IStringPool;

#endif // Types.h

#ifndef BASE_HELPER_H
#define BASE_HELPER_H

#define DO_SINGLE_TIME(action)			\
    {						\
	static int lock = 1;			\
	if (lock)				\
	{					\
	    lock = 0;				\
	    action;				\
	}					\
    }

#define DO_MANY_TIME(action, count)		\
    {						\
	static int lock = count;		\
	if (lock)				\
	{					\
	    --lock;				\
	    action;				\
	}					\
    }

#define IS_NULL_OFFSET(ptr)						\
    ({									\
	vassert(sizeof(ptr) == 8 && "IS_NULL_OFFSET takes pointer as parameter!"); \
	size_t address = (size_t) ptr;					\
	address < 10000 ? 1 : 0;					\
    })

#define DO_ONES(func) { static int flag = 1; if (flag) { flag = 0; func; } }
#define DO_ONES3(func, msg, out) { static int flag = 1; if (flag) { flag = 0; func(msg, out); } }

#define CALL_N(n)					\
    {							\
	static int start = 1;				\
	if (start > n)					\
	{						\
	    vassert(0 && "More then "#n" call");	\
	}						\
	++start;					\
    }

#define FlagSwitchDefine(enableFunction, disableFunction)	\
    ({								\
	static int flag = 0;					\
	if (!flag)						\
	{							\
	    flag = 1;						\
	    enableFunction;					\
	}							\
	else							\
	{							\
	    flag = 0;						\
	    disableFunction;					\
	}							\
    })

#define StructEquals(s1, s2)					\
    ({								\
	int result = 0;						\
	size_t size1 = sizeof((s1));				\
	size_t size2 = sizeof((s2));				\
								\
	if (size1 == size2)					\
	{							\
	    result = memcmp(&(s1), &(s2), size1) == 0 ? 1 : 0;	\
	}							\
								\
	result;							\
    })

#define elvis_m1(x, y)				\
    ({						\
	__typeof__(x) xv = (x);			\
	__typeof__(y) yv = (y);			\
	(xv != -1) ? xv : yv;			\
    })
#define elvis(x, y)				\
    ({						\
	__typeof__(x) xv = (x);			\
	__typeof__(y) yv = (y);			\
	(xv) ? xv : yv;				\
    })

#define nullptr ((void*) 0)

#define ptr_diff(ptr1, ptr2)					\
    ({								\
	size_t length = ((size_t)ptr2) - ((size_t)ptr1);	\
	length;							\
    })

#endif


#ifndef GUARD_H
#define GUARD_H

void _vbreak(const char* msg, const char* file, int line, const char* condition);

#if defined(ENGINE_DEBUG)
#define vassert(a)							\
    ({									\
	if (!(a))							\
	{								\
	    _vbreak("[ASSERT] %s:%d condition: %s is false!\n", __FILE__, __LINE__, #a); \
	}								\
    })
#define vassert_not_null(ptr) vassert(ptr != NULL)
#define vassert_null(ptr) vassert((ptr == NULL))
#define vassert_break() vassert(0 && "We shouldn't get here!!!")
#define vassert_null_offset(ptr) vassert(!IS_NULL_OFFSET(ptr))
#else // ENGINE_DEBUG
#define vassert(a) ((void))
#define vassert_not_null(ptr) ((void))
#define vassert_null(ptr) ((void))
#define vassert_break() ((void))
#define vassert_null_offset(ptr) ((void))
#endif // ENGINE_RELEASE

/*
  DOCS(typedef): Always exist, independent from the build (debug/release)
*/
#define vguard(ptr)							\
    ({									\
	if (!(ptr))							\
	{								\
	    _vbreak("[GUARD] %s:%d condition: %s is false!\n", __FILE__, __LINE__, #ptr); \
	}								\
    })
#define vguard_not_null(ptr)			\
    ({						\
	vguard(ptr != NULL && #ptr);		\
    })
#define vguard_null(ptr)			\
    ({						\
	vguard(ptr == NULL);			\
    })
#define vguard_legacy(func)				\
    ({							\
	vguard(0 && #func " - legacy function!");	\
    })
#define vguard_not_impl()			\
    ({						\
	vguard(0 && "not implementsd!");	\
    })

#endif // GUARD_H

#include <stdio.h>

#ifndef SIMPLE_STANDARD_LIBRARY_H
#define SIMPLE_STANDARD_LIBRARY_H

/*
  Content:
  [Environment.h]

  [SimpleTimer.h] - run inside app loop, pass delta time for correct work.

  [Bitset.h] - bitset for huge bitsets

  [FpsCounter.h] - counter for fps, should be run inside app loop,
  pass function for geting app time to fps count and you can get fps

  [Logger.h] - logger to console/file.

  [SimpleThread.h] - pthread && win thread abstraction, cross-platform

  [MemoryAllocator.h] - base allocator for all apps, easy stack arena allocator create function.

  [GlobalHelpers.h] - just some shit without group

  [Array.h] - dynamic array implementation, like List<T> in C#, but faster and implemented in C99 (modern, cool, easy to use and fast enought to not worry about performance)

  [RingQueue.h] - ring queue based on Array.h

  [String.h] - ascii-string api, works fine

  [WideString.h] - utf32 strings

  [StringBuilder.h] - ascii string builder

  [HashTable.h] - hash table for int/string/wide string keys

  [IO.h] - file read/write api

  [Path.h] - path combine like in C# (not that smart but something)

  [Profiler.h] - easy to use profiler_start/profiler_end/profiler_calc

  [SimpleImage.h] - rgba image, very inefficient

  [SimpleSocket.h] - abstraction over linux/windows Berkley sockets, cross-platform as all code in this SSL

*/

//#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wreserved-user-defined-literal"
#pragma clang diagnostic ignored "-Wunused-function"


//#pragma clang diagnostic pop

#if defined(LINUX_PLATFORM)

#elif defined(WINDOWS_PLATFORM)
#include <windows.h>
#include <shlwapi.h>
#endif

#define STATIC_ANALIZER_CHECK 1

/*

  ###################################
  ###################################
  Environment.h
  ###################################
  ###################################

*/
#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

void sleep_for(size_t seconds);
void linux_set_current_stack_size(i64 currentBytesNumber, i64 maxBytesNumber);

#ifdef LINUX_PLATFORM
#define NEW_LINE "\n"
#define PATH_SEPARATOR '/'
#define PATH_SEPARATOR_STRING "/"
#define ROOT_DIRECTORY "/"
#elif WINDOWS_PLATFORM
#define NEW_LINE "\r\n"
#define PATH_SEPARATOR "\\"
#define PATH_SEPARATOR_STRING "\\"
#define ROOT_DIRECTORY "C:\\"
#else
#error "Unknown platform!"
#endif

#endif // Environment.h

/*

  ###################################
  ###################################
  Time.h
  ###################################
  ###################################

*/

typedef struct SimpleTime
{
    u8 DayOfTheWeek;
    u8 Seconds;
    u8 Minutes;
    u8 Hours;

    u8 Day;
    u8 Month;
    u16 Year;
} SimpleTime;

#include <time.h>

/*

  ###################################
  ###################################
  Time.h
  ###################################
  ###################################

*/

void simple_time_now(SimpleTime* pTime);
void simple_time_print(SimpleTime* pTime);


/*

  ###################################
  ###################################
  SimpleTimer.h
  ###################################
  ###################################

*/

typedef struct TimerData
{
    f32 _CountSeconds;
    f32 WaitSeconds;
} SimpleTimerData;

i32 simple_timer_interval(SimpleTimerData* pTimerData, f32 timestep);
void simple_timer_reset(SimpleTimerData* pTimerData);
void simple_timer_sleep(i64 sec);
void simple_timer_mssleep(i64 ms);
void simple_timer_usleep(i64 usec);

/*

  ###################################
  ###################################
  Bitset.h
  ###################################
  ###################################

*/
#ifndef BITSET_H
#define BITSET_H

typedef struct Bitset
{
    u64* Chunks;
    i32 ChunksCount;
} Bitset;


Bitset bitset_new(i32 bitsCount);
void bitset_reset(Bitset cbs);
void bitset_set(Bitset cbs, i32 bit);
void bitset_unset(Bitset cbs, i32 bit);
i32 bitset_get(Bitset cbs, i32 bit);

#endif // BITSET

/*

  ###################################
  ###################################
  FpsCounter.h
  ###################################
  ###################################

*/
#ifndef FPS_COUNTER_H
#define FPS_COUNTER_H

typedef double (*GetTimeDelegate)();
typedef struct FpsCounter
{
    u32 Fps;
    u32 Frames;
    f64 Since;

    GetTimeDelegate GetTime;
} FpsCounter;

FpsCounter fps_counter_create(GetTimeDelegate getTimeDelegate);
void fps_counter_update(FpsCounter* fpsCounter);

#endif //FpsCounter.h

/*

  #####################################
  #####################################
  Logger.h (CONSOLE LOGGER)
  #####################################
  #####################################

*/
#ifndef LOGGER_H
#define LOGGER_H

/*
  ==================
  ==================
  ==              ==
  ==    COLORS    ==
  ==              ==
  ==================
  ==================

  Name            BG  FG
  Black           30  40
  Red             31  41
  Green           32  42
  Yellow          33  43
  Blue            34  44
  Magenta         35  45
  Cyan            36  46
  White           37  47
  Bright Black    90  100
  Bright Red      91  101
  Bright Green    92  102
  Bright Yellow   93  103
  Bright Blue     94  104
  Bright Magenta  95  105
  Bright Cyan     96  106
  Bright White    97  107

  [a;b;cmTEXT\033[0m
  a - mode
  b - background color
  c - foreground color
  \033 - foreground & background

  examples:
  printf("\033[36mTexting\033[0m \n");

  //Background
  printf("\033[1;42;30mTexting\033[0m\t\t");
  printf("\033[3;44;30mTexting\033[0m\t\t");
  printf("\033[4;104;30mTexting\033[0m\t\t");
  printf("\033[5;100;30mTexting\033[0m\n");

  printf("\033[4;47;35mTexting\033[0m\t\t");
  printf("\033[3;47;35mTexting\033[0m\t\t");
  printf("\033[3;43;30mTexting\033[0m\t\t");

  107


*/

#undef WHITE
#undef BLUE
#undef YELLOW
#undef GREEN
#undef RED
#undef BLACK
#undef ORANGE

#define WHITEBG(x) "\e[30;107m"x"\e[0m"

#define BLACK(x) "\x1B[30m"x"\033[0m"
#define RED(x) "\x1B[31m"x"\033[0m"
#define GREEN(x) "\x1B[32m"x"\033[0m"
#define GREENQ(x) "\x1B[32m"x"\033[0m"
#define ORANGE(x) "\x1B[33m"x"\033[0m"
#define YELLOW(x) "\x1B[93m"x"\033[0m"
#define BLUE(x) "\x1B[34m"x"\033[0m"
#define BLUEM(x) "\x1B[94m"x"\033[0m"
#define MAGNETA(x) "\x1B[35m"x"\033[0m"
#define CYAN(x) "\x1B[36m"x"\033[0m"
#define WHITE(x) "\x1B[37m"x"\033[0m"

#define BRIGHTBLACK(x) "\x1B[90m"x"\033[0m"
#define BRIGHTRED(x) "\x1B[91m"x"\033[0m"
#define BRIGHTGREEN(x) "\x1B[92m"x"\033[0m"
#define BRIGHTYELLOW(x) "\x1B[93m"x"\033[0m"
#define BRIGHTBLUE(x) "\x1B[94m"x"\033[0m"
#define BRIGHTMAGNETA(x) "\x1B[95m"x"\033[0m"
#define BRIGHTCYAN(x) "\x1B[96m"x"\033[0m"
#define BRIGHTWHITE(x) "\x1B[97m"x"\033[0m"

#define RED5(x) "\033[5;1;31m"x"\033[0m"
#define REDBG5(x) "\033[5;101;30m"x"\033[0m"
#define GREEN5(x) "\033[5;1;32m"x"\033[0m"

typedef enum LoggerType
{
    LoggerType_Terminal = 1 << 0,
    LoggerType_File     = 1 << 1
} LoggerType;

typedef enum LoggerFlags
{
    LoggerFlags_None = 0,
    LoggerFlags_PrintType         = 1 << 0,
    LoggerFlags_PrintFile         = 1 << 1,
    LoggerFlags_PrintShortFile    = 1 << 2,
    LoggerFlags_PrintLine         = 1 << 3,
    LoggerFlags_PrintShortLine    = 1 << 4,
    LoggerFlags_PrintTime         = 1 << 5,
    LoggerFlags_PrintShortTime    = 1 << 6,

    LoggerFlags_Short = LoggerFlags_PrintType,
    LoggerFlags_Full = LoggerFlags_PrintType | LoggerFlags_PrintFile | LoggerFlags_PrintLine | LoggerFlags_PrintShortTime

} LoggerFlags;


extern LoggerType gLogOutput;

void logger_init();
void logger_to_terminal();
void logger_to_file();
void logger_output(const char* levelMessage, const char* format, const char* file, i32 line, ...);
void logger_set_flags(LoggerFlags flags);
LoggerFlags logger_get_flags();
void logger_set_short();
void logger_set_long();

#define GLOG(format, ...)                                               \
    logger_output(BRIGHTWHITE("[LOG]"), format, __FILE__, __LINE__, ##__VA_ARGS__)
#define GSUCCESS(format, ...)                                           \
    logger_output(GREENQ("[SUCCESS]"), format, __FILE__, __LINE__, ##__VA_ARGS__)
#define GERROR(format, ...)                                             \
    logger_output(RED("[ERROR]"), format, __FILE__, __LINE__, ##__VA_ARGS__)
#define GWARNING(format, ...)                                           \
    logger_output(BRIGHTYELLOW("[WARNING]"), format, __FILE__, __LINE__, ##__VA_ARGS__)
#define GINFO(format, ...)                                              \
    logger_output(MAGNETA("[INFO]"), format, __FILE__, __LINE__, ##__VA_ARGS__)
#ifdef DEBUG
#define GDEBUG(format, ...)
#else
#define GDEBUG(format, ...)                                             \
    logger_output(BRIGHTGREEN("[DEBUG]"), format, __FILE__, __LINE__, ##__VA_ARGS__)
#endif
#define GFORMAT(string, format, ...) sprintf(string, format, __VA_ARGS__)

#endif // Logger.h

/*

  ###################################
  ###################################
  Array.h (ARRAY)
  ###################################
  ###################################

*/

#ifndef ARRAY_H
#define ARRAY_H

#include <string.h>

typedef struct ArrayHeader
{
    // Only one value for now
    i32 IsReserved;
    i32 ElementSize;
    i64 Count;
    i64 Capacity;
    void* Buffer;
} ArrayHeader;

static i32 StartSize = 1;

#define array_header(b) ((ArrayHeader*) (((char*)b) - sizeof(ArrayHeader)))
#define array_len(b) ((b != NULL) ? array_header(b)->Count : 0)
#define array_count(b) ((b != NULL) ? array_header(b)->Count : 0)
#define array_cap(b) ((b != NULL) ? array_header(b)->Capacity : 0)
#define array_capacity(b) ((b != NULL) ? array_header(b)->Capacity : 0)
#define array_element_size(b) ((b != NULL) ? array_header(b)->ElementSize : 0)
#define array_esize(b) array_element_size(b)

#define array_reserve(b, elementsCount)                                 \
    ({                                                                  \
	(b) = internal_array_reserve((const void*)b, elementsCount, sizeof(*b)); \
	(b);                                                            \
    })
#define array_reserve_w_alloc(b, elementsCount, alloc)                  \
    ({                                                                  \
	internal_array_reserve_w_alloc((const void*)b, elementsCount, sizeof(*b), alloc); \
    })

#define array_grow(b)                                           \
    ({                                                          \
	b = internal_array_grow((const void*)b, sizeof(*b));	\
    })

#define array_set(b, v)                         \
    {                                           \
	assert(b);                              \
	int count = array_count(b);		\
	for (int i = 0; i < count; i++)         \
	{                                       \
	    b[i] = v;                           \
	};                                      \
    }

/* void _array_push(void* pArr, void* pData, size_t size) */
/* { */
/*     b[array_count(b)] = (__VA_ARGS__); */
/*     ++array_header(b)->Count; */
/* } */

#define array_push(b, ...)						\
    {									\
	if ((b) == NULL || array_len(b) >= array_cap(b))		\
	{								\
	    (b) = internal_array_grow((const void*)b, sizeof(__typeof__(b[0]))); \
	}								\
									\
	b[array_count(b)] = (__VA_ARGS__);				\
	++array_header(b)->Count;					\
    }
#define array_push_array(b, otherArray)                                 \
    {                                                                   \
	b = array_reserve_w_alloc(b, array_capacity(otherArray), memory_allocate); \
	memcpy((void*)b, (void*)otherArray, array_count(otherArray) * array_element_size(otherArray)); \
	array_header(b)->Count = array_header(otherArray);              \
    }

#define array_push_void(b, voidData, sizeofsb)                          \
    ({                                                                  \
	if ((b) == NULL || array_len(b) >= array_cap(b))                \
	{                                                               \
	    (b) = internal_array_grow((const void*)b, sizeofsb);        \
	    array_header(b)->IsReserved = 0;                            \
	}                                                               \
									\
	void* returnValue = (b) + array_count(b) * sizeofsb;            \
	memcpy(returnValue, voidData, sizeofsb);                        \
	/* b[array_count(b)] = (voidData); */                           \
	++array_header(b)->Count;                                       \
									\
	returnValue;                                                    \
    })


#define array_push_empty_with_size(b, size)                             \
    {                                                                   \
	if ((b) == NULL || array_len(b) >= array_cap(b))                \
	{                                                               \
	    (b) = internal_array_grow((const void*)b, size);            \
	    array_header(b)->IsReserved = 0;                            \
	}                                                               \
									\
	assert(array_header(b)->IsReserved != 1 && "This array reserved, you can't use push here, use add instead!"); \
									\
	++array_header(b)->Count;                                       \
    }

#define array_add(b, i, ...)			\
    {                                           \
	b[i] = (__VA_ARGS__);			\
    }

#define array_needs_realloc(b)                          \
    ({                                                  \
	((b) == NULL || array_len(b) >= array_cap(b));  \
    })

#define array_push_w_alloc(b, alloc, free, ...)                         \
    {                                                                   \
	if (array_needs_realloc(b))                                     \
	{                                                               \
	    (b) = internal_array_grow_w_alloc((const void*)b, sizeof(*b), alloc, free); \
	    array_header(b)->IsReserved = 0;                            \
	}                                                               \
	b[array_count(b)] = (__VA_ARGS__);                              \
	++array_header(b)->Count;                                       \
    }
#define array_insert(b, ind, ...)                                       \
    ({                                                                  \
	vguard_not_null(b);                                             \
	vassert(b && "array should be allocated before usage!");        \
	ArrayHeader* hdr = array_header(b);                             \
	vassert(ind >= 0 && ind < hdr->Capacity && "Wrong array_insert index!"); \
	(b)[ind] = (__VA_ARGS__);                                       \
	++hdr->Count;                                                   \
    })
#define array_insert_w_func(b, func, ...)				\
    ({									\
	vassert(b && "array should be allocated before usage!");	\
	ArrayHeader* hdr = array_header(b);				\
	if (hdr->Count >= (hdr->Capacity - 1))				\
	{								\
	    func							\
		hdr->Count = 0;						\
	}								\
									\
	(b)[hdr->Count] = (__VA_ARGS__);				\
	++hdr->Count;							\
    })
#define array_push_front(b, ...)					\
    ({									\
	if ((b) == NULL || array_len(b) >= array_cap(b))		\
	{								\
	    (b) = internal_array_grow((const void*)b, sizeof(*b));	\
	}								\
	else								\
	{								\
	    i32 i, count = array_count(b);				\
	    for (i = (count - 1); i >= 1; --i)				\
	    {								\
		b[i] = b[i - 1];					\
	    }								\
	}								\
	b[0] = (__VA_ARGS__);						\
	++array_header(b)->Count;					\
    })

//NOTE(bies): Used for PriorityQueue
#define array_push_at(b, i, ...)					\
    {									\
	if ((b) == NULL || ((array_len(b) + 1) >= array_cap(b)))	\
	{								\
	    (b) = internal_array_grow((const void*)b, sizeof(*b));	\
	}								\
									\
	b = internal_array_shift_right(b, i);				\
	b[i] = (__VA_ARGS__);						\
									\
	++array_header(b)->Count;					\
    }
#define array_push_at_w_alloc(b, i, alloc, free, ...)                   \
    {                                                                   \
	if ((b) == NULL || array_len(b) >= array_cap(b))                \
	{                                                               \
	    (b) = internal_array_grow_w_alloc((const void*)b, sizeof(*b), alloc, free); \
	}                                                               \
	b[i] = (__VA_ARGS__);                                           \
	++array_header(b)->Count;                                       \
    }
#define array_sort(b, condition)                        \
    ({                                                  \
	vguard_not_null(b);                             \
	i32 i, j, count = array_count(b);		\
	for (i = 0; i < count; ++i)                     \
	{                                               \
	    for (j = 0; j < (count-1); ++j)		\
	    {                                           \
		__typeof__(b[0]) item0 = (b)[j];        \
		__typeof__(b[0]) item1 = (b)[j + 1];    \
		if (condition)                          \
		{                                       \
		    (b)[j + 1] = item0;			\
		    (b)[j    ] = item1;			\
		}                                       \
	    }                                           \
	}                                               \
    })


#define array_remove_condition(b, condition)	\
    {						\
	vguard_not_null((b));			\
						\
	i32 i, j, count = array_count(b);	\
	for (i = 0; i < count; ++i)		\
	{					\
	    __typeof__(b[0]) item = (b)[i];	\
	    if (condition)			\
	    {					\
		for (j = i; j < count; ++j)	\
		{				\
		    b[j] = b[j + 1];		\
		}				\
		--array_header(b)->Count;	\
						\
		break;				\
	    }					\
	}					\
    }

void array_remove(void* array, void* itemPtr);

#define array_remove_at(b, i)                                           \
    {                                                                   \
	vguard((i >= 0) && (i < array_count((b))) && "i index is wrong for array_remove_at"); \
	if ((b) != NULL)                                                \
	{                                                               \
	    _array_remove_at(b, i);                                 \
	    --array_header(b)->Count;                                   \
	}                                                               \
									\
    }

#define array_peek(b)					\
    ({							\
	vassert_not_null((b));				\
	i64 count = array_count(b);			\
	vassert (count > 0 && "Array is empty!");	\
	__typeof__((b)[0]) item = (b)[count - 1];	\
	item;						\
    })
#define array_pop(b)                                                    \
    ({                                                                  \
	vassert_not_null((b));                                          \
	i64 count = array_count(b);                                     \
	vassert (count > 0 && "Array is empty!");                       \
	__typeof__((b)[0]) item = (b)[count - 1];                       \
	memset(((void*)b) + (array_count(b) - 1) * array_esize(b), 0, array_esize(b)); \
	--array_header(b)->Count;                                       \
	item;                                                           \
    })
#define array_pop_begin(b)                                      \
    ({                                                          \
	vassert((b) && "Array can't be null or undefined!!!");	\
	vassert (array_count(b) > 0 && "Array is empty!");      \
								\
	__typeof__((b)[0]) item = (b)[0];                       \
								\
	memset((void*)b, 0, array_esize(b));                    \
								\
	if (array_count(b) > 1)                                 \
	{                                                       \
	    memcpy((void*)b,                                    \
		   (((void*)b) + array_esize(b)),               \
		   (array_count(b) - 1) * array_esize(b));      \
	}                                                       \
								\
	--array_header(b)->Count;                               \
								\
	item;                                                   \
    })


//#define array_end(b) (((void*)b) + array_count(b) * sizeof(*b))
#define array_end(b) (&b[array_count(b) - 1])
#define array_copy(src)	internal_array_copy(src)
#define array_copy_w_alloc(src, alloc) internal_array_copy_w_alloc(src, alloc)
#define array_clearv(b, val)                    \
    ({                                          \
	if ((b) != NULL)                        \
	{                                       \
	    int i, count = array_count(b);      \
	    for (i = 0; i < count; ++i)         \
	    {                                   \
		b[i] = val;                     \
	    }                                   \
	    array_header(b)->Count = 0;         \
	}                                       \
    })

#define array_find(b, v)                        \
    ({                                          \
	__typeof__((b)[0]) result = {0};        \
	if ((b) != NULL)                        \
	{                                       \
	    i32 i, count = array_count(b);      \
	    for (i = 0; i < count; ++i)         \
	    {                                   \
		if ((b)[i] == v)		\
		{                               \
		    result = (b)[i];            \
		    break;                      \
		}                               \
	    }                                   \
	}                                       \
	result;                                 \
    })
#define array_find_index(b, v)			\
    ({                                          \
	i32 result = -1;                        \
	if ((b) != NULL)                        \
	{                                       \
	    i32 i, count = array_count(b);      \
	    for (i = 0; i < count; ++i)         \
	    {                                   \
		if ((b)[i] == v)		\
		{                               \
		    result = i;			\
		    break;                      \
		}                               \
	    }                                   \
	}                                       \
	result;                                 \
    })
#define array_index_of(b, indexOfCondition)		\
    ({							\
	i64 result = -1;				\
							\
	if ((b) != NULL)				\
	{						\
	    i64 i, count = array_count(b);		\
	    for (i = 0; i < count; ++i)			\
	    {						\
		__typeof__((b[0])) item = (b)[i];	\
		if (indexOfCondition)			\
		{					\
		    result = i;				\
		    break;				\
		}					\
	    }						\
	}						\
							\
	result;						\
    })

/*
  Usage:
  array_find_predicate(array, tempValue, item == tempValue)
*/
#define array_find_predicate(b, predicate)		\
    ({							\
	__typeof__((b[0])) result = {0};		\
	if ((b) != NULL)				\
	{						\
	    i32 i, count = array_count(b);		\
	    for (i = 0; i < count; ++i)			\
	    {						\
		__typeof__((b[0])) item = (b)[i];	\
		if (predicate)				\
		{					\
		    result = b[i];			\
		    break;				\
		}					\
	    }						\
	}						\
	result;						\
    })

#define array_free(b)							\
    ({									\
	if ((b))							\
	{								\
	    _memory_free(array_header((b)), __LINE__, __FILE__);	\
	}								\
	(b) = NULL;							\
    })

#define array_free_w_item(b)					\
    ({								\
	if ((b))						\
	{							\
	    i32 i, count = array_count(b);			\
	    for (i = 0; i < count; ++i)				\
	    {							\
		_memory_free((b)[i], __LINE__, __FILE__);	\
	    }							\
	    array_free(b);					\
	}							\
    })

#define array_for(count, code)                  \
    ({                                          \
	i32 i;                                  \
	for (i = 0; i < count; ++i)             \
	{                                       \
	    code                                \
		}                               \
    })

#define array_foreach(b, code)                  \
    ({                                          \
	i64 i, count = array_count(b);          \
	for (i = 0; i < count; ++i)             \
	{                                       \
	    __typeof__((b[0])) item = (b)[i];	\
	    code;                               \
	}                                       \
    })
#define array_foreach_ptr(b, code)			\
    ({							\
	i32 i, count = array_count(b);			\
	for (i = 0; i < count; ++i)			\
	{						\
	    __typeof__((b[0]))* item = &((b)[i]);	\
	    code;					\
	}						\
    })

#define array_filter(b, condition)			\
    ({							\
	__typeof__((b[0])) filteredItem = (b)[0];	\
	i32 i, count = array_count(b);			\
	for (i = 1; i < count; ++i)			\
	{						\
	    __typeof__((b[0])) item = (b)[i];		\
	    if ((condition))				\
	    {						\
		filteredItem = item;			\
	    }						\
	}						\
	filteredItem;					\
    })

#define array_any(b) ({ (b) != NULL ? (array_header((b))->Count > 0) : 0; })
#define array_any_cond(b, cond)				\
    ({							\
	i32 i, res = -1, count = array_count(b);	\
	for (i = 0; i < count; ++i)			\
	{						\
	    __typeof__((b[0])) item = (b)[i];		\
	    if (cond)					\
	    {						\
		res = i;				\
		break;					\
	    }						\
	}						\
	res;						\
    })

#define array_distinct(b, distinctCondition)                            \
    ({                                                                  \
	__typeof__((b)) distinctArr = NULL;                             \
	i32 i, count = array_count(b);                                  \
	for (i = 0; i < count; ++i)                                     \
	{                                                               \
	    __typeof__((b[0])) distinctItem = (b)[i];                   \
	    if (array_index_of((distinctArr), distinctCondition) == -1)	\
	    {                                                           \
		array_push(distinctArr, distinctItem);                  \
	    }                                                           \
	}                                                               \
	distinctArr;                                                    \
    })

#define array_where(b, condition)                               \
    ({                                                          \
	vassert(array_any(b) && "array_where: Empty array!");	\
	__typeof__((b)) resultArr = NULL;                       \
								\
	for (i32 i = 0; i < array_count(b); ++i)                \
	{                                                       \
	    __typeof__((b)[0]) item = (b)[i];                   \
	    if (condition)                                      \
	    {                                                   \
		array_push(resultArr, item);                    \
	    }                                                   \
	}                                                       \
								\
	resultArr;                                              \
    })

#define array_clear(b)				\
    ({                                          \
	if ((b) != NULL)			\
	{                                       \
	    internal_array_clear(b);            \
	}                                       \
    })

#define internal_array_reserve(array, elementsCount,elementSize)        \
    ({ internal_array_reserve_w_alloc(array, elementsCount, elementSize, _memory_allocate); })
#define internal_array_grow(array, elementSize)                         \
    ({ internal_array_grow_w_alloc(array, elementSize, _memory_allocate, _memory_free); })
#define internal_array_copy(src)                                \
    ({ internal_array_copy_w_alloc(src, _memory_allocate); })

void _array_remove_at(void* b, i32 i);

void* internal_array_reserve_w_alloc(const void* array, int elementsCount, int elementSize, void* (*allocDelegate)(size_t n, i32 line, const char* file));
void* internal_array_grow_w_alloc(const void* array, i32 elementSize, void* (*allocDelegate)(size_t n, i32 line, const char* file), void (*freeDelegate)(void* data, i32 line, const char* file));
void* internal_array_copy_w_alloc(const void* src, void* (*allocDelegate)(size_t n, i32 line, const char* file));
void* internal_array_shift_right(void* b, i32 i);
void* internal_array_pop(void* b);
void* internal_array_clear(void* array);

#endif // ARRAY_H

/*

  ###################################
  ###################################
  RingQueue.h (RING_QUEUE)
  ###################################
  ###################################

*/
#ifndef RING_QUEUE_H
#define RING_QUEUE_H

/*
  DOCS(typedef):
  RingQueue only for small size collection, max count = 2^15 (32K)

  RingQueue work as default queue except that we change StartIndex
  every call:ring_queue_add() to ++StartIndex, and after Count == Capacity,
  we set it back to zero, StartIndex = 0, so we replace old member with
  newer one;


  InsertIndex: 0
  0 1 2 3 4 5 6 7 8 9
  a b c d d d d d d d
  ^

  pop()

  InsertIndex: 9
  0 1 2 3 4 5 6 7 8 9
  a * c d d d d d d d
  ^


*/
typedef struct RingQueueHeader
{
    i16 Count;
    i16 Capacity;
    i16 InsertIndex;
    i16 ElementSize;
    void* Buffer;
} RingQueueHeader;

#define ring_queue_header(rq) ((RingQueueHeader*) (((char*)rq) - sizeof(RingQueueHeader)))
#define ring_queue_count(rq) ((rq) ? ring_queue_header(rq)->Count : 0)
#define ring_queue_capacity(rq) ((rq) ? ring_queue_header(rq)->Capacity : 0)
#define ring_queue_insert_index(rq) ((rq)? ring_queue_header(rq)->InsertIndex:0)

void* _ring_queue_new(const void*, i16 capacity, size_t size);
void _ring_queue_free(void* ringQueue);
i64 _ring_queue_push(void*);
i16 _rinq_queue_get_popped_index(void*);
void _ring_queue_pop(void*, i16);
void ring_queue_test();

#define rinq_queue_new(rq, capacity)                                    \
    ({                                                                  \
	vguard(rq == NULL && "RingQueue should be null ring_queue_new!"); \
	(rq) = _ring_queue_new((const void*)rq, capacity, sizeof(*rq));	\
    })

#define ring_queue_push(rq, ...)                \
    ({                                          \
	vguard_not_null((rq));                  \
	i64 ind = _ring_queue_push(rq);         \
	(rq)[ind] = (__VA_ARGS__);              \
    })

#define ring_queue_pop(rq)                              \
    ({                                                  \
	vguard_not_null((rq));                          \
							\
	__typeof__((rq)[0]) item;                       \
	i16 popInd = _rinq_queue_get_popped_index(rq);	\
	item = (rq)[popInd];                            \
	_ring_queue_pop(rq, popInd);                    \
							\
	item;                                           \
    })

#define ring_queue_peek(rq)                             \
    ({                                                  \
	vguard_not_null((rq));                          \
							\
	i32 popInd = _ring_queue_get_poped_index(rq);   \
	(rq)[popInd];                                   \
    })


#endif // RING_QUEUE_H

/*

  ###################################
  ###################################
  PriorityQueue.h
  ###################################
  ###################################

*/
#ifndef PRIORITY_QUEUE_H
#define PRIORITY_QUEUE_H

#define priority_queue_count(b) ({array_count((b));})

#define priority_queue_clear(b)                 \
    ({                                          \
	array_clear(b);                         \
    })

#define priority_queue_index_of(b, newItem)                             \
    ({                                                                  \
	i32 i, result = -1, count = array_count(b);                     \
	for (i = 0; i < count; ++i)                                     \
	{                                                               \
	    __typeof__((b)[0]) item = (b)[i];                           \
	    if (memcmp(&newItem.Value, &item.Value, sizeof(item.Value)) == 0) \
	    {                                                           \
		result = 1;                                             \
		break;                                                  \
	    }                                                           \
	}                                                               \
	result;                                                         \
    })

#define priority_queue_push(b, newItem)					\
    ({									\
	if ((b) == NULL || array_len(b) >= array_cap(b))		\
	{								\
	    (b) = internal_array_grow((const void*)b, sizeof(*b));	\
	}								\
									\
	i32 ind = priority_queue_index_of(b, newItem);			\
	if (ind == -1)							\
	{								\
	    i32 i, count = array_count(b);				\
	    for (i = 0; i < count; ++i)					\
	    {								\
		__typeof__((b)[0]) item = (b)[i];			\
		if (newItem.Priority < item.Priority)			\
		{							\
		    break;						\
		}							\
	    }								\
									\
	    ++array_header(b)->Count;					\
	    array_push_at(b, i, newItem);				\
	    --array_header(b)->Count;					\
	}								\
    })

#define priority_queue_pop(b)                   \
    ({                                          \
	__typeof__((b)[0]) result = (b)[0];	\
	(b) = internal_array_pop((b));		\
	result;                                 \
    })

#define priority_queue_get(b)                   \
    ({                                          \
	vguard_not_null((b));                   \
	__typeof__((b)[0]) result = (b)[0];     \
	result;                                 \
    })


#endif // PriorityQueue.h


/*

  ###################################
  ###################################
  SimpleThread.h
  ###################################
  ###################################

*/
#ifndef SIMPLE_THREAD_H
#define SIMPLE_THREAD_H

#if defined(LINUX_PLATFORM)
#include <pthread.h>
#elif defined(WINDOWS_PLATFORM)
#define _WINSOCKAPI_
#include <Windows.h>
#endif

typedef struct SimpleThread
{
#if defined(WINDOWS_PLATFORM)
//#error "Platform not supported!"
    HANDLE ID;
#elif defined(LINUX_PLATFORM)
    pthread_t ID;
#endif
} SimpleThread;

typedef struct SimpleMutex
{
#if defined(WINDOWS_PLATFORM)
    HANDLE ID;
#elif defined(LINUX_PLATFORM)
    pthread_mutex_t ID;
#endif
} SimpleMutex;

typedef void* (*SimpleThreadDelegate)(void* arg);

SimpleThread simple_thread_create(SimpleThreadDelegate threadFunction, size_t stackSize, void* data);
void simple_thread_attach(SimpleThread* thread);
ResultType simple_thread_cancel(SimpleThread thread);

SimpleMutex simple_thread_mutex_create();
ResultType simple_thread_mutex_destroy(SimpleMutex* simpleMutex);
ResultType simple_thread_mutex_lock(SimpleMutex* simpleMutex);
ResultType simple_thread_mutex_unlock(SimpleMutex* simpleMutex);

#endif // SimpleThread.h

/*

  ###################################
  ###################################
  MemoryAllocator.h
  ###################################
  ###################################

*/
#ifndef MEMORY_ALLOCATOR_H
#define MEMORY_ALLOCATOR_H

#define arena_create(size) _arena_create(size, __LINE__, __FILE__)
#define arena_create_and_set(size) _arena_create_and_set(size, __LINE__, __FILE__)

Arena* _arena_create(size_t size, i32 line, const char* file);
Arena* _arena_create_and_set(size_t size, i32 line, const char* file);
void arena_clear(Arena* pArena);
void arena_destroy(Arena* arena);
void arena_print(Arena* arena);
void arena_format(char* buffer, Arena* arena);

typedef struct MemoryBlock
{
    const char* File;
    i32 Line;
    //NOTE(bies): this is size wo header
    i64 AllocatedSize;
    void* Address;
} MemoryBlock;

#define memory_block_header(b) ((MemoryBlock*) (((char*)b) - sizeof(MemoryBlock)))

typedef enum PrintAllocationSourceType
{
    PrintAllocationSourceType_None = 0,
    PrintAllocationSourceType_Terminal
} PrintAllocationSourceType;

#define memory_allocate(size) ({vguard((size > 0) && "Size should be > 0!!!");_memory_allocate(size, __LINE__, __FILE__);})
#define memory_allocate_type(type) (type*) _memory_allocate(sizeof(type), __LINE__, __FILE__)
#define memory_free(data) _memory_free(data, __LINE__, __FILE__)
#define memory_free_bytes(count) memory_helper_free_bytes(count, __LINE__, __FILE__)
#define memory_reallocate(data, size) memory_helper_reallocate(data, size, __LINE__, __FILE__)

void memory_set_arena(Arena* arena);
Arena* memory_get_arena();
void memory_bind_current_arena();
void memory_unbind_current_arena();
void* _memory_allocate(size_t size, i32 line, const char* file);
MemoryBlock** memory_helper_get_memory_blocks();
void* memory_helper_reallocate(void* data, i32 size, i32 line, const char* file);
void _memory_free(void* data, i32 line, const char* file);
i32 memory_helper_get_allocated_size();
void memory_helper_format_size(char* buf, size_t bytes);
void memory_set_print(PrintAllocationSourceType type);
void* memory_helper_malloc(size_t size, i32 line, const char* file);
void  memory_helper_free(void* data, i32 line, const char* file);

#endif // MEMORY_ALLOCATOR_H MemoryAllocator.h

/*

  ###################################
  ###################################
  String.h
  ###################################
  ###################################

*/
#ifndef STRING_H
#define STRING_H

#define STRING_EMPTY ""
#define STRING_NULL "\0"

#include <stdio.h>
#define string_format(out, format, ...) sprintf(out, format, __VA_ARGS__)

/* Connect this to String */

#define istring_header(istr) ((IString*) (((char*)istr) - sizeof(IString)))
#define istring_length(istr) ((istr) ? istring_header(istr)->Length : -1)
#define istring(str)				\
    ({                                          \
	char* istr = istring_get_buffer(str);   \
	(istr) ? istr : istring_allocate(str);  \
    })

#define istring_ext(stringPool, str)                            \
    ({                                                          \
	char* istr = istring_get_buffer_ext(stringPool, str);   \
	(istr) ? istr : istring_allocate_ext(stringPool, str);  \
    })


typedef struct String
{
    i64 Length;
    char* Buffer;
} String;

/* DOCS(typedef): IString local api */

IStringPool* istring_pool_create();
void istring_pool_destroy(IStringPool*);

IString* istring_new_ext(IStringPool* stringPool, const char* src);
char* istring_allocate_ext(IStringPool* stringPool, const char* src);
char* istring_get_buffer_ext(IStringPool* stringPool, const char* src);
IString* istring_get_ext(IStringPool* stringPool, const char* src);
void istring_free_ext(IStringPool* stringPool, char* istring);
void istring_free_headers_ext(IStringPool* stringPool);

/* DOCS(typedef): Global global api */
IString* istring_new(const char* src);
char* istring_allocate(const char* src);
char* istring_get_buffer(const char* src);
IString* istring_get(const char* src);
IString** istring_get_headers();
void istring_free(char* istring);
void istring_free_headers();


char* string(const char* string);
void string_i32(char* input, i32 number);
char* string_allocate(i32 length);
i32 string_count_of_fast(const char* string, i32 length, char c);
i32 string_count_of(const char* string, char c);
i32 string_count_upper(const char* string);
void string_set(char* string, char c, u32 length);
i64 string_length(const char* str);
size_t string_length_to_delimiters(const char* str, char delimeters[], size_t delimetersLength);
char* string_copy(const char* oth, i32 length);
char* string_copy_bigger(const char* oth, i32 length, i32 bigLength);
char* string_concat(const char* left, const char* right);
char* string_concat_with_space_between(const char* left, const char* right, i32 length, char c);
char* string_concat3(const char* left, const char* middle, const char* right);
char* string_concat3l(const char* left, const char* middle, const char* right, i32 leftLength, i32 middleLength, i32 rightLength);
i32 string_compare(const char* left, const char* right);
i32 string_compare_length(const char* left, const char* right, i64 length);
i32 string_compare_length_safe(const char* left, const char* right, i32 length);
i32 string_compare_w_length(const char* left, const char* right, i32 lengthLeft, i32 lengthRight);
char* string_to_upper(const char* input);
char* string_to_lower(const char* input);
i32 string_index_of(const char* input, char character);
i32 string_index_of_string(const char* input, const char* string);
i32 string_last_index_of(const char* input, char character);
i32 string_last_index_of_string(const char* input, const char* string);
i32 string_last_index_of_upper(const char* input, i32 length);
char* string_substring(const char* input, i32 startIndex);
char* string_substring_length(const char* input, size_t inputLength, i32 length);
char* string_substring_range(const char* input, i32 startIndex, i32 endIndex);
char* string_after(const char* input, i32 length, char c);
char* string_replace_string(char* input, size_t inputLength, char* replaceStr, size_t replaceStrLength, char* newString, size_t newStringLength);

/*
  3, 5
  "01234567" - len: 8
  "01267" - len: 5
*/
char* string_cut(const char* input, u32 begin, u32 end);
char* string_replace_char(char* input, char c);
char* string_trim_char(char* input, size_t length, size_t* newLength, char c);
char** string_split(char* input, char splitCharacter);
char** string_split_length(char* input, size_t inputLength, char splitCharacter);
char* string_join(const char** list, char joinCharacter);
char* string_join_i32(const i32* list, char joinCharacter);
void string_i64(char* input, i64 number);
i32 string_is_integer(char* input, size_t length);
i32 string_to_i32(char* input);
i32 string_to_i32_length(char* input, i32 length);
f32 string_to_f32_length(char* input, size_t length);
f32 string_to_f32(char* input);

void string_i64(char* input, i64 number);
void string_f32(char* input, f32 number);
void string_f64(char* input, f64 number);

force_inline char
char_to_upper(char character)
{
    if (character >= 'a' && character <= 'z')
	return (character - 'a' + 'A');
    else
	return character;
}

force_inline char
char_to_lower(char character)
{
    if (character >= 'A' && character <= 'Z')
	return (character - 'A' + 'a');
    else
	return character;
}

force_inline i32
char_is_upper(char character)
{
    if (character >= 'A' && character <= 'Z')
	return 1;
    return 0;
}
force_inline i32
char_is_lower(char character)
{
    if (character >= 'a' && character <= 'z')
	return 1;
    return 0;
}

force_inline i32
char_is_integer(char character)
{
    if (character >= '0' && character <= '9')
	return 1;
    return 0;
}

#endif // String.h

/*

  ###################################
  ###################################
  New Intern string.h
  ###################################
  ###################################

*/




/*

  ###################################
  ###################################
  WideString.h
  ###################################
  ###################################

*/
#ifndef WIDE_STRING_H
#define WIDE_STRING_H

#define wide_string_woa(buffer)                                         \
    ({                                                                  \
	(WideString) { .Length = wcslen(buffer), .Buffer = buffer };	\
    })

#define wide_string_print(str)				\
    ({							\
	WideString vStr = (str);			\
	for (i32 i = 0; i < vStr.Length; ++i)		\
	{						\
	    if (printf("%C", vStr.Buffer[i]) < 0)	\
	    {						\
		perror("printf");			\
	    }						\
	}						\
    })
#define wide_string_print_line(str)             \
    ({                                          \
	wide_string_print(str);                 \
	printf("\n");                           \
    })
void wide_string_p(WideString ws);
void wide_string_pl(WideString ws);

WideString wide_string(wchar* buffer);
WideString wide_string_new(wchar* buffer, size_t length);
WideString* wide_string_ptr(wchar* input, size_t length);
WideString wide_string_format(const wchar* format, ...);

#define WideString(b, l) (WideString) {.Buffer = b, .Length = l}
#define wide_string_news(buffer)		\
    ({                                          \
	WideString result = {			\
	    .Buffer = buffer,			\
	    .Length = wcslen(buffer)            \
	};                                      \
	result;                                 \
    })

#define wide_string_newa(inbuffer, buffer)      \
    ({                                          \
	i64 len = string_length(buffer);        \
	mbstowcs(inbuffer, buffer, len);        \
	WideString result = {                   \
	    .Buffer = inbuffer,                 \
	    .Length = len                       \
	};                                      \
	result;                                 \
    })

void wide_string_destroy(WideString wideString);
WideString wide_string_utf8(const char* utf8Str);
size_t wide_string_utf8_length(const char* utf8Str);
size_t wide_string_utf8_get_char_size(const char* str);
wchar* wide_string_raw(wchar* buf, size_t length);
i32 wide_string_is_valid(WideString input);
WideString wide_string_concat(WideString first, WideString second);
WideString wide_string_concat_native(wchar* firstNative, wchar* secondNative);
// NOTE(typedef): includes rangeStart, rangeEnd [ startIndex, endIndex ]
WideString wide_string_substring_range(WideString input, i32 startIndex, i32 endIndex);
WideString wide_string_substring(WideString input, i32 startIndex);
WideString* wide_string_split(WideString input, wchar splitCharacter);
WideString* wide_string_split_native(wchar* input, wchar splitCharacter);
i32 wide_string_cequals(WideString str1, wchar* buffer, size_t length);
i32 wide_string_equals(WideString str1, WideString str2);
char* wide_string_as_char(WideString input);
char* wchar_as_char(wchar* input, size_t size);
WideString char_as_wide_string(char* input, size_t length);
WideString* char_as_wide_string_ptr(char* input, size_t length);
wchar* char_as_wchar(char* input, size_t length);
char* wide_string_as_char(WideString input);

/* DOCS(typedef): Additional algorithms */
size_t wide_string_hash(WideString input);

WideString* iwide_string_new_utf8(IWideStringPool*, char*);

void wide_string_test();

#endif // WideString.h

/*

  ###################################
  ###################################
  GlobalHelpers.h
  ###################################
  ###################################

*/
#ifndef GLOBAL_HELPERS_H
#define GLOBAL_HELPERS_H

#define DOUBLE_ARRAY_CREATE(r, c, type) (type**)_double_array_create(r, c, sizeof(type))
#define DOUBLE_ARRAY_DESTROY(darr, rows) _double_array_destroy((size_t*)darr, rows)

void* _double_array_create(i32 rows, i32 cols, size_t size);
void* _double_array_destroy(size_t* darr, i32 rows);
i32 string_get_next_i32(char* stream, i32 skipChars, i32* index);

#endif // GlobalHelpers.h

/*

  ###################################
  ###################################
  StringBuilder.h
  ###################################
  ###################################

*/
#ifndef STRING_BUILDER_H
#define STRING_BUILDER_H

#define StringBuilderAllocateDelegate(size) memory_allocate(size)
#define StringBuilderFreeDelegate(data) memory_free(data)

typedef struct StringBuilderHeader
{
    i64 Count;
    i64 Capacity;
    char* Buffer;
} StringBuilderHeader;

#define START_ALLOCATION_SIZE 257

#define sb_string_int_to_string(input, number)                          \
    ({                                                                  \
	i8 isNumberNegative = ((number < 0) ? 1 : 0);                   \
	i32 i, rank = sb_string_number_rank(number), numberLength = rank + isNumberNegative + 1; \
									\
	if (isNumberNegative)                                           \
	{                                                               \
	    input[0] = '-';                                             \
	}                                                               \
									\
	for (i = isNumberNegative; i < numberLength; ++i)               \
	{                                                               \
	    input[i] = sb_string_number_of_digit(number, rank) + 48;	\
	    --rank;                                                     \
	}                                                               \
    })
#define sb_string_i32_to_string(input, number) sb_string_int_to_string(input, number)
#define sb_string_i64_to_string(input, number) sb_string_int_to_string(input, number)
#define sb_string_f64_to_string(input, number) sprintf(input, "%f", number)

#define string_builder_header(s) ((StringBuilderHeader*) (((char*)s) - sizeof(StringBuilderHeader)))
#define string_builder_count(s) ((s) != NULL ? string_builder_header((s))->Count : 0)
#define string_builder_capacity(s) ((s) != NULL ? string_builder_header((s))->Capacity : 0)
#define string_builder_buffer(s) ((s) != NULL ? string_builder_header((s))->Buffer : NULL)
#define string_builder_free(s) StringBuilderFreeDelegate(string_builder_header((s)))

#define string_builder_clear(s)			\
    ({						\
	vguard_not_null(s);			\
	memset(s, 0, string_builder_count(s));	\
	string_builder_header(s)->Count = 0;	\
    })

#define string_builder_appendc(s, c)					\
    ({									\
	string_builder_append_base((s), 1);				\
	StringBuilderHeader* header = string_builder_header((s));	\
	header->Buffer[header->Count] = (c);				\
	++header->Count;						\
    })
#define string_builder_appends(s, str)                                  \
    ({                                                                  \
	vguard_not_null(str && "string_builder_appends (s, NULL) !!!"); \
	i32 strLength = strlen((str));                                  \
	string_builder_append_base((s), strLength);                     \
	StringBuilderHeader* header = string_builder_header((s));       \
	memcpy((header->Buffer + header->Count), (str), strLength*sizeof(*(s))); \
	header->Count += strLength;                                     \
    })
#define string_builder_appendf(s, f, ...)                       \
    ({                                                          \
	(s) = _string_builder_appendf((s), (f), ##__VA_ARGS__);	\
    })
#define string_builder_append_base(s, count)                    \
    {                                                           \
	StringBuilderHeader* hdr = string_builder_header((s));	\
								\
	if ((s) == NULL)                                        \
	{                                                       \
	    (s) = _string_builder_new();                        \
	}                                                       \
	else if ((hdr->Count + count) >= hdr->Capacity)         \
	{                                                       \
	    size_t newCapacity = 2 * (hdr->Count + count) + 1;	\
	    (s) = _string_builder_grow(s, newCapacity);         \
	}                                                       \
    }

char* _string_builder_new();
char* _string_builder_grow(char* builder, size_t newCapacity);
char* _string_builder_appendf(char* builder, const char* format, ...);

#endif // StringBuilder.h

/*

  ###################################
  ###################################
  HashTable.h
  ###################################
  ###################################

  Can we use memcpr() ???
*/
#ifndef HASH_TABLE_H
#define HASH_TABLE_H

#define HashTableAllocate(size) memory_allocate(size)
#define HashTableFree(d) memory_free(d)

/*
  Hash Table Statistics (for profiling purposes)
*/
#if HASH_TABLE_PROFILING == 1
typedef struct TableStatistics
{
    i32 PutAttempt;
    i32 GetAttempt;
} TableStatistics;
TableStatistics table_get_statistics();
#endif // HASH_TABLE_PROFILING

typedef struct TableHeader
{
    size_t ElementSize;
    i64 Count;
    i64 Capacity;
    i64 Index;
    i32 NextPrime;
    void* Buffer;
} TableHeader;

#define table_header(b) ((TableHeader*) (((char*)b) - sizeof(TableHeader)))
#define table_count(b) ((b != NULL) ? table_header(b)->Count : 0)
#define table_capacity(b) ((b != NULL) ? table_header(b)->Capacity : 0)
#define table_element_size(b) ((b != NULL) ? table_header(b)->ElementSize : 0)
#define table_index(b) ((b != NULL) ? table_header(b)->Index : 0)
#define table_next_prime(b) ((b != NULL) ? table_header(b)->NextPrime : 0)
#define table_free(b) ((b) ? HashTableFree(table_header(b)) : 0)
#define shash_free(table) table_free((table))
#define hash_free(table) table_free((table))

/*
  Base
*/

force_inline i32
i32_comparer(i32 key)
{
    return key != -1;
}

force_inline i32
string_comparer(const char* key)
{
    return key != NULL;
}

force_inline i32
wstr_def_cmp(WideString ws)
{
    return ws.Buffer != NULL;
}

#define base_ghash_put(table, key, value, hashPutDelegate, defValComparer, isInt) \
    ({                                                                  \
	TableHeader* hdr = table_header(table);                         \
									\
	if (table == NULL)                                              \
	{                                                               \
	    table = _table_new(table, sizeof(*table), isInt ? -1 : 0);	\
	}                                                               \
	else if (hdr->Count >= i64(0.7 * hdr->Capacity))                \
	{                                                               \
	    __typeof__((table)) newTable = _table_grow((table), sizeof(*table), isInt ? -1 : 0); \
	    TableHeader* newHeader = table_header((newTable));          \
	    for (i32 j = 0; j < hdr->Capacity; ++j)                     \
	    {                                                           \
		if (defValComparer((table)[j].Key))                     \
		{                                                       \
		    hashPutDelegate((newTable),(table)[j].Key);         \
		    (newTable)[newHeader->Index].Key = ((table)[j].Key); \
		    (newTable)[newHeader->Index].Value = ((table)[j].Value); \
		}                                                       \
	    }                                                           \
									\
	    table_free(table);                                          \
									\
	    table = newTable;                                           \
	}                                                               \
									\
	hashPutDelegate(table, key);                                    \
	hdr = table_header(table);                                      \
	(table)[(hdr->Index)].Key   = (key);                            \
	(table)[(hdr->Index)].Value = (value);                          \
    })

#define base_ghash_get(table, key, hashGetDelegate)	\
    ({							\
	hashGetDelegate(table, key);			\
	TableHeader* hdr = table_header(table);		\
	((table) != NULL && (hdr->Index != -1))		\
	    ? (table)[hdr->Index].Value			\
	    : ((__typeof__(table[0].Value)) { 0 });	\
    })

void* _table_new(void* table, size_t elemSize, i32 defVar);
void* _table_grow(void* table, size_t elemSize, i32 defVar);


/*
  DOCS(typedef): String Hash Table (string Key)
*/
#define shash_put(table, key, value)                                    \
    ({                                                                  \
	base_ghash_put(table, key, value, _base_shash_put, string_comparer, 0); \
    })
#define shash_get(table, key)                           \
    ({                                                  \
	base_ghash_get(table, key, _base_shash_get);	\
    })
#define shash_geti(table, key)                  \
    ({                                          \
	i64 ind;                                \
	if (table != NULL)                      \
	{                                       \
	    _base_shash_get(table, key);        \
	    ind = table_header(table)->Index;	\
	}                                       \
	else                                    \
	{                                       \
	    ind = -1;                           \
	}                                       \
						\
	ind;                                    \
    })

void _base_shash_put(void* table, const char* key);
void _base_shash_get(void* table, const char* key);

/*
  DOCS(typedef): Int Hash Table (int Key)
*/
#define hash_put(table, key, value)                                     \
    ({                                                                  \
	base_ghash_put((table), (key), (value), _base_hash_put, i32_comparer, 1); \
    })
#define hash_get(table, key)                            \
    ({                                                  \
	base_ghash_get((table), (key), _base_hash_get);	\
    })
// BUG(typedef): Енто костыль, убрать, нужно юзать get а потом чекать Index == -1
#define hash_geti(table, key)                   \
    ({                                          \
	i64 ind;                                \
	if (table != NULL)                      \
	{                                       \
	    _base_hash_get(table, key);         \
	    ind = table_header(table)->Index;	\
	}                                       \
	else                                    \
	{                                       \
	    ind = -1;                           \
	}                                       \
						\
	ind;                                    \
    })

void* _base_hash_put(void* table, i32 key);
void* _base_hash_get(void* table, i32 key);

/*
  DOCS(typedef): Wide Hash Table (WideString Key)
*/
#define whash_put(table, key, value)                                    \
    ({                                                                  \
	base_ghash_put((table), (key), (value), _base_whash_put, wstr_def_cmp, 0); \
    })
#define whash_get(table, key)					\
    ({								\
	base_ghash_get((table), (key), _base_whash_get);	\
    })
#define whash_geti(table, key)			\
    ({                                          \
	i64 ind = -1;                           \
	if (table != NULL)                      \
	{                                       \
	    _base_whash_get(table, key);        \
	    ind = table_header(table)->Index;   \
	}                                       \
						\
	ind;                                    \
    })

size_t whash(WideString key);
void _base_whash_put(void* table, WideString key);
void _base_whash_get(void* table, WideString key);

#endif // HashTable.h

/*

  #####################################
  #####################################
  IO.h
  #####################################
  #####################################

*/
#ifndef IO_H
#define IO_H

char* file_read_string_ext(const char* filePath, size_t* length);
char* file_read_string(const char* filePath);
char* file_get_name_with_extension(const char* path);
void file_write_string(const char* filePath, char* data, size_t len);
void file_write_bytes(const char* filePath, u8* data, size_t len);
i32 file_write_string_exe(const char* filePath, char* data, size_t len);
void file_append_string(const char* filePath, char* data, size_t len);
u8* file_read_bytes_ext(const char* filePath, size_t* sizePtr);
u8* file_read_bytes(const char* filePath);
i32 file_get_size(const char* filePath);
i32 platform_directory_create(const char* name);

#endif // IO.h

/*

  #####################################
  #####################################
  Path.h
  #####################################
  #####################################

*/
#ifndef PATH_H
#define PATH_H

typedef struct IElement
{
    i32 AbsolutePathLength;
    i32 NameLength;
    i32 DirLength;
    char* AbsolutePath;
    char* Directory;
    char* Name;
    // NOTE(bies); Extension is part of AbsolutePath
    char* NameWithExtension;
    // NOTE(bies); Extension is part of Filename
    char* Extension;
} IElement;

#define ielement_header(e) ((IElement*) (((char*)e) - sizeof(IElement)))
#define ielement_absolute_length(e) ((e != NULL)? ielement_header(e)->AbsolutePathLength : 0)
#define ielement_name_length(e) ((e != NULL)? ielement_header(e)->NameLength : 0)
#define ielement_directory_length(e) ((e != NULL)? ielement_header(e)->DirLength : 0)
#define ielement_absolute_path(e) ((e != NULL)? ielement_header(e)->AbsolutePath : 0)
#define ielement_directory(e) ((e != NULL)? ielement_header(e)->Directory : 0)
#define ielement_name(e) ((e != NULL)? ielement_header(e)->Name : 0)
#define ielement_name_with_extension(e) ((e != NULL)? ielement_header(e)->NameWithExtension : 0)
#define ielement_extension(e) ((e != NULL)? ielement_header(e)->Extension : 0)

enum Path
{
    PATH_IS_SOMETHING = 0,
    PATH_IS_FILE,
    PATH_IS_DIRECTORY
};

char* ielement(const char* directory, const char* name);
void ielement_free_all();
u8 path(const char* path);
char* path_get_home_directory();
const char* path_get_extension(const char* path);
const char* path_get_name(const char* path);
char* path_get_name_wo_extension(const char* path);
char* path_get_directory(const char* path);
char* path_combine(const char* left, const char* right);
char* path_combine3(const char* left, const char* mid, const char* right);
char* path_combine_directory_and_name(const char* path, char* name);
const char* path_combine_interning(const char* left, const char* right);
i32 path_contains_slash(const char* path, i64 pathLength);
const char* path_get_current_directory();
char* path_get_absolute(char* path);
i32 path_is_file_exist(const char* path);
i32 path_is_directory_exist(const char* path);
char* path_get_filename(const char* path);
const char* path_get_filename_interning(const char* path);
char* path_get_prev_directory(const char* currentDirectory);
const char* path_get_prev_directory_interning(const char* currentDirectory);
i32 path_directory_create(const char* path);
const char** path_directory_get_files(const char* directory);
const char** path_directory_get_directories(const char* directory);
i32 path_is_inside(const char* path, const char* item);

size_t path_get_last_access_time_raw(const char* path);
size_t path_get_last_modification_time_raw(const char* path);
size_t path_get_last_creation_time_raw(const char* path);


#if defined(LINUX_PLATFORM)

#include <assert.h>
#include <unistd.h>
#include <pwd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#define PATH_SEPARATOR_STRING "/"
#define ROOT_DIRECTORY "/"

#elif defined(WINDOWS_PLATFORM)

#ifndef PATH_SEPARATOR_STRING
#define PATH_SEPARATOR_STRING "/"
#endif

#ifndef ROOT_DIRECTORY
#define ROOT_DIRECTORY "/"
#endif

#else
#error "Platform unsupported"
#endif

#endif //Path.h

/*

  #####################################
  #####################################
  Profiler.h
  #####################################
  #####################################

*/
#ifndef PROFILER_H
#define PROFILER_H

/*

  USAGE(bies):

  TimeState state;
  profiler_start(&state);
// do smth we want profile
profiler_end(&state);

state.Result; <- time in ns
i64 ms = profiler_get_microseconds(&state);

*/

#define PROFILER_NS_TO_S(ns)   (ns / (1000 * 1000 * 1000))
#define PROFILER_NS_TO_MS(ns)  (ns / (1000 * 1000))
#define PROFILER_NS_TO_MCS(ns) (ns / (1000))

typedef enum ProfilerTimeType
{
    PROFILER_TIME_NS = 0,
    PROFILER_TIME_MCS,
    PROFILER_TIME_MS,
    PROFILER_TIME_S,
} ProfilerTimeType;

#define _MAKE_U(a, b) a##b
#define MAKE_U(a, b) _MAKE_U(a, b)
#define MAKE_UNIQUE(a) MAKE_U(a##_, __LINE__)

#define profiler_calc(x)                        \
    {                                           \
	TimeState state;                        \
	profiler_start(&state);                 \
	x;                                      \
	profiler_end(&state);                   \
	printf("Time for executing: %s\n", #x);	\
	profiler_print(&state);                 \
    }

#ifdef LINUX_PLATFORM

#include <time.h>
typedef struct TimeState
{
    struct timespec Start;
    struct timespec End;
    i64 Result;
} TimeState;

void profiler_start(TimeState* state);
void profiler_end(TimeState* state);

#elif defined(WINDOWS_PLATFORM)

// NOTE: check if delta in nanoseconds

typedef struct TimeState
{
    LARGE_INTEGER Start;
    LARGE_INTEGER End;
    i64 Result;
} TimeState;

void profiler_start(TimeState* state);
void profiler_end(TimeState* state);

#endif

ProfilerTimeType profiler_get_time_type(TimeState* state);
i64 profiler_get_nanoseconds(TimeState* state);
i64 profiler_get_microseconds(TimeState* state);
i64 profiler_get_milliseconds(TimeState* state);
i64 profiler_get_seconds(TimeState* state);
f64 profiler_get_microseconds_as_float(TimeState* state);
f64 profiler_get_milliseconds_as_float(TimeState* state);
f64 profiler_get_seconds_as_float(TimeState* state);
void profiler_print(TimeState* state);
void profiler_print_as_float(TimeState* state);
char* profiler_get_string(TimeState* state);
char* profiler_get_string_as_float(TimeState* state);

#endif // Profiler.h

/*

  #####################################
  #####################################
  SimpleImage.h
  #####################################
  #####################################

*/

typedef struct SimpleImage
{
    /*
      NOTE(typedef): It always rgba, so channels always 4
    */
    i32 Width;
    i32 Height;
    void* Data;
} SimpleImage;

SimpleImage* simple_image_create(void* data, i32 width, i32 height, i32 channels);
SimpleImage* simple_image_load_from_disk(const char* path);
void simple_image_write_to_disk(SimpleImage* simpleImage, const char* path);
void simple_image_destroy(SimpleImage* simpleImage);

// End of SimpleImage.h

/*

  #####################################
  #####################################
  SimpleSocket.h
  #####################################
  #####################################

*/

#if defined(LINUX_PLATFORM)
#include <netinet/in.h> // sockaddr_in
#elif defined(WINDOWS_PLATFORM)
//#define WIN32_LEAN_AND_MEAN
#define _WINSOCKAPI_
#include <windows.h>
#include <winsock2.h>
//#include <ws2tcpip.h>
#endif

/*
  DOCS(typedef): Все данные по сети передаются как big endian,
  так что их нужно htonl() чтобы конвертнуть назад в little endian.
  Данные функции, как и все api-функции работают в стандартной архитектуре,
  то есть little endian
*/
u32 ip_as_integer(u8 ip3, u8 ip2, u8 ip1, u8 ip0);
void ip_as_string(char str[], u32 ip);
char* ip_to_user_string(u32 address);
void ip_print(u32 address);
char* ip_to_string(u32 address);
u32 htonf(f32 f);
f32 ntohf(u32 p);

typedef enum ConnectionProtocolType
{
    ConnectionProtocolType_UDP = 0,
    ConnectionProtocolType_TCP,
} ConnectionProtocolType;


typedef enum IpType
{
    IpType_V4 = 0,
    IpType_V6,
} IpType;

typedef struct Socket
{
    i32 IsAsync;
#if defined(LINUX_PLATFORM)
    i32 Descriptor;
    struct sockaddr_in Address;
    struct sockaddr_in ServerAddress;
#elif defined(WINDOWS_PLATFORM)
    SOCKET Descriptor;
    struct sockaddr_in Address;
    struct sockaddr_in ServerAddress;
#endif
} Socket;

typedef enum SocketType
{
    SocketType_UDP = 0,
    SocketType_TCP,
} SocketType;

typedef struct SocketAddress
{
    u32 Ip;
    u16 Port;
} SocketAddress;

typedef struct SocketSettings
{
    SocketType Type;
    IpType IpType;
    i32 Protocol;

    SocketAddress Address;
    SocketAddress ServerAddress;
} SocketSettings;

Socket socket_new(SocketSettings settings);
Socket socket_new_address(char* address, SocketType socketType);
i32 socket_is_invalid(Socket* pSocket);
i32 socket_is_valid(Socket* pSocket);

i32 socket_connect(Socket* pSocket);
i32 socket_connect_to(Socket* pSocket, u32 ip, u16 port);
void socket_make_async(Socket* pSocket);
i32 socket_bind(Socket* pSocket);
Socket socket_accept(Socket* pSocket);
i32 socket_listen(Socket* pSocket, i32 clientsCount);
void socket_close(Socket* pSocket);

// DOCS(typedef): TCP Operations
i32 socket_send_ext(i32 descriptor, void* data, size_t size, i32 flags);
i32 socket_send(Socket* pSocket, void* data, size_t size);
i32 socket_recv_ext(i32 descriptor, void* data, size_t size, i32 flags);
i32 socket_recv(Socket* pSocket, void* data, size_t size);
i32 socket_peek(Socket* pSocket, void* data, size_t size);

// DOCS(typedef): UDP Operations
i32 socket_send_to_ext(i32 descriptor, void* data, size_t size, i32 flags, struct sockaddr_in* pAddress);
i32 socket_recv_from_ext(i32 descriptor, void* data, size_t size, i32 flags, struct sockaddr_in* pAddress);
i32 socket_send_to(Socket* pSocket, void* data, size_t size);
i32 socket_recv_from(Socket* pSocket, void* data, size_t size);
i32 socket_peek_from(Socket* pSocket, void* data, size_t size);

u32 socket_parse_ipv4(char* str, size_t length);
u16 socket_parse_port(char* str, size_t length);
i32 socket_parse_ip_port(char* address, u32* pIp, u16* pPort);
struct sockaddr_in socket_address_v4(u32 ip, u16 port);

i32 socket_compare_address(Socket* a, Socket* b);
i32 socket_compare_raw_address(struct sockaddr_in* a, struct sockaddr_in* b);
i32 socket_compare_server_address(Socket* a, Socket* b);

// DOCS: Debug only
void socket_set_debug_mode();
void socket_set_release_mode();

// NOTE: WINDOWS ONLY
void socket_init();
void socket_deinit();

// End of SimpleSocket.h


/*

  ###################################
  ###################################
  SimpleStandardLibrary.c
  ###################################
  ###################################

*/
#if defined(SSL_IMPLEMENTATION)

#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <time.h>

#if defined(LINUX_PLATFORM)
#include <fcntl.h>
#elif defined(WINDOWS_PLATFORM)
#include <Windows.h>
#endif

/*

  ###################################
  ###################################
  Environment.c
  ###################################
  ###################################

*/
void
sleep_for(size_t seconds)
{
    size_t waitTo = time(0) + seconds;
    while (time(0) < waitTo);
}

#if defined(LINUX_PLATFORM)
#include <sys/resource.h>
void
linux_set_current_stack_size(i64 currentBytesNumber, i64 maxBytesNumber)
{
    struct rlimit resource_limit;
    i32 result = getrlimit(RLIMIT_STACK, &resource_limit);
    if (result < 0)
    {
	GERROR("Get RLIMIT_STACK error!\n");
	return;
    }
    else
    {
	GLOG("Resource limit: %ld (max: %ld)\n", (i64)resource_limit.rlim_cur, (i64)resource_limit.rlim_max);
	resource_limit.rlim_cur = (i64) currentBytesNumber;
	resource_limit.rlim_max = (i64) maxBytesNumber;
	result = setrlimit(RLIMIT_STACK, &resource_limit);
	if (result == 0)
	{
	    GLOG("New Resource limit: %ld mb (max: %ld)\n", i64(TOMB(resource_limit.rlim_cur)), i64(resource_limit.rlim_max));
	}
    }
}
#endif



/*

  ###################################
  ###################################
  Logger.c
  ###################################
  ###################################

*/

//TODO(typedef): Replace printf with more low level func

struct LoggerSettings
{
    LoggerType Type;
    LoggerFlags Flags;
    const char* Path;
};

static struct LoggerSettings gLoggerSettings = {
    .Type = LoggerType_Terminal,
    .Flags = LoggerFlags_Full,
    .Path = "default.log"
};

void
logger_init()
{
    if (path_is_file_exist(gLoggerSettings.Path))
    {
	remove(gLoggerSettings.Path);
    }
}

void
logger_to_terminal()
{
    gLoggerSettings.Type = LoggerType_Terminal;
}
void
logger_to_file()
{
    gLoggerSettings.Type = LoggerType_File;
}

void
logger_output(const char* levelMessage, const char* format, const char* file, i32 line, ...)
{
    FILE* pOutputHandle;

    typedef int (*WriteToLogDelegate)(FILE* pHandle, const char* format, ...);
    WriteToLogDelegate write_to_log = fprintf;
    char cleanedBuf[64] = {};
    static char* sb = NULL;

    if (gLoggerSettings.Type == LoggerType_Terminal)
    {
	pOutputHandle = stdout;
    }
    else if (gLoggerSettings.Type == LoggerType_File)
    {
	pOutputHandle = fopen(gLoggerSettings.Path, "a+");

	// Clean message from color codes
	char* ptr = (char*) levelMessage;
	while (*ptr != 0)
	{
	    if (*ptr == '[' && char_is_upper(*(ptr + 1)))
	    {
		char* oth = ptr;
		while (*oth != ']' && *oth != 0)
		    ++oth;
		size_t cleanLength = (size_t)(oth - ptr + 1);
		memcpy(cleanedBuf, ptr, cleanLength);
		break;
	    }

	    ++ptr;
	}

	levelMessage = (const char*) cleanedBuf;

    }
    else
    {
	GERROR("No impl for file logger for now!\n");
	vguard_not_impl();
    }


    vassert(gLoggerSettings.Flags != LoggerFlags_None && "Logger flags should be set to some value!");

    if (gLoggerSettings.Flags & LoggerFlags_PrintType)
    {
	string_builder_appendf(sb, "%s ", levelMessage);
    }

    i32 isPrintFile = gLoggerSettings.Flags & LoggerFlags_PrintFile;
    if (isPrintFile)
    {
	string_builder_appendf(sb, "file: %s", file);
    }

    i32 isPrintShortFile = gLoggerSettings.Flags & LoggerFlags_PrintShortFile;
    if (isPrintShortFile)
    {
	string_builder_appendf(sb, "f:%s", file);
    }

    if (gLoggerSettings.Flags & LoggerFlags_PrintLine)
    {
	if (isPrintFile || isPrintShortFile)
	{
	    string_builder_appends(sb, ", ");
	}

	string_builder_appendf(sb, "line: %d ", line);
    }

    if (gLoggerSettings.Flags & LoggerFlags_PrintShortLine)
    {
	if (isPrintFile || isPrintShortFile)
	{
	    string_builder_appends(sb, ", ");
	}

	string_builder_appendf(sb, "l:%d ", line);
    }

    //  LoggerFlags_PrintTime     = 1 << 5,
    // LoggerFlags_PrintShortTime = 1 << 6,
    if (gLoggerSettings.Flags & LoggerFlags_PrintTime)
    {
	SimpleTime time;
	simple_time_now(&time);

	char timeBuf[256] = {};
	snprintf(timeBuf, 256, "time: %0.2d:%0.2d:%0.2d %0.2d.%0.2d.%0.4d ",
		 time.Hours, time.Minutes, time.Seconds,
		 time.Day, time.Month, time.Year);

	string_builder_appends(sb, timeBuf);
    }
    else if (gLoggerSettings.Flags & LoggerFlags_PrintShortTime)
    {
	SimpleTime time;
	simple_time_now(&time);

	char timeBuf[256] = {};
	snprintf(timeBuf, 256, "time: %0.2d:%0.2d:%0.2d ",
		 time.Hours, time.Minutes, time.Seconds);

	string_builder_appends(sb, timeBuf);
    }

    va_list variadicList;
    va_start(variadicList, line);

    if (gLoggerSettings.Type == LoggerType_Terminal)
    {
	printf("%s", sb);
	vprintf(format, variadicList);
    }
    else if (gLoggerSettings.Type == LoggerType_File)
    {
	vfprintf(pOutputHandle, format, variadicList);

	fclose(pOutputHandle);
    }

    va_end(variadicList);

    string_builder_clear(sb);
}

void
logger_set_terminal()
{
    gLoggerSettings.Type = LoggerType_Terminal;
}

void
logger_set_file()
{
    gLoggerSettings.Type = LoggerType_File;
}

void
logger_set_flags(LoggerFlags flags)
{
    gLoggerSettings.Flags = flags;
}

LoggerFlags
logger_get_flags()
{
    return gLoggerSettings.Flags;
}

#if 0

void
logger_set_short()
{
    LogOutputType* type = logger_get_type();

    LogOutputType value = 0;
    value |= LogOutputType_Long;
    value &= ~LogOutputType_Short;

    *type = LogOutputType_Short;
}

void
logger_set_long()
{
    LogOutputType* type = logger_get_type();

    LogOutputType value = 0;
    value |= LogOutputType_Long;
    value &= ~LogOutputType_Short;

    *type = value;
}

LogOutputType*
logger_get_type()
{
    static LogOutputType logOutputType = LogOutputType_Terminal;
    return &logOutputType;
}
#endif

/*

  ###################################
  ###################################
  Time.c
  ###################################
  ###################################

*/

void
simple_time_now(SimpleTime* pTime)
{
    time_t rawtime = time(NULL);

    if (rawtime == -1)
    {
	return;
    }

    /* struct tm { */
/*     int tm_sec;         /\* seconds *\/ */
/*     int tm_min;         /\* minutes *\/ */
/*     int tm_hour;        /\* hours *\/ */

/*     int tm_mday;        /\* day of the month *\/ */
/*     int tm_mon;         /\* month *\/ */
/*     int tm_year;        /\* year *\/ */
/*     int tm_wday;        /\* day of the week *\/ */
/*     int tm_yday;        /\* day in the year *\/ */
/*     int tm_isdst;       /\* daylight saving time *\/ */
/* }; */

    struct tm* ptr = localtime(&rawtime);

    pTime->DayOfTheWeek = ptr->tm_wday;

    pTime->Seconds = ptr->tm_sec;
    pTime->Minutes = ptr->tm_min;
    pTime->Hours = ptr->tm_hour;

    pTime->Day = ptr->tm_mday;
    pTime->Month = ptr->tm_mon + 1;
    pTime->Year = 1900 + ptr->tm_year;
}

void
simple_time_print(SimpleTime* pTime)
{
    GINFO("%d:%d:%d %0.2d.%0.2d.%0.4d\n", pTime->Hours, pTime->Minutes, pTime->Seconds, pTime->Day, pTime->Month, pTime->Year);
}

/*

  ###################################
  ###################################
  SimpleTimer.c
  ###################################
  ###################################

*/

i32
simple_timer_interval(SimpleTimerData* pTimerData, f32 timestep)
{
    pTimerData->_CountSeconds += timestep;
    if (pTimerData->_CountSeconds >= pTimerData->WaitSeconds)
    {
	pTimerData->_CountSeconds = 0.0;
	return 1;
    }

    return 0;
}

void
simple_timer_reset(SimpleTimerData* pTimerData)
{
    pTimerData->_CountSeconds = 0.0;
}

void
simple_timer_sleep(i64 sec)
{
    simple_timer_mssleep(1000 * sec);
}

void
simple_timer_mssleep(i64 ms)
{
#if defined(LINUX_PLATFORM)
    simple_timer_usleep(1000 * ms);
#elif defined(WINDOWS_PLATFORM)
    Sleep(ms);
#endif
}

void
simple_timer_usleep(i64 usec)
{
#if defined(LINUX_PLATFORM)
    usleep(usec);
#elif defined(WINDOWS_PLATFORM)
    HANDLE timer;
    LARGE_INTEGER ft;

    ft.QuadPart = -(10 * usec); // Convert to 100 nanosecond interval, negative value indicates relative time

    timer = CreateWaitableTimer(NULL, TRUE, NULL);
    SetWaitableTimer(timer, &ft, 0, NULL, NULL, 0);
    WaitForSingleObject(timer, INFINITE);
    CloseHandle(timer);
#endif
}

/*

  ###################################
  ###################################
  Bitset.c
  ###################################
  ###################################

*/
#define bitset_devide_to_bigger(bv) ((((bv) % 64) == 0) ? ((bv) / 64) : ((bv) / 64 + 1))

Bitset
bitset_new(i32 bitsCount)
{
    i32 chunksCount = bitset_devide_to_bigger(bitsCount);

    size_t size = chunksCount * sizeof(u64);
    u64* chunks = (u64*) memory_allocate(size);
    memset((void*)chunks, 0, size);

    Bitset bs = {
	.Chunks = chunks,
	.ChunksCount = chunksCount
    };

    return bs;
}

void
bitset_reset(Bitset cbs)
{
    size_t size = cbs.ChunksCount * sizeof(u64);
    memset((void*)cbs.Chunks, 0, size);
}

void
bitset_set(Bitset cbs, i32 bit)
{
    i32 chunkInd = bit / 65;
    vassert(chunkInd < cbs.ChunksCount && "Bit out of range!");
    i32 chunkBit = bit % 65;
    cbs.Chunks[chunkInd] |= (1 << chunkBit);
}
void
bitset_unset(Bitset cbs, i32 bit)
{
    i32 chunkInd = bit / 65;
    vassert(chunkInd < cbs.ChunksCount && "Bit out of range!");
    i32 chunkBit = bit % 65;
    cbs.Chunks[chunkInd] &= ~(1 << chunkBit);
}

i32
bitset_get(Bitset cbs, i32 bit)
{
    i32 chunkInd = bit / 64;
    vassert(chunkInd < cbs.ChunksCount && "Bit out of range!");
    i32 chunkBit = bit % 65;
    i32 value = cbs.Chunks[chunkInd] & (1 << chunkBit);
    return value;
}

void
bitset_test()
{
    Bitset bitset = bitset_new(247);
    bitset_set(bitset, 18);
    bitset_set(bitset, 1);
    bitset_set(bitset, 2);
    bitset_set(bitset, 3);
    bitset_set(bitset, 8);
    bitset_set(bitset, 19);
    bitset_set(bitset, 19);
    bitset_set(bitset, 60);
    bitset_set(bitset, 61);
    bitset_set(bitset, 62);
    bitset_set(bitset, 63);
    bitset_set(bitset, 64);
    bitset_set(bitset, 78);
    bitset_set(bitset, 129);
    bitset_set(bitset, 223);
    bitset_set(bitset, 246);
    GINFO("Chunks Count: %d\n", bitset.ChunksCount);
    GINFO("1 << 0 == %d\n", 1 << 0);
    for (i32 i = 0; i < bitset.ChunksCount; ++i)
    {
	i64 chunk = bitset.Chunks[i];
	printf("Chunk[%d]: ", i);
	for (i32 b = 0; b < 64; ++b)
	{
	    i32 bit = (chunk & (1LL << b)) > 0 ? 1 : 0;
	    if (bit == 1)
		printf("%d[%d] ", bit, b);
	}

	printf("\n");
    }
}

/*

  ###################################
  ###################################
  FPSCounter.c
  ###################################
  ###################################

*/
FpsCounter
fps_counter_create(GetTimeDelegate getTimeDelegate)
{
    FpsCounter counter = {0};
    counter.Fps = 0;
    counter.Frames = 0;
    counter.Since = 0.0;
    counter.GetTime = getTimeDelegate;
    return counter;
}

void
fps_counter_update(FpsCounter* fpsCounter)
{
#define SimpleRound(x) ( (i32) ((x) + 1) )

    ++fpsCounter->Frames;
    f64 now = fpsCounter->GetTime();
    f64 elapsed = now - fpsCounter->Since;
    if (elapsed >= 1)
    {
	fpsCounter->Fps = SimpleRound(fpsCounter->Frames / elapsed);
	fpsCounter->Frames = 0;
	fpsCounter->Since = now;
    }
}

/*

  ###################################
  ###################################
  Array.c
  ###################################
  ###################################

*/

void
array_remove(void* array, void* itemPtr)
{
    vguard_not_null(array);
    ArrayHeader* pHdr = array_header(array);

    i64 count = array_count(array);
    for (i64 i = 0; i < count; ++i)
    {
	void* iterPtr = (void*) (((char*)array) + pHdr->ElementSize * i);
	if (memcmp(iterPtr, itemPtr, pHdr->ElementSize) == 0)
	{
	    _array_remove_at(array, i);
	    break;
	}
    }

}

void
_array_remove_at(void* array, i32 i)
{
    ArrayHeader* pHdr = array_header(array);

    void* iterPtr = (void*) (((char*)array) + pHdr->ElementSize * i);
    memset(iterPtr, 0, pHdr->ElementSize);

    if ((pHdr->Count > 1) && (i < (pHdr->Count - 1)))
    {
	void* nextIterPtr = (void*) (((char*)iterPtr) + pHdr->ElementSize);
	size_t copySize = (pHdr->Count - (i + 1)) * pHdr->ElementSize;
	memcpy(iterPtr, nextIterPtr, copySize);
    }

    --pHdr->Count;
}

void*
internal_array_reserve_w_alloc(const void* array, int elementsCount, int elementSize, void* (*allocDelegate)(size_t n, i32 line, const char* file))
{
    vguard_null(array);

    ArrayHeader* newHeader = (ArrayHeader*) allocDelegate(elementsCount * elementSize + sizeof(ArrayHeader), __LINE__, __FILE__);
    newHeader->Buffer = (void*) (((u8*)newHeader) + sizeof(ArrayHeader));
    newHeader->Count = 0;
    newHeader->Capacity = elementsCount;
    newHeader->ElementSize = elementSize;

    return newHeader->Buffer;
}

void*
internal_array_grow_w_alloc(const void* array, i32 elementSize, void* (*allocDelegate)(size_t n, i32 line, const char* file), void (*freeDelegate)(void* data, i32 line, const char* file))
{
    if (array != NULL)
    {
	size_t newCapacity = 2 * array_cap(array) + 1;
	size_t newSize = newCapacity * elementSize + sizeof(ArrayHeader);
	ArrayHeader* header = array_header(array);

	ArrayHeader* newHeader = NULL;
	newHeader = (ArrayHeader*) allocDelegate(newSize, __LINE__, __FILE__);
	newHeader->Buffer = ((char*)newHeader) + sizeof(ArrayHeader);
	newHeader->ElementSize = elementSize;
	newHeader->Count = header->Count;
	newHeader->Capacity = newCapacity;
	newHeader->IsReserved = 0;

	size_t copySize = header->Count * elementSize;
	memcpy(newHeader->Buffer, array, copySize);
	assert(copySize < newSize && "CopySize >= NewSize!!!!!!");

	freeDelegate(header, __LINE__, __FILE__);

	return newHeader->Buffer;
    }

    return internal_array_reserve_w_alloc(array, StartSize, elementSize, allocDelegate);
}

void*
internal_array_copy_w_alloc(const void* src, void* (*allocDelegate)(size_t n, i32 line, const char* file))
{
    if ((src) != NULL)
    {
	ArrayHeader* header = array_header(src);
	header->IsReserved = 0;
	i32 count = header->Count;
	i32 elementSize = header->ElementSize;
	void* result = internal_array_reserve_w_alloc(NULL, count, elementSize, allocDelegate);

	array_header(result)->Count = header->Count;
	memcpy(result, src, count * elementSize);

	return result;
    }

    return NULL;
}

void*
internal_array_shift_right(void* b, i32 i)
{
    ArrayHeader* header = array_header(b);
    assert(i < header->Count && "Header remove index >= array_count!!!");
    assert(header->Count < header->Capacity && "Capacity should be exceeded before write!");

    size_t writeOffset = (i + 1) * header->ElementSize;
    size_t copySize = header->ElementSize * (header->Count - i);
    void* dataToCopy = b + i * header->ElementSize;

    memcpy(b + writeOffset, dataToCopy, copySize);

    return b;
}

void*
internal_array_pop(void* b)
{
    vguard_not_null(b);

#if STATIC_ANALIZER_CHECK == 1
    if (!b) return b;
#endif

    ArrayHeader* header = array_header(b);

    memset(b, 0, header->ElementSize);

    if (header->Count > 0)
    {
	size_t nextCopySize = header->ElementSize * (header->Count - 1);
	memcpy(b, (b + header->ElementSize), nextCopySize);
	--header->Count;
    }

    return b;
}

void*
internal_array_clear(void* array)
{
    ArrayHeader* hdr = array_header(array);
    if (!hdr->Buffer)
	return hdr->Buffer;
    memset(hdr->Buffer, 0, hdr->Capacity * hdr->ElementSize);
    hdr->Count = 0;
    return hdr->Buffer;
}

/*

  ###################################
  ###################################
  RingQueue.c
  ###################################
  ###################################

*/

void*
_ring_queue_new(const void* array, i16 capacity, size_t size)
{
    vguard_null(array);

    size_t ringQueueHeaderSize = sizeof(RingQueueHeader);

    RingQueueHeader* hdr = (RingQueueHeader*) memory_allocate(capacity * size + ringQueueHeaderSize);
    hdr->Count = 0;
    hdr->Capacity = capacity;
    hdr->InsertIndex = 0;
    hdr->ElementSize = size;
    hdr->Buffer = ((char*)hdr) + ringQueueHeaderSize;

    return hdr->Buffer;
}

void
_ring_queue_free(void* ringQueue)
{
    vguard_null(ringQueue);
    RingQueueHeader* hdr = ring_queue_header(ringQueue);
    memory_free(hdr);
}

i64
_ring_queue_push(void* rq)
{
    vguard_not_null(rq);

    RingQueueHeader* hdr = ring_queue_header(rq);
    i16 cap = hdr->Capacity;

    if (hdr->Count < cap)
    {
	++hdr->Count;
    }

    i16 ind = hdr->InsertIndex;

    ++hdr->InsertIndex;
    if (hdr->InsertIndex >= cap)
    {
	hdr->InsertIndex = 0;
    }

    return ind;
}

i16
_rinq_queue_get_popped_index(void* rq)
{
    vguard_not_null((rq));

    RingQueueHeader* hdr = ring_queue_header(rq);
    vguard(hdr->Count > 0 && "Ring Queue is empty!");

    i16 ind = hdr->InsertIndex;
    if (ind > 0)
	--ind;
    else
	ind = hdr->Capacity - 1;

    return ind;
}

void
_ring_queue_pop(void* rq, i16 popInd)
{
    RingQueueHeader* hdr = ring_queue_header(rq);
    i16 cnt       = hdr->Count;
    i16 size      = hdr->ElementSize;
    i16 lastInd   = (cnt - 1);
    i16 elemToCpy = lastInd - popInd;

    memcpy(rq + popInd * size,
	   rq + (popInd + 1) * size,
	   elemToCpy * size);

    hdr->InsertIndex = popInd;
    --hdr->Count;
}

void
ring_queue_test()
{
    i32* rq = NULL;
    rinq_queue_new(rq, 6);

    for (i32 i = 0; i < 24; ++i)
    {
	ring_queue_push(rq, i);
    }
    for (i32 i = 0; i < 6; ++i)
    {
	printf("rq[%d] = %d\n", i, rq[i]);
    }

    vguard(0);
}

// End of RingQueue.c

/*

  ###################################
  ###################################
  SimpleThread.c
  ###################################
  ###################################

*/

SimpleThread
simple_thread_create(SimpleThreadDelegate threadFunction, size_t stackSize, void* data)
{
    SimpleThread thread;

#ifdef WINDOWS_PLATFORM
    thread.ID = CreateThread(NULL, stackSize, threadFunction, NULL, 0, NULL);
#elif defined(LINUX_PLATFORM)
    pthread_attr_t threadAttributes;
    pthread_attr_init(&threadAttributes);
    i32 result = pthread_attr_setguardsize(&threadAttributes, stackSize);
    i32 status = pthread_create(&thread.ID, &threadAttributes, threadFunction, data);
    if (status == -1)
    {
	perror("Mutex error: ");
	vassert(0 && "Thread creation error!");
    }
    pthread_attr_destroy(&threadAttributes);
#endif

    //pthread_exit(void* retval);

    return thread;
}

void
simple_thread_attach(SimpleThread* thread)
{
#if defined(WINDOWS_PLATFORM)
    DWORD result = WaitForSingleObject(thread->ID, INFINITE); // NOTE(typedef): INFINITE -> no time interval, declared in some windows files
    if (result == WAIT_FAILED)
    {
	vassert(0 && "Thread can't join!");
    }
#elif defined(LINUX_PLATFORM)
    i32 status = pthread_join(thread->ID, NULL);
    if (status != 0)
    {
	vassert(0 && "Thread can't join!");
    }
#endif
}

ResultType
simple_thread_cancel(SimpleThread thread)
{
#if defined(WINDOWS_PLATFORM)
    BOOL result = TerminateThread(thread.ID, 0);
    if (result)
    {
	return ResultType_Success;
    }

    return ResultType_Error;
#elif defined(LINUX_PLATFORM)
    i32 result = pthread_cancel(thread.ID);
    if (result == 0)
    {
	return ResultType_Success;
    }

    return ResultType_Error;
#endif
}

SimpleMutex
simple_thread_mutex_create()
{
    SimpleMutex simpleMutex;

#if defined(WINDOWS_PLATFORM)
    simpleMutex.ID = CreateMutexA(NULL, FALSE, NULL);
    if (simpleMutex.ID == NULL)
    {
	GWARNING("Can't create mutex!\n");
	vassert_break();
    }
#elif defined(LINUX_PLATFORM)
    i32 result = pthread_mutex_init(&simpleMutex.ID, NULL);
    if (result != 0)
    {
	GWARNING("Can't create mutex!\n");
	vassert_break();
    }
#endif

    return simpleMutex;
}

ResultType
simple_thread_mutex_destroy(SimpleMutex* simpleMutex)
{
#if defined(WINDOWS_PLATFORM)
    BOOL result = CloseHandle(simpleMutex->ID);
    if (result == FALSE)
    {
	vassert("Can't close handle!");
	return ResultType_Error;
    }
    return ResultType_Success;
#elif defined(LINUX_PLATFORM)

    i32 result = pthread_mutex_destroy(&simpleMutex->ID);
    if (result != 0)
    {
	GWARNING("Can't create mutex!\n");
	return ResultType_Error;
    }
    return ResultType_Success;

#endif
}

ResultType
simple_thread_mutex_lock(SimpleMutex* simpleMutex)
{
#if defined(WINDOWS_PLATFORM)
    DWORD result = WaitForSingleObject(simpleMutex->ID, INFINITE);
    if (result == WAIT_FAILED)
    {
	vassert(0 && "Thread can't join!");
	return ResultType_Error;
    }
    return ResultType_Success;
#elif defined(LINUX_PLATFORM)
    i32 result = pthread_mutex_lock(&simpleMutex->ID);
    if (result != 0)
    {
	GWARNING("Can't create mutex!\n");
	return ResultType_Error;
    }
    return ResultType_Success;
#endif
}

ResultType
simple_thread_mutex_unlock(SimpleMutex* simpleMutex)
{
#if defined(WINDOWS_PLATFORM)
    BOOL result = ReleaseMutex(simpleMutex->ID);
    if (result == FALSE)
    {
	vassert(0 && "Thread can't join!");
	return ResultType_Error;
    }
    return ResultType_Success;
#elif defined(LINUX_PLATFORM)
    i32 result = pthread_mutex_unlock(&simpleMutex->ID);
    if (result != 0)
    {
	GWARNING("Can't create mutex!\n");
	return ResultType_Error;
    }
    return ResultType_Success;
#endif
}

// End Of SimpleThread.c


/*

  ###################################
  ###################################
  MemoryAllocator.c
  ###################################
  ###################################

*/

static void
block_create(MemoryBlock* this, i64 size, const char* file, i32 line)
{
    this->AllocatedSize = size;
    this->File = file;
    this->Line = line;
    this->Address = (void*) (((char*)this) + sizeof(MemoryBlock));

    assert(this == memory_block_header(this->Address) && "Wrong MemoryBlock->Address!!!");
    assert((((size_t)(this->Address) - (size_t)(this)) == sizeof(MemoryBlock)) && "Wrong MemoryBlock->Address!!!");
}

static void
print_address(const char* text, void* address)
{
    size_t addr = (size_t)address;
    if (addr)
    {
	size_t lowAddress = addr % 1000;
	char add[3];
	if (lowAddress < 100 && lowAddress >= 10)
	{
	    add[0] = '0';
	    add[1] = '\0';
	}
	else if (lowAddress < 10)
	{
	    add[0] = '0';
	    add[1] = '0';
	    add[2] = '\0';
	}
	else
	{
	    add[0] = '\0';
	    add[1] = ' ';
	    add[2] = ' ';
	}

	//printf("%s: %lu"YELLOW("%s")YELLOW("%lu "), text, addr / 1000,  add, lowAddress);
    }
    else
    {
	//printf("%s: "RED("      NULL      "), text);
    }
}

static void
block_show(MemoryBlock* block)
{
    printf("File: %s, Line: %d, Size: %ld ", block->File, block->Line, block->AllocatedSize);
    print_address("Current", (void*) block);
    printf("\n");
}

static i64 GlobalAllocatedSize = 0;
static MemoryBlock** MemoryBlocks = NULL;
static PrintAllocationSourceType PrintSourceType = PrintAllocationSourceType_None;
static i32 AllocCalls = 0;
static i32 FreeCalls  = 0;
Arena* CurrentArena = NULL;
static Arena** Arenas = NULL;
static i64 ArenasGlobalId = 0;

Arena*
_arena_create(size_t size, i32 line, const char* file)
{
    size_t arenaSize = sizeof(Arena);
    Arena* arena = (Arena*) memory_helper_malloc(size + arenaSize, __LINE__, __FILE__);
    arena->Id = ArenasGlobalId;
    arena->Offset = 0;
    arena->Size = size;
    arena->Line = line;
    arena->File = file;
    arena->Data = ((void*)arena) + arenaSize;

    ++ArenasGlobalId;

    return arena;
}

void
arena_clear(Arena* pArena)
{
    pArena->Offset = 0;
    memset(pArena->Data, 0, pArena->Size);
}

Arena*
_arena_create_and_set(size_t size, i32 line, const char* file)
{
    Arena* arena = _arena_create(size, line, file);
    memory_set_arena(arena);
    return arena;
}

void
arena_destroy(Arena* arena)
{
    memset(arena->Data, 0, arena->Size);

    if (CurrentArena == arena)
    {
	memory_set_arena(NULL);
    }

    memory_helper_free(arena, __LINE__, __FILE__);
}

void
arena_format(char* buffer, Arena* arena)
{
    const i32 size = 128;
    char offsetBuf[size];
    char sizeBuf[size];
    memory_helper_format_size(offsetBuf, arena->Offset);
    memory_helper_format_size(sizeBuf, arena->Size);
    string_format(buffer, "[id: %ld line: %d file: %s] %s / %s", arena->Id, arena->Line, arena->File, offsetBuf, sizeBuf);
}

void
arena_print(Arena* arena)
{
    if (arena == NULL)
    {
	GWARNING("Arena is NULL!\n");
	return;
    }

    char buf[256];
    arena_format(buf, arena);
    GWARNING("Arena: %s\n", buf);
}

void
memory_bind_current_arena()
{
    if (!array_any(Arenas))
    {
	return;
    }

    CurrentArena = Arenas[array_count(Arenas) - 1];
}

void
memory_unbind_current_arena()
{
    CurrentArena = NULL;
}

void
memory_set_arena(Arena* arena)
{
    CurrentArena = NULL;

    if (arena != NULL)
    {
	array_push(Arenas, arena);
	CurrentArena = arena;
    }
    else if (array_any(Arenas))
    {
	array_pop(Arenas);
	if (array_any(Arenas))
	    CurrentArena = array_pop(Arenas);
    }
}

Arena*
memory_get_arena()
{
    return CurrentArena;
}

void*
_memory_allocate(size_t size, i32 line, const char* file)
{
    if (PrintSourceType == PrintAllocationSourceType_Terminal)
    {
	GSUCCESS("Allocated memory Size: %d, FILE: %s LINE: %d\n", size, file, line);
    }

    if (CurrentArena != NULL)
    {
	if (CurrentArena->Offset >= CurrentArena->Size)
	{
	    GERROR("Need more memory for arena offset:%d size:%d !!!\n", CurrentArena->Offset, CurrentArena->Size);
	    vguard(CurrentArena->Offset < CurrentArena->Size && "Need more memory for arena!");
	}

	vguard(CurrentArena->Offset >= 0 && "Wrong arena offset");
	void* data = CurrentArena->Data + CurrentArena->Offset;
	CurrentArena->Offset += size;

	return data;
    }

    ++AllocCalls;

    GlobalAllocatedSize += size;

    size_t newSize = size + sizeof(MemoryBlock);
    assert(size > 0 && "memory_allocate(size) where size > 0 !!!");

    MemoryBlock* header = (MemoryBlock*) malloc(newSize);
    block_create(header, size, file, line);
    array_push_w_alloc(MemoryBlocks, memory_helper_malloc, memory_helper_free, header);

    return header->Address;
}

MemoryBlock**
memory_helper_get_memory_blocks()
{
    return MemoryBlocks;
}

void*
memory_helper_reallocate(void* data, i32 size, i32 line, const char* file)
{
    GWARNING("Do no use realloc!\n");
    return _memory_allocate(size, line, file);
}

void
_memory_free(void* data, i32 line, const char* file)
{
    if (CurrentArena != NULL)
    {
	//arena_print(CurrentArena);
	vguard(CurrentArena->Offset < CurrentArena->Size && "Need more memory for arena!");
	vguard(CurrentArena->Offset >= 0 && "Wrong arena offset");
	return;
    }

    ++FreeCalls;
    //MemoryBlock* block = list_find(&g_AllocList, data);
    //list_remove(&g_AllocList, block);
    MemoryBlock* block = memory_block_header(data);

    vassert_null_offset(block->Address);

    array_remove(MemoryBlocks, block);

    if (PrintSourceType == PrintAllocationSourceType_Terminal)
    {
	GSUCCESS("Free memory: Size: %d, FILE: %s LINE: %d\n", block->AllocatedSize, file, line);
    }

    GlobalAllocatedSize -= block->AllocatedSize;

    free(block);

    block = NULL;
}

void
memory_helper_free_bytes(size_t size, i32 line, const char* file)
{
    if (CurrentArena != NULL)
    {
	vassert(CurrentArena->Offset < CurrentArena->Size && "Need more memory for arena!");
	vassert(CurrentArena->Offset >= 0 && "Wrong arena offset");
	CurrentArena->Offset -= size;

	if (PrintSourceType == PrintAllocationSourceType_Terminal)
	{
	    GSUCCESS("Free memory: Size: %d, FILE: %s LINE: %d\n", size, file, line);
	}
    }
}

i32
memory_helper_get_allocated_size()
{
    return GlobalAllocatedSize;
}

void
memory_helper_format_size(char* buf, size_t bytes)
{
    i64 kb = KB(1),
	mb = MB(1),
	gb = GB(1);
    if (bytes >= kb && bytes < mb)
    {
	string_format(buf, "%0.2f kb", ((f32)bytes) / kb);
    }
    else if (bytes >= mb && bytes < gb)
    {
	string_format(buf, "%0.2f mb", ((f32)bytes) / mb);
    }
    else if (bytes >= gb)
    {
	string_format(buf, "%0.2f gb", ((f32)bytes) / gb);
    }
    else
    {
	string_format(buf, "%zu bytes", bytes);
    }
}

//TODO(bies): rename this as soon as possible
void
memory_set_print(PrintAllocationSourceType type)
{
    PrintSourceType = type;
}

void*
memory_helper_malloc(size_t size, i32 line, const char* file)
{
    return malloc(size);
}

void
memory_helper_free(void* data, i32 line, const char* file)
{
    vguard_not_null(data);
#if STATIC_ANALIZER_CHECK == 1
    if (!data) return;
#endif

    free(data);
}


/*

  ###################################
  ###################################
  String.c
  ###################################
  ###################################

*/

IStringPool*
istring_pool_create()
{
    IStringPool* iStringPool = (IStringPool*) memory_allocate(sizeof(IStringPool));

    iStringPool->Strings = NULL;

    return iStringPool;
}

// note: this shit exist forever
void
istring_pool_destroy(IStringPool* pPool)
{
#if 0
    if (pPool == NULL)
	return;

    i64 cnt = array_count(pPool->Strings);
    for (i64 i = 0; i < cnt; ++i)
    {
	IString* istr = pPool->Strings[i];
	if (istr != NULL)
	    istring_free_ext(pPool, istr->Buffer);
    }

    array_free(pPool->Strings);

    memory_free(pPool);
#endif
}

/* DOCS(typedef): IString local api */
IString*
istring_new_ext(IStringPool* stringPool, const char* src)
{
    IString* istr = istring_get(src);
    if (istr)
    {
	return istr;
    }

    i32 ind = array_count(stringPool->Strings);
    istring_allocate_ext(stringPool, src);
    vassert(&stringPool->Strings[ind] && "Problem with getting IString!");

    return stringPool->Strings[ind];
}

char*
istring_allocate_ext(IStringPool* stringPool, const char* src)
{
    vguard_not_null(src);

    i64 length = string_length(src);
    size_t size = sizeof(IString) + (length + 1) * sizeof(*src);
    IString* interning = (IString*) memory_allocate(size);
    char* buffer = (char*) (((char*)interning) + sizeof(IString));
    vguard_not_null(buffer);
    memcpy(buffer, src, length * sizeof(*src));
    buffer[length] = '\0';

    interning->Buffer = (char*) buffer;
    interning->Length = length;

    vassert(interning->Buffer == ((void*)interning) + sizeof(*interning));

    array_push(stringPool->Strings, interning);

    return interning->Buffer;
}

char*
istring_get_buffer_ext(IStringPool* stringPool, const char* src)
{
    i32 i;
    i32 count = array_count(stringPool->Strings);

    for (i = 0; i < count; ++i)
    {
	if (string_compare(src, stringPool->Strings[i]->Buffer))
	{
	    return stringPool->Strings[i]->Buffer;
	}
    }

    return NULL;
}

IString*
istring_get_ext(IStringPool* stringPool, const char* src)
{
    i32 i;
    i32 count = array_count(stringPool->Strings);

    for (i = 0; i < count; ++i)
    {
	if (string_compare(src, stringPool->Strings[i]->Buffer))
	{
	    return stringPool->Strings[i];
	}
    }

    return NULL;
}

void
istring_free_ext(IStringPool* stringPool, char* istring)
{
    IString* iheader = istring_header(istring);
    array_remove(stringPool->Strings, iheader);
    memory_free(iheader);
}

void
istring_free_headers_ext(IStringPool* stringPool)
{
    if (!stringPool->Strings)
	return;

    array_foreach(stringPool->Strings, memory_free(item););
    array_free(stringPool->Strings);
}

static IString** g_IStrings = NULL;

IString*
istring_new(const char* src)
{
    IStringPool stringPool = { .Strings = g_IStrings };
    return istring_new_ext(&stringPool, src);
}

char*
istring_allocate(const char* src)
{
    IStringPool stringPool = { .Strings = g_IStrings };
    return istring_allocate_ext(&stringPool, src);
}

IString*
istring_get(const char* src)
{
    IStringPool stringPool = { .Strings = g_IStrings };
    return istring_get_ext(&stringPool, src);
}

char*
istring_get_buffer(const char* src)
{
    IStringPool stringPool = { .Strings = g_IStrings };
    return istring_get_buffer_ext(&stringPool, src);
}

IString**
istring_get_headers()
{
    return g_IStrings;
}

void
istring_free(char* istring)
{
    IStringPool stringPool = { .Strings = g_IStrings };
    istring_free_ext(&stringPool, istring);
}

void
istring_free_headers()
{
    IStringPool stringPool = { .Strings = g_IStrings };
    istring_free_headers_ext(&stringPool);
}

//Internal
force_inline i32
_string_number_rank(i32 number)
{
    i32 rank = 0;
    for (; ;)
    {
	number /= 10;
	if (number == 0)
	{
	    return rank;
	}

	++rank;
    }
}

force_inline i32
_string_number_of_rank(i32 number, i32 rank)
{
    if (rank <= 0)
    {
	return number;
    }

    for (i32 i = 0; i < rank; i++)
    {
	number *= 10;
    }

    return number;
}

force_inline i32
_string_number_of_digit(i32 number, i32 digit)
{
    i32 i;

    if (_string_number_rank(number) < digit)
    {
	return 0;
    }

    if (number < 0)
    {
	number *= -1;
    }

    if (digit == 0)
    {
	return (number % 10);
    }

    number %= _string_number_of_rank(1, (digit + 1));
    number /= _string_number_of_rank(1, digit);

    return number;
}

#define _string_int(input, number)                                      \
    ({                                                                  \
	i8 isNumberNegative = ((number < 0) ? 1 : 0);                   \
	i32 i, rank = _string_number_rank(number), numberLength = rank + isNumberNegative + 1; \
									\
	if (isNumberNegative)                                           \
	{                                                               \
	    input[0] = '-';                                             \
	}                                                               \
									\
	for (i = isNumberNegative; i < numberLength; ++i)               \
	{                                                               \
	    input[i] = _string_number_of_digit(number, rank) + 48;      \
	    --rank;                                                     \
	}                                                               \
    })


char*
string(const char* string)
{
    i64 length = string_length(string);
    char* newString = (char*) memory_allocate((length + 1) * sizeof(char));
    newString[length] = '\0';
    memcpy(newString, string, length * sizeof(char));

    return newString;
}

void
string_i32(char* input, i32 number)
{
    _string_int(input, number);
}

char*
string_allocate(i32 length)
{
    i32 size = (length + 1) * sizeof(char);
    char* newString = (char*) memory_allocate(size);
    memset(newString, '\0', size);

    return newString;
}

i32
string_count_of_fast(const char* string, i32 length, char c)
{
    i32 count = 0;
    //char* ptr = (char*) string;
    for (i32 i = 0; i < length; ++i)
    {
	if (string[i] == c)
	    ++count;
    }

    return count;
}

i32
string_count_of(const char* string, char c)
{
    i64 length = string_length(string);
    i32 count = string_count_of_fast(string, length, c);

    return count;
}

i32
string_count_upper(const char* string)
{
    i32 count = 0;
    char* optr = (char*) string;
    char oc;
    oc = *optr;

    while (oc != '\0')
    {
	if (char_is_upper(oc))
	{
	    ++count;
	}

	++optr;
	oc = *optr;
    }

    return count;
}

void
string_set(char* string, char c, u32 length)
{
    i32 i;
    for (i = 0; i < length; ++i)
    {
	string[i] = c;
    }
}

i64
string_length(const char* str)
{
    vguard_not_null(str);

    char* ptr;
    for (ptr = (char*) str; *ptr != '\0'; ++ptr);

    return (i64) ((size_t)(ptr - str));
}

size_t
string_length_to_delimiters(const char* str, char delimeters[], size_t delimetersLength)
{
    vguard_not_null(str);

    char* ptr = (char*) str;
    char c = *ptr;
    while (c != '\0')
    {
	i32 interrupt = 0;
	for (i32 i = 0; i < delimetersLength; ++i)
	{
	    if (c == delimeters[i])
	    {
		interrupt = 1;
		break;
	    }
	}

	if (interrupt)
	    break;

	++ptr;
	c = *ptr;
    }

    size_t length = ptr - str;
    return length;
}

char*
string_copy(const char* oth, i32 length)
{
    assert(oth);

    char* result = (char*) memory_allocate((length + 1) * sizeof(char));
    memcpy(result, oth, length);
    result[length] = '\0';
    return result;
}
char*
string_copy_bigger(const char* oth, i32 length, i32 bigLength)
{
    vguard_not_null(oth);
    vassert(bigLength > length && "bigLength <= length!");

    char* result = (char*) memory_allocate((bigLength + 1) * sizeof(char));
    memcpy(result, oth, length);
    result[bigLength] = '\0';
    return result;
}

char*
string_concat(const char* left, const char* right)
{
    i32 leftLength = string_length(left);
    i32 rightLength = string_length(right);
    i32 bothLength = leftLength + rightLength;

    if (bothLength > 100000)
    {
	GERROR("LENGTH: %d\n", bothLength);
    }
    char* newString = (char*) memory_allocate((bothLength + 1) * sizeof(char));

    memcpy(newString, left, leftLength);
    memcpy(newString + leftLength, right, rightLength);
    newString[bothLength] = '\0';

    return newString;
}

char*
string_concat_with_space_between(const char* left, const char* right, i32 length, char c)
{
    vassert_break();
    assert(left && "left is NULL or undefined!");
    assert(right && "right is NULL or undefined!");

    i32 leftLength = string_length(left);
    i32 rightLength = string_length(right);
    i32 bothLength = leftLength + rightLength;

    assert((length > bothLength) && "length >");

    i32 totalLength = (length + rightLength + 1);
    char* result = (char*) memory_allocate(totalLength * sizeof(char));
    //memset(result, c, totalLength);
    memcpy(result, left, leftLength);
    memcpy(result + length, right, rightLength);

    result[bothLength] = '\0';

    return result;
}

char*
string_concat3(const char* left, const char* middle, const char* right)
{
    assert(left && "left is NULL or undefined!");
    assert(middle && "middle is NULL or undefined!");
    assert(right && "right is NULL or undefined!");

    i32 leftLength = string_length(left);
    i32 middleLength = string_length(middle);
    i32 rightLength = string_length(right);
    i32 bothLength = leftLength + middleLength + rightLength;

    char* newString = (char*) memory_allocate(bothLength + 1);

    memcpy(newString, left, leftLength);
    memcpy(newString + leftLength, middle, middleLength);
    memcpy(newString + leftLength + middleLength, right, rightLength);
    newString[bothLength] = '\0';

    return newString;
}

char*
string_concat3l(const char* left, const char* middle, const char* right, i32 leftLength, i32 middleLength, i32 rightLength)
{
    assert(left && "left is NULL or undefined!");
    assert(middle && "middle is NULL or undefined!");
    assert(right && "right is NULL or undefined!");

    i32 bothLength = leftLength + middleLength + rightLength;
    char* newString = (char*) memory_allocate(bothLength + 1);

    memcpy(newString, left, leftLength);
    memcpy(newString + leftLength, middle, middleLength);
    memcpy(newString + leftLength + middleLength, right, rightLength);

    newString[bothLength] = '\0';

    return newString;
}

i32
string_compare(const char* left, const char* right)
{
    vguard_not_null(left);
    vguard_not_null(right);

    i64 i = 0,
	leftLength  = string_length(left),
	rightLength = string_length(right);

    if ((leftLength != rightLength)
	|| (leftLength == 0)
	|| (rightLength == 0))
    {
	return 0;
    }

    char* ptrl = (char*) left;
    char* ptrr = (char*) right;
    for ( ;i < leftLength; )
    {
	if (*ptrl != *ptrr)
	{
	    return 0;
	}

	++i;
	++ptrl;
	++ptrr;
    }

    return 1;
}

i32
string_compare_length(const char* left, const char* right, i64 length)
{
    vguard_not_null(left);
    vguard_not_null(right);

    i64 i = 0;
    char* ptrl = (char*) left;
    char* ptrr = (char*) right;
    for ( ;i < length; )
    {
	char lc = *ptrl;
	char rc = *ptrr;
	if (lc != rc)
	{
	    return 0;
	}

	++i;
	++ptrl;
	++ptrr;
    }

    return 1;
}

i32
string_compare_length_safe(const char* left, const char* right, i32 length)
{
    assert(left && "left is NULL or undefined!");
    assert(right && "right is NULL or undefined!");

    i32 llength = string_length(left);
    i32 rlength = string_length(right);
    if (llength != rlength)
	return 0;

    if (length > llength || length > rlength)
	return 0;

    i32 i;
    for (i = 0; i < length; ++i)
    {
	if (left[i] != right[i])
	{
	    return 0;
	}
    }

    return 1;
}


i32
string_compare_w_length(const char* left, const char* right, i32 leftLength, i32 rightLength)
{
    assert(left && "left is NULL or undefined!");
    assert(right && "right is NULL or undefined!");

    if (leftLength != rightLength)
    {
	return 0;
    }

    char* eptr = ((char*) left + leftLength);
    char* lptr = (char*) left;
    char* rptr = (char*) right;
    for (;lptr != eptr;)
    {
	if (*lptr != *rptr)
	{
	    return 0;
	}

	++lptr;
	++rptr;
    }

    return 1;
}

char*
string_to_upper(const char* input)
{
    assert(input && "input is NULL or undefined!");

    i32 i;
    i32 inputLength = string_length(input);
    char* result = memory_allocate((inputLength + 1) * sizeof(char));

    for (i = 0; i < inputLength; ++i)
    {
	char element = input[i];
	if (element >= 'a' && element <= 'z')
	{
	    result[i] = element - 'a' + 'A';
	}
	else
	{
	    result[i] = element;
	}
    }
    result[inputLength] = '\0';
    return result;
}

char*
string_to_lower(const char* input)
{
    assert(input && "input is NULL or undefined!");

    i32 i, input_length;
    char  element;
    char* result;

    if (input == NULL)
    {
	return NULL;
    }

    input_length = string_length(input);
    result = memory_allocate((input_length + 1) * sizeof(char));
    for (i = 0; i < input_length; ++i)
    {
	element = input[i];
	if (element >= 'A' && element <= 'Z')
	{
	    result[i] = element - 'A' + 'a';
	}
	else
	{
	    result[i] = element;
	}
    }
    result[input_length] = '\0';
    return result;
}

i32
string_index_of(const char* input, char character)
{
    assert(input && "input is NULL or undefined!");

    i32 i;

    if (input == NULL)
    {
	return -1;
    }

    for (i = 0; input[i] != '\0'; ++i)
    {
	if (input[i] == character)
	{
	    return i;
	}
    }

    return -1;
}

i32
string_index_of_string(const char* input, const char* string)
{
    assert(input && "input is NULL or undefined!");
    assert(string && "string is NULL or undefined!");

    i32 i, j, flag, inputLength, stringLength;

    assert(string != NULL);

    inputLength = string_length(input);
    stringLength = string_length(string);

    if (inputLength <= 0 || stringLength <= 0)
    {
	return -1;
    }

    flag = -1;
    for (i = 0; i < inputLength; ++i)
    {
	for (j = 0; j < stringLength; ++j)
	{
	    if (input[i + j] == string[j])
	    {
		flag = 1;
	    }
	    else
	    {
		flag = -1;
		break;
	    }
	}

	if (flag == 1)
	{
	    return i;
	}
    }

    return -1;
}

i32
string_last_index_of(const char* input, char character)
{
    assert(input && "Input can't be NULL!!!");

    i32 i, startIndex;

    startIndex = string_length(input) - 1;

    for (i = startIndex; i >= 0; i--)
    {
	if (input[i] == character)
	{
	    return i;
	}
    }

    return -1;
}

i32
string_last_index_of_string(const char* input, const char* string)
{
    vguard_not_null(input);
    vguard_not_null(string);

    i32 i, j, flag, inputLength, stringLength;

    inputLength = string_length(input);
    stringLength = string_length(string);

    if (inputLength <= 0 || stringLength <= 0)
    {
	return -1;
    }

    flag = -1;
    i32 temp = 0;
    for (i = inputLength; i >= 0; i--)
    {
	for (j = stringLength; j >= 0; j--)
	{
	    if (input[i - temp] == string[j])
	    {
		flag = 1;
	    }
	    else
	    {
		flag = -1;
		break;
	    }
	    ++temp;
	}

	if (flag == 1)
	{
	    return temp;
	}
    }

    return -1;
}

i32
string_last_index_of_upper(const char* input, i32 length)
{
    i32 i;
    char* ptr = (char*) input;

    for (i = (length - 1); i >= 0; --i)
    {
	char c = ptr[i];
	if (char_is_upper(c))
	{
	    return i;
	}
    }

    return -1;
}

char*
string_substring(const char* input, i32 startIndex)
{
    vguard(input && "input length is NULL !!!");

    i32 i, newLength, inputLength;
    char* result;

    inputLength = string_length(input);
    assert(startIndex < inputLength && "start index >= input length !!!");
    assert(startIndex > 0 && "start index < 0!!!");

    newLength = inputLength - startIndex;
    result = memory_allocate((newLength + 1) * sizeof(char));
    memcpy(result, input + startIndex, newLength);
    result[newLength] = '\0';

    return result;
}

char*
string_substring_length(const char* input, size_t inputLength, i32 length)
{
    vguard_not_null(input);
    vassert(length <= inputLength && "start index >= input length !!!");

    char* result = memory_allocate((length + 1) * sizeof(char));
    memcpy(result, input, length);
    result[length] = '\0';

    return result;
}

char*
string_substring_range(const char* input, i32 startIndex, i32 endIndex)
{
    i32 i, inputLength, newLength;
    char* result;

    assert(input && input != NULL && "input can't be NULL!!!");

    inputLength = string_length(input);
    newLength = endIndex - startIndex + 1;
    result = memory_allocate((newLength + 1) * sizeof(char));

    vassert(startIndex < inputLength);
    vassert(startIndex >= 0);
    vassert(inputLength > endIndex && "Out of input string range!");
    vassert(startIndex <= endIndex && "Out of input string range!");

    memcpy(result, input + startIndex, newLength);
    result[newLength] = '\0';

    return result;
}

char*
string_after(const char* input, i32 length, char c)
{
    char* end = (char*) (input + length);
    for (char* ptr = (char*) input; ptr != end; ++ptr)
    {
	if (*ptr == c)
	{
	    return string_copy(ptr, end - ptr);
	}
    }

    vassert_break();
    return NULL;
}

char*
string_replace_string(char* input, size_t inputLength, char* replaceStr, size_t replaceStrLength, char* newString, size_t newStringLength)
{
    typedef struct ReplaceStringRecord
    {
	size_t StartIndex;
    } ReplaceStringRecord;

    ReplaceStringRecord* records = NULL;

    {
	i32 i = 0;
	char* ptr = input;
	while (*ptr != '\0')
	{
	    if (string_compare_length(ptr, replaceStr, replaceStrLength))
	    {
		ReplaceStringRecord record = {
		    .StartIndex = i
		};
		array_push(records, record);
	    }

	    ++i;
	    ++ptr;
	}
    }

    if (records == NULL || newString == NULL || newStringLength == 0)
	return string_copy(input, inputLength);

    i32 i, count = array_count(records),
	lengthDiff = count * (inputLength - replaceStrLength);
    vassert(count > 0 && "count > 0!");

    size_t size = inputLength + lengthDiff + 1;
    char* result = memory_allocate(size);
    memset(result, 0, size);
    memcpy(result, input, records->StartIndex);
    //GINFO("Result: %.*s\n", records->StartIndex, result);
    char* wStr = result + records->StartIndex;
    //GINFO("ReplaceLength: %d\n", replaceStrLength);

    for (i = 0; i < count; ++i)
    {
	ReplaceStringRecord record = records[i];

	memcpy(wStr, newString, newStringLength);
	wStr += newStringLength;

	i32 newReadPos = record.StartIndex + replaceStrLength;
	i32 len;
	if (i != (count - 1))
	{
	    ReplaceStringRecord nextRecord = records[i + 1];
	    len = nextRecord.StartIndex - newReadPos;
	}
	else
	{
	    len = inputLength - newReadPos;
	}

	//GINFO("newReadPos: %d, len: %d\n", newReadPos, len);
	memcpy(wStr, input + newReadPos, len);
	wStr += len;

	//GERROR("Result: %.*s\n", i32(wStr - result), result);
    }

    return result;
}

/*
  3, 5
  "01234567" - len: 8
  "01267" - len: 5
*/
char*
string_cut(const char* input, u32 begin, u32 end)
{
    i32 i, inputLength = string_length(input), resultLength = inputLength - (end - begin + 1);
    char* result;

    assert(input);
    assert(begin >= 0);
    assert(end < inputLength);
    assert(begin < end);
    assert(inputLength);
    assert(resultLength);

    result = memory_allocate(resultLength);
    for (i = 0; i < begin; ++i)
    {
	result[i] = input[i];
    }

    for (i = (end + 1); i < inputLength; ++i)
    {
	result[begin + i - end - 1] = input[i];
    }

    return result;
}

char*
string_replace_char(char* input, char c)
{
    vassert_break();
    return NULL;

    /* i32 length = string_length(input); */
    /* i32 ind = 0; */
    /* const i32 bufLen = 2048; */
    /* vassert(length > bufLength && "2048 max size!"); */
    /* char buf[bufLen]; */
    /* memset(buf, '\0', bufLen * sizeof(char)); */

    /* for (i32 i = 0; i < length; ++i) */
    /* { */
    /*	char ch = input[i]; */
    /*	if (ch != c) */
    /*	{ */
    /*      buf[ind] = ch; */
    /*      ++ind; */
    /*	} */
    /* } */

    /* return string(buf); */
}

char*
string_trim_char(char* input, size_t length, size_t* newLength, char c)
{
    size_t cCount = 0;
    char* ptr = input;
    char pc = *ptr;
    while (pc != '\0')
    {
	if (pc == c)
	{
	    ++cCount;
	}

	++ptr;
	pc = *ptr;
    }

    size_t nLength = length - cCount;
    char* newStr = memory_allocate((nLength + 1) * sizeof(char));
    newStr[nLength] = '\0';

    *newLength = nLength;

    ptr = input;
    char* wptr = newStr;
    pc = *ptr;
    while (pc != '\0')
    {
	if (pc != c)
	{
	    *wptr = pc;
	    ++wptr;
	}

	++ptr;
	pc = *ptr;
    }

    return newStr;
}


char**
string_split(char* input, char splitCharacter)
{
    vguard_not_null(input);

    char** result = string_split_length(input, string_length(input), splitCharacter);

    return result;
}

char**
string_split_length(char* input, size_t inputLength, char splitCharacter)
{
    i32 i,
	wordBeginIndex = 0,
	isWordIndexSet = 0;
    char** result = NULL;

    if (string_index_of(input, splitCharacter) == -1)
    {
	array_push(result, string(input));
	return result;
    }

    for (i = 0; i < inputLength; ++i)
    {
	char character = input[i];
	if (character != splitCharacter && !isWordIndexSet)
	{
	    isWordIndexSet = 1;
	    wordBeginIndex = i;
	}

	char* word;
	if (character == splitCharacter && isWordIndexSet)
	{
	    isWordIndexSet = 0;

	    word = string_substring_range(input, wordBeginIndex, i - 1);
	    array_push(result, word);
	}
	else if (i == (inputLength - 1))
	{
	    word = string_substring(input, wordBeginIndex);
	    array_push(result, word);
	}
    }

    return result;
}

char*
string_join(const char** list, char joinCharacter)
{
    i32 i, listCount, finalLength, curLength = 0, strLength;
    char* finalString = NULL;
    const char* str = NULL;

    assert(list && "List is NULL or Undefined!!!");
    listCount = array_count(list);
    assert(listCount && "List is empty !!!");

    finalLength = listCount;
    for (i = 0; i < listCount; ++i)
    {
	finalLength += string_length(list[i]);
    }

    finalString = (char*) memory_allocate(finalLength);
    for (i = 0; i < (listCount - 1); ++i)
    {
	str = list[i];
	strLength = string_length(str);
	memcpy(finalString + curLength, str, strLength);
	finalString[curLength + strLength] = joinCharacter;
	curLength += strLength + 1;
    }

    str = list[listCount - 1];
    strLength = string_length(str);
    memcpy(finalString + curLength, str, strLength);

    finalString[finalLength - 1] = '\0';

    return finalString;
}

char*
string_join_i32(const i32* list, char joinCharacter)
{
    assert(0 && "Not tested yet, let's write some Unit Test's!");

    char* result = NULL;
    char stringValue[32];

    i32 i, count = array_count(list), el;
    for (i = 0; i < count; ++i)
    {
	el = list[i];
	string_i32(stringValue, el);
    }

    return NULL;
}

void
string_i64(char* input, i64 number)
{
    _string_int(input, number);
}

void
string_f32(char* input, f32 number)
{
    sprintf(input, "%f", number);
}

void
string_f64(char* input, f64 number)
{
    sprintf(input, "%f", number);
}

i32
string_is_integer(char* input, size_t length)
{
    size_t ind = 0;
    i32 flag = 1;
    char* ptr = input;

    while (*ptr)
    {
	if (ind >= length)
	    break;

	if (*ptr < '0' || *ptr > '9')
	{
	    flag = 0;
	    break;
	}

	++ind;
    }

    return flag;
}

i32
string_to_i32(char* input)
{
    return string_to_i32_length(input, string_length(input));
}

i32
string_to_i32_length(char* input, i32 length)
{
    i32 digit,
	i = 0, result = 0, isNegative = 0,
	multiplier = 10,
	rank = length;

    if (input[0] == '-')
    {
	isNegative = 1;
	i = 1;
    }

    for (; i < rank; ++i)
    {
	digit = input[i] - '0';
	result = (result * multiplier) + digit;
    }

    if (isNegative)
	result *= -1;

    return result;
}

f32
string_to_f32_length(char* input, size_t length)
{
    i32 digit,
	i = 0, isNegative = 0,
	multiplier = 10,
	rank = length;
    f32 intResult = 0,
	floatMultiplier = 0.1;
    char c;

    if (input[0] == '-')
    {
	isNegative = 1;
	i = 1;
    }

    for (; i < rank; ++i)
    {
	c = input[i];
	if (c == '.')
	{
	    ++i;
	    break;
	}

	digit = c - '0';
	intResult = (intResult * multiplier) + digit;
    }

    f32 floatResult = 0.0f;
    for (i32 f = (rank - 1); f >= i; --f)
    {
	c = input[f];

	digit = c - '0';

	/*
	  12.318
	  ^
	  0.8
	  0.08 + 0.1 = 0.18
	*/

	floatResult = (floatResult * floatMultiplier) + digit * floatMultiplier;
    }

    intResult += floatResult;

    if (isNegative)
	intResult *= -1;

    return intResult;
}

f32
string_to_f32(char* input)
{
    f32 value = string_to_f32_length(input, string_length(input));
    return value;
}

/*

  ###################################
  ###################################
  WideString.c
  ###################################
  ###################################

*/
#include <wchar.h>

void
wide_string_p(WideString ws)
{
    for (i32 i = 0; i < ws.Length; ++i)
    {
	wchar c = ws.Buffer[i];
	if (printf("%C", c) < 0)
	{
	    perror("printf");
	}
    }
}
void
wide_string_pl(WideString ws)
{
    wide_string_p(ws);
    printf("\n");
}

WideString
wide_string(wchar* buffer)
{
    size_t length = wcslen(buffer);
    WideString result = wide_string_new(buffer, length);
    return result;
}

WideString
wide_string_new(wchar* buffer, size_t length)
{
    size_t size = (length + 1) * sizeof(*buffer);
    wchar* buf = (wchar*) memory_allocate(size);
    wcsncpy(buf, buffer, length);
    buf[length] = L'\0';

    WideString header = {
	.Length = length,
	.Buffer = buf,
    };

    return header;
}

WideString
wide_string_format(const wchar* format, ...)
{
    va_list valist;
    va_start(valist, format);

    size_t bufSize = MB(15);
    wchar* buf = (wchar*) memory_allocate(bufSize);
    i32 length = vswprintf(buf, bufSize, format, valist);
    va_end(valist);

    size_t size = length * sizeof(wchar);
    wchar* wbuf = (wchar*) memory_allocate(size);
    memcpy(wbuf, buf, size);
    WideString ws = {
	.Length = length,
	.Buffer = wbuf
    };
    memory_free(buf);

    return ws;
}

WideString*
wide_string_ptr(wchar* input, size_t length)
{
    size_t size = length * sizeof(wchar);
    WideString* pWideString = (WideString*) memory_allocate(sizeof(WideString) + size);

    pWideString->Length = length;
    pWideString->Buffer = ((void*) pWideString) + sizeof(WideString);

    wcsncpy(pWideString->Buffer, input, length);

    return pWideString;
}

void
wide_string_destroy(WideString wideString)
{
    memory_free(wideString.Buffer);
}

size_t
wide_string_utf8_length(const char* utf8Str)
{
    size_t length = 0;
    char* ptr = (char*) utf8Str;
    while (*ptr)
    {
	length += (*ptr++ & 0xc0) != 0x80;
    }

    return length;
}

size_t
wide_string_utf8_get_char_size(const char* str)
{
    if (0xf0 == (0xf8 & str[0]))      return 4; // 4 byte utf8 codepoin
    else if (0xe0 == (0xf0 & str[0])) return 3; // 3 byte utf8 codepoint
    else if (0xc0 == (0xe0 & str[0])) return 2; // 2 byte utf8 codepoint

    return 1; // 1 byte utf8 codepoint otherwise
}


WideString
wide_string_utf8(const char* utf8Str)
{
    if (utf8Str == NULL)
    {
	return (WideString) {
	    .Buffer = NULL,
	    .Length = 0
	};
    }

    size_t ut8StrLength = wide_string_utf8_length(utf8Str);
    WideString result = char_as_wide_string((char*)utf8Str, ut8StrLength);
    //wide_string_print_line(result);
    return result;
}

wchar*
wide_string_raw(wchar* buf, size_t length)
{
    size_t size = (length + 1) * sizeof(wchar);
    wchar* res = (wchar*) memory_allocate(size);
    wcsncpy(res, buf, length);
    res[length] = L'\0';

    return res;
}

i32
wide_string_is_valid(WideString input)
{
    i32 isValid = input.Buffer && input.Length > 0;
    return isValid;
}

WideString
wide_string_concat(WideString first, WideString second)
{
    size_t length = first.Length + second.Length;
    size_t firstSize = first.Length * sizeof(wchar);
    size_t size = length * sizeof(wchar);

    wchar* buf = (wchar*) memory_allocate(size);
    wcsncpy(buf, first.Buffer, first.Length);
    wcsncpy(((void*)buf) + firstSize, second.Buffer, second.Length);

    WideString result = {
	.Length = length,
	.Buffer = buf
    };

    return result;
}

WideString
wide_string_concat_native(wchar* firstNative, wchar* secondNative)
{
    WideString first = wide_string_woa(firstNative);
    WideString second = wide_string_woa(secondNative);
    return wide_string_concat(first, second);
}

// [startIndex, endIndex]
WideString
wide_string_substring_range(WideString input, i32 startIndex, i32 endIndex)
{
    assert(input.Buffer && "input can't be NULL!!!");

    // 0 5
    //  0123456
    // "Привет мир"
    size_t newLength = endIndex - startIndex + 1;

    WideString result;
    result.Length = newLength;
    result.Buffer = (wchar*) memory_allocate(newLength * sizeof(wchar));

    assert(startIndex < input.Length);
    assert(startIndex >= 0);
    assert(input.Length > endIndex);
    assert(startIndex <= endIndex);

    wcsncpy(result.Buffer, &input.Buffer[startIndex], newLength);

    return result;
}

WideString
wide_string_substring(WideString input, i32 startIndex)
{
    WideString result = wide_string_substring_range(input, startIndex, input.Length - 1);
    return result;
}

WideString*
wide_string_split(WideString input, wchar splitCharacter)
{
    WideString* result = NULL;
    i32 lastIndex = input.Length - 1;
    i32 prevSeparatorIndex = 0;

    for (i32 i = 0; i < input.Length; ++i)
    {
	wchar character = input.Buffer[i];

	if (character == splitCharacter)
	{
	    WideString word = wide_string_substring_range(input, prevSeparatorIndex, i - 1);
	    array_push(result, word);
	    prevSeparatorIndex = i + 1;
	}
	else if (i == lastIndex)
	{
	    WideString word = wide_string_substring(input, prevSeparatorIndex);
	    array_push(result, word);
	}
    }

    return result;
}

WideString*
wide_string_split_native(wchar* input, wchar splitCharacter)
{
    WideString str = {
	.Length = wcslen(input),
	.Buffer = input
    };
    WideString* result = wide_string_split(str, splitCharacter);

    return result;
}

i32
wide_string_cequals(WideString str1, wchar* buffer, size_t length)
{
    if (str1.Length != length || str1.Buffer == NULL || buffer == NULL)
	return 0;

    i32 result = memcmp(str1.Buffer, buffer, str1.Length * sizeof(wchar));

    return result == 0;
}

i32
wide_string_equals(WideString str1, WideString str2)
{
    if (str1.Length != str2.Length || str1.Buffer == NULL || str2.Buffer == NULL)
	return 0;

    i32 result = memcmp(str1.Buffer, str2.Buffer, str1.Length * sizeof(wchar));

    return result == 0;
}

char*
wchar_as_char(wchar* input, size_t size)
{
    char* result = (char*) memory_allocate(size);
    wcstombs(result, input, size);
    return result;
}

//WIP
WideString
char_as_wide_string(char* input, size_t length)
{
    size_t size = length * sizeof(wchar);
    wchar* result = (wchar*) memory_allocate(size);
    mbstowcs(result, input, length);

    WideString wideString;
    wideString.Buffer = result;
    wideString.Length = length;

    return wideString;
}

WideString*
char_as_wide_string_ptr(char* input, size_t length)
{
    size_t size = length * sizeof(wchar);
    WideString* pWideString = (WideString*) memory_allocate(sizeof(WideString) + size);

    pWideString->Length = length;
    pWideString->Buffer = ((void*) pWideString) + sizeof(WideString);

    mbstowcs(pWideString->Buffer, input, length);

    return pWideString;
}


wchar*
char_as_wchar(char* input, size_t length)
{
    size_t size = length * sizeof(wchar);
    wchar* result = (wchar*) memory_allocate(size);
    wcstombs(input, result, size);
    return result;
}

char*
wide_string_as_char(WideString input)
{
    size_t size = input.Length * sizeof(wchar);
    char* result = (char*) memory_allocate(size);
    wcstombs(result, input.Buffer, size);
    return result;
}

size_t
wide_string_hash(WideString input)
{
    size_t hash = 5381;
    wchar* ptr = input.Buffer;
    wchar c;
    for (i32 i = 0; i < input.Length; ++i)
    {
	c = *ptr;
	hash = ((hash << 5) + hash) + c;
	++ptr;
    }

    return hash;
}

WideString*
iwide_string_new_utf8(IWideStringPool* iWideStringPool, char* utf8)
{
    vassert_not_null(iWideStringPool);
    vassert_not_null(utf8);

    size_t lengthUtf = wide_string_utf8_length(utf8);
    size_t sizeUtf = lengthUtf * sizeof(wchar);

    wchar translatedBuf[KB(2)] = {};
    mbstowcs(translatedBuf, utf8, lengthUtf);

    // DOCS(typedef): Find existing one
    i64 cnt = array_count(iWideStringPool->Pool);
    for (i64 i = 0; i < cnt; ++i)
    {
	WideString* pWideString = iWideStringPool->Pool[i];
	if (pWideString->Length != lengthUtf)
	    continue;

	if (memcmp(pWideString->Buffer, translatedBuf, sizeUtf) == 0)
	    return pWideString;
    }

    WideString* pNewString = char_as_wide_string_ptr(utf8, lengthUtf);
    array_push(iWideStringPool->Pool, pNewString);

    return pNewString;
}

void
wide_string_test()
{
    WideString str1 = wide_string(L"Аллоцирование памяти!");
    GINFO("Length: %d\n", str1.Length);

    printf("%ld\n", sizeof(wchar_t));
    wide_string_pl(str1);

    WideString str2 = wide_string(L"アクションが起こった");
    WideString str3 = wide_string_concat(str1, str2);
    wide_string_pl(str3);

    //wchar* f = L"Память";
    //wchar* s = L"αποκλειστικό";
    //wchar* r = wcsncat(f, s, wcslen(f)*sizeof(*f) + wcslen(s) * sizeof(*s));
    WideString str4 = wide_string_concat_native(L"Память", L" αποκλειστικό");
    wide_string_pl(str4);

    //        6   10  14 16  21
    // "Привет мир это я тебе говорю!"
    //  0123456789
    //WideString* strs = wide_string_split_native(L"Привет мир это я тебе говорю!", L' ');
    wchar* lcwstr = L"Привет мир это я тебе говорю!";
    WideString* strs = wide_string_split(wide_string(lcwstr), L' ');
    //array_foreach(strs, wide_string_print("Splitter: ", item););

    for (i32 i = 0; i < array_count(strs); ++i)
    {
	WideString wstr = strs[i];
	wide_string_pl(wstr);
    }

    const wchar* f = L"Некоторая строка с форматированием %d %f";
    WideString wformat = wide_string_format(f, 22, 3.14f);
    wide_string_print_line(wformat);

}

/*

  ###################################
  ###################################
  GlobalHelpers.c
  ###################################
  ###################################

*/
void*
_double_array_create(i32 rows, i32 cols, size_t size)
{
    size_t ptrSize = sizeof(size_t);
    size_t colSize = cols * size;
    size_t* data = (size_t*) memory_allocate(rows * ptrSize);
    for (i32 r = 0; r < rows; ++r)
    {
	data[r] = (size_t)memory_allocate(colSize);
    }

    return data;
}

void*
_double_array_destroy(size_t* darr, i32 rows)
{
    for (i32 r = 0; r < rows; ++r)
    {
	memory_free((void*)darr[r]);
    }

    memory_free(darr);
    return NULL;
}

i32
string_get_next_i32(char* stream, i32 skipChars, i32* index)
{
    /*
      0123
      ^
    */
    i32 intIndex = 0;
    stream = stream + skipChars;
    char c = *stream;
    const i32 intBufferLength = 128;
    char intBuffer[intBufferLength];

    memset(intBuffer, '\0', intBufferLength * sizeof(char));

    while (c == ' ' || c == '\t' || c == '\n' || c == 'r')
    {
	++stream;
	c = *stream;
    }

    while (c >= '0' && c <= '9')
    {
	switch (c)
	{
	case '0': case '1': case '2':
	case '3': case '4': case '5':
	case '6': case '7': case '8':
	case '9':
	{
	    intBuffer[intIndex] = c;
	    ++intIndex;
	    break;
	}
	}

	++stream;
	c = *stream;
    }

    i32 result = string_to_i32(intBuffer);

    *index = skipChars + (intIndex + 1);

    return result;
}

/*

  ###################################
  ###################################
  StringBuilder.c
  ###################################
  ###################################

*/
force_inline i32
sb_string_number_rank(i32 number)
{
    i32 rank = 0;
    for (; ;)
    {
	number /= 10;
	if (number != 0)
	{
	    ++rank;
	}
	else
	{
	    return rank;
	}
    }
}

force_inline i32
sb_string_number_of_rank(i32 number, i32 rank)
{
    if (rank <= 0)
    {
	return number;
    }

    for (i32 i = 0; i < rank; i++)
    {
	number *= 10;
    }

    return number;
}

force_inline i32
sb_string_number_of_digit(i64 number, i8 digit)
{
    i32 i;

    if (sb_string_number_rank(number) < digit)
    {
	return 0;
    }

    if (number < 0)
    {
	number *= -1;
    }

    if (digit == 0)
    {
	return (number % 10);
    }

    number %= sb_string_number_of_rank(1, (digit + 1));
    number /= sb_string_number_of_rank(1, digit);

    return number;
}

char*
_string_builder_new()
{
    size_t headerSize = sizeof(StringBuilderHeader);
    size_t newCapacity = START_ALLOCATION_SIZE;
    size_t bufferSize = newCapacity * sizeof(char);
    size_t newSize = bufferSize + headerSize;

    StringBuilderHeader* header = (StringBuilderHeader*) memory_allocate(newSize);
    header->Count = 0;
    header->Capacity = newCapacity;
    header->Buffer = (char*) (((char*)header) + headerSize);
    memset(header->Buffer, '\0', bufferSize);

    return header->Buffer;
}

char*
_string_builder_grow(char* builder, size_t newCapacity)
{
    vassert(sizeof(char) == 1);

    size_t headerSize = sizeof(StringBuilderHeader);
    size_t newSize = newCapacity + headerSize;

    StringBuilderHeader* header = string_builder_header(builder);
    StringBuilderHeader* newHeader = (StringBuilderHeader*) memory_allocate(newSize);
    newHeader->Capacity = newCapacity;
    newHeader->Count = header->Count;
    newHeader->Buffer = (char*) (((char*)newHeader) + headerSize);
    memset(newHeader->Buffer, '\0', newCapacity);
    memcpy(newHeader->Buffer, header->Buffer, (header->Count * sizeof(char)));

    memory_free(header);

    return newHeader->Buffer;
}

char*
_string_builder_appendf(char* builder, const char* format, ...)
{
    i32 state = 0;
    i32 argumentsCount = 0;
    va_list valist;

    for (char* ptr = (char*)format; *ptr != '\0'; ptr++)
    {
	char c = *ptr;
	if (c == '%')
	{
	    state = 1;
	}
	else if ((state == 1) && (c == 'c' || c == 's' || c == 'd' || c == 'b' || c == 'f'))
	{
	    state = 0;
	    ++argumentsCount;
	}
	else
	{
	    state = 0;
	}
    }

    vguard(argumentsCount > 0 && "No arguments provided!");

    state = 0;
    va_start(valist, format);
    while (*format != '\0')
    {
	char f = *format;

	switch (f)
	{
	case '%':
	{
	    state = 1;
	    break;
	}
	case 'c':
	{
	    if (state == 1)
	    {
		char elementc = (char) va_arg(valist, i32);
		string_builder_appendc(builder, elementc);

		state = 0;
	    }
	    else
	    {
		string_builder_appendc(builder, f);
	    }

	    break;
	}
	case 's':
	{
	    if (state == 1)
	    {
		const char* elements = va_arg(valist, const char *);
		string_builder_appends(builder, (char*) elements);

		state = 0;
	    }
	    else
	    {
		string_builder_appendc(builder, f);
	    }

	    break;
	}
	case 'l':
	{
	    if (state == 1)
	    {
		state = 2;
	    }
	    else
	    {
		string_builder_appendc(builder, f);
	    }

	    break;
	}
	case 'd':
	{
	    if (state == 1)
	    {
		i32 number = va_arg(valist, i32);
		char str[64];
		memset(str, '\0', 64);
		sb_string_i32_to_string(str, number);
		string_builder_appends(builder, str);

		state = 0;
	    }
	    else if (state == 2)
	    {
		i64 number = va_arg(valist, i64);
		char str[64];
		memset(str, '\0', 64);
		sb_string_i64_to_string(str, number);
		string_builder_appends(builder, str);

		state = 0;
	    }
	    else
	    {
		string_builder_appendc(builder, f);
	    }

	    break;
	}
	case 'b':
	{
	    if (state == 1)
	    {
		u8 number = (u8) va_arg(valist, i32);
		char str[4];
		memset(str, '\0', 4);
		sb_string_i32_to_string(str, number);
		string_builder_appends(builder, str);

		state = 0;
	    }
	    else
	    {
		string_builder_appendc(builder, f);
	    }

	    break;
	}
	case 'f':
	{
	    if (state == 1)
	    {
		f64 f64Number = va_arg(valist, f64);
		char str[64];
		memset(str, '\0', 64);
		sb_string_f64_to_string(str, f64Number);
		string_builder_appends(builder, str);

		state = 0;
	    }
	    else
	    {
		string_builder_appendc(builder, f);
	    }

	    break;
	}
	default:
	{
	    string_builder_appendc(builder, f);
	    state = 0;

	    break;
	}

	}

	format++;
    }

    va_end(valist);

    return builder;
}

/*

  ###################################
  ###################################
  HashTable.c
  ###################################
  ###################################

*/
i32
hash_table_i32_pow(i32 x, i32 n)
{
    i32 result = 1;
    while (n)
    {
	result *= x;
	--n;
    }

    return result;
}


/*
  Hash Table Statistics (for profiling purposes)
*/
#if HASH_TABLE_PROFILING == 1

TableStatistics gStatistics;

TableStatistics
table_get_statistics()
{
    return gStatistics;
}
#endif

/*
  Base
*/


/*
  DOCS(typedef): String Hash Table (const char* Key)
*/
typedef struct InternalStringElement
{
    const char* Key;
    void* Data;
} InternalStringElement;

typedef struct InternalIntElement
{
    i32 Key;
    void* Data;
} InternalIntElement;

i32
_get_prime(TableHeader* header)
{
    static i32 HashTablePrimes[] = {
	53, 97, 193, 389,
	769, 1543, 3079, 6151,
	12289, 24593, 49157, 98317,
	196613, 393241, 786433, 1572869,
	3145739, 6291469, 12582917, 25165843,
	50331653, 100663319, 201326611, 402653189,
	805306457, 1610612741
    };

    i32 nextIndex = (header == NULL) ? 0 : MinMax(header->NextPrime, 0, 24);
    i32 prime = HashTablePrimes[nextIndex];
    return prime;
}

void*
_table_new(void* table, size_t elemSize, i32 defVal)
{
    i32 prime = _get_prime(NULL);
    size_t newSize = prime * elemSize;
    TableHeader* newHeader = (TableHeader*) HashTableAllocate(newSize + sizeof(TableHeader));

    TableHeader tableHeader = {
	.Count = 0,
	.Capacity = prime,
	.ElementSize = elemSize,
	.NextPrime = 1,
	.Buffer = ((void*)newHeader) + sizeof(TableHeader)
    };
    *newHeader = tableHeader;

    memset(newHeader->Buffer, defVal, newSize);

    return newHeader->Buffer;
}

void*
_table_grow(void* table, size_t elemSize, i32 defVal)
{
    vguard_not_null(table);
#if STATIC_ANALIZER_CHECK == 1
    if (!table) return table;
#endif

    TableHeader* prevHeader = table_header(table);
    i32 prime = _get_prime(prevHeader);
    size_t newSize = prime * elemSize;
    TableHeader* newHeader = (TableHeader*) HashTableAllocate(newSize + sizeof(TableHeader));
    TableHeader tableHeader = {
	.Count = 0,
	.Capacity = prime,
	.ElementSize = elemSize,
	.NextPrime = prevHeader->NextPrime + 1,
	.Buffer = ((void*)newHeader) + sizeof(TableHeader)
    };
    *newHeader = tableHeader;

    memset(newHeader->Buffer, defVal, newSize);

    return newHeader->Buffer;
}

force_inline size_t
shash(const char* key, i32 prime, i32 bucketNumber)
{
#if USE_OLD_GET_SHASH_ALGO

    vassert(bucketNumber != 0);
    size_t shash = 0;
    i32 keyLength = string_length(key);
    for (i32 i = 0; i < keyLength; i++)
    {
	shash += hash_table_i32_pow(prime, (keyLength - (i + 1))) * key[i];
	shash %= bucketNumber;
    }
    return shash;

#else // djb2
    size_t hash = 5381;

    size_t length = string_length(key);
    char* end = ((char*)key) + length;
    for (char* ptr = (char*) key; ptr != end; ++ptr)
    {
	hash = ((hash << 5) + hash) + *ptr;
    }
    return hash;

#endif

}

force_inline i32
_get_shash(const char* key, i32 bucketNumber, i32 attempt)
{
    const i32 PRIME_1 = 117;
    const i32 PRIME_2 = 119;

    size_t hashA = shash(key, PRIME_1, bucketNumber);
    size_t hashB = shash(key, PRIME_2, bucketNumber);
    return (hashA + (attempt * (hashB + 1))) % bucketNumber;
}

void
_base_shash_put(void* table, const char* key)
{
    vguard_not_null(table);

    TableHeader* header = table_header(table);

    i32 i = 0,
	index,
	keyLength = string_length(key);
    char* itemsKey = (char*) key;
    do
    {
	index = _get_shash(itemsKey, header->Capacity, i);
	itemsKey = *((char**) (table + index * header->ElementSize));
	++i;

	if (itemsKey == NULL)
	{
	    ++header->Count;
	    break;
	}

	if (i >= header->Capacity)
	{
	    index = -1;
	    break;
	}
    }
    while (!string_compare_length(itemsKey, key, keyLength));

    // NOTE(typedef): always set to index wo any checks
    header->Index = index;

#if HASH_TABLE_PROFILING == 1
    gStatistics.PutAttempt = i;
#endif
}

void
_base_shash_get(void* table, const char* key)
{
    vguard_not_null(table);

    TableHeader* header = table_header(table);

    i64 i = 0,
	index,
	keyLength = string_length(key);
    char* itemsKey = (char*) key;

    do
    {
	index = _get_shash(itemsKey, header->Capacity, i);
	itemsKey = *((char**) (table + index * header->ElementSize));
	++i;

	if (itemsKey == NULL || i >= header->Capacity)
	{
	    index = -1;
	    break;
	}

    }
    while (!string_compare_length(itemsKey, key, keyLength));

    header->Index = index;
#if HASH_TABLE_PROFILING == 1
    gStatistics.GetAttempt = i;
#endif
}

/*
  DOCS(typedef): Int Hash Table (int Key)
*/

force_inline i32
hash(i32 key)
{
#if 1

    u32 ukey = (u32) key;
    ukey = ((ukey >> 16) ^ ukey) * 0x45d9f3b;
    ukey = ((ukey >> 16) ^ ukey) * 0x45d9f3b;
    ukey = (ukey >> 16) ^ ukey;
    return i32(key % I32_MAX_HALF);

#else

    u32 ukey = (u32) key;
    ukey += ~(key << 9);
    ukey ^= ((key >> 14) | (key << 18));
    ukey += (key << 4);
    ukey ^= ((key >> 10) | (key << 22));
    return i32(key);

#endif
}

force_inline i32
get_hash(i32 key, i32 bucketNumber, i32 attempt)
{
    i32 hashA = hash(key);
    i32 hashB = attempt * (hash(key) + 1);
    return (hashA + hashB) % bucketNumber;
}

void*
_base_hash_put(void* table, i32 key)
{
    vguard_not_null(table);

    TableHeader* header = table_header(table);

    i32 i = 0,
	index,
	itemsKey = key;
    do
    {
	index = get_hash(itemsKey, header->Capacity, i);
	itemsKey = *((i32*) (table + index * header->ElementSize));
	++i;

	if (itemsKey == -1)
	{
	    ++header->Count;
	    break;
	}

	if (i >= header->Capacity)
	{
	    index = -1;
	    break;
	}

    } while (itemsKey != key);

    header->Index = index;

#if HASH_TABLE_PROFILING == 1
    gStatistics.PutAttempt = i;
#endif

    return table;
}

void*
_base_hash_get(void* table, i32 key)
{
    vguard_not_null(table);

    TableHeader* header = table_header(table);
    i32 i = 0, index, itemsKey = key;
    do
    {
	index = get_hash(itemsKey, header->Capacity, i);
	itemsKey = *((i32*) (table + index * header->ElementSize));
	++i;

	if (itemsKey == -1 || i >= header->Capacity)
	{
	    index = -1;
	    break;
	}

    } while (itemsKey != -1 && itemsKey != key);

    header->Index = index;

#if HASH_TABLE_PROFILING == 1
    gStatistics.GetAttempt = i;
#endif

    return table;
}

/*
  DOCS(typedef): Wide Hash Table (WideString Key)
*/


// DOCS(typedef): djb2 algorithm
size_t
whash(WideString key)
{
    size_t hash = 5381;
    wchar* ptr = key.Buffer;
    for (i32 i = 0; i < key.Length; ++i)
    {
	hash = ((hash << 5) + hash) + *ptr;
	++ptr;
    }

    return hash;

}

force_inline i32
whash_double(WideString key, i32 bucketNumber, i32 attempt)
{
    size_t hashA = whash(key);
    size_t hashB = whash(key);
    return (hashA + (attempt * (hashB + 1))) % bucketNumber;
}

void
_base_whash_put(void* table, WideString key)
{
    vguard_not_null(table);

    TableHeader* header = table_header(table);

    i32 i = 0,
	index;
    WideString itemsKey = key;
    do
    {
	index = whash_double(itemsKey, header->Capacity, i);
	itemsKey = *((WideString*) (table + index * header->ElementSize));
	++i;

	if (itemsKey.Buffer == NULL)
	{
	    ++header->Count;
	    break;
	}

	if (i >= header->Capacity)
	{
	    index = -1;
	    break;
	}
    }
    while (!wide_string_equals(itemsKey, key));

    header->Index = index;
}

void
_base_whash_get(void* table, WideString key)
{
    vguard_not_null(table);

    TableHeader* hdr = table_header(table);

    i64 i = 0,
	index;
    WideString itemsKey = key;

    do
    {
	index = whash_double(itemsKey, hdr->Capacity, i);
	itemsKey = *((WideString*) (table + index * hdr->ElementSize));
	++i;

	if (itemsKey.Buffer == NULL || i >= hdr->Capacity)
	{
	    index = -1;
	    break;
	}

    }
    while (!wide_string_equals(itemsKey, key));

    hdr->Index = index;
}

/*

  #####################################
  #####################################
  IO.c
  #####################################
  #####################################

*/

#if defined(LINUX_PLATFORM)

#include <sys/stat.h>  // mkdir
#include <sys/types.h>  // mkdir

char*
file_read_string_ext(const char* filePath, size_t* length)
{
    FILE* file;
    char* result;
    i32 fileLength;

    file = fopen(filePath, "r");
    if (file)
    {
	fseek(file, 0, SEEK_END);
	fileLength = (ftell(file));
	fseek(file, 0, SEEK_SET);
	result = memory_allocate((fileLength + 1) * sizeof(char));

	fread(result, sizeof(char), (fileLength), file);
	result[fileLength] = '\0';

	fclose(file);

	*length = fileLength;
	return((char*)result);
    }

    *length = 0;
    return NULL;
}

char*
file_read_string(const char* filePath)
{
    size_t length;
    return file_read_string_ext(filePath, &length);
}

char*
file_get_name_with_extension(const char* path)
{
    i32 lastIndex = 0;
    i32 pathLength = string_length(path);
    i32 i;

    for (i = (pathLength - 1); i >= 0; --i)
    {
	char c = path[i];
	if (c == '/')
	{
	    lastIndex = i;
	    break;
	}
    }

    i32 nameIndex = (lastIndex + 1);
    i32 newLength = pathLength - nameIndex;
    char* fileName = memory_allocate((newLength + 1) * sizeof(char));
    for (i = nameIndex; i < pathLength; ++i)
	fileName[i - nameIndex] = path[i];

    fileName[newLength] = '\0';

    return fileName;
}

void
file_write_string(const char* filePath, char* data, size_t len)
{
    FILE* file = fopen(filePath, "w");
    vguard_not_null(file);
#if STATIC_ANALIZER_CHECK == 1
    if (!file) return;
#endif
    fwrite(data, 1, len, file);
    fclose(file);
}

void
file_write_bytes(const char* filePath, u8* data, size_t len)
{
    FILE* file = fopen(filePath, "wb");
    vguard_not_null(file);
#if STATIC_ANALIZER_CHECK == 1
    if (!file) return;
#endif
    fwrite(data, 1, len, file);
    fclose(file);
}

i32
file_write_string_exe(const char* filePath, char* data, size_t len)
{
    i32 fileDescriptor = open(filePath, O_RDWR | O_CREAT | O_EXCL, S_IRWXU);
    if (fileDescriptor >= 0)
    {
	write(fileDescriptor, data, len);
	close(fileDescriptor);
	return 1;
    }
    return 0;
}

void
file_append_string(const char* filePath, char* data, size_t len)
{
    FILE* file;
    file = fopen(filePath, "a+");
    fwrite(data, 1, len, file);
    fclose(file);
}

u8*
file_read_bytes_ext(const char* filePath, size_t* sizePtr)
{
    FILE* file;
    u8* result;
    size_t size;

    file = fopen(filePath, "rb");
    if (file)
    {
	fseek(file, 0, SEEK_END);
	size = (i32)ftell(file);
	fseek(file, 0, SEEK_SET);
	result = memory_allocate(size * sizeof(u8));

	fread(result, sizeof(u8), size, file);
	*sizePtr = size;

	fclose(file);
	return (u8*) result;
    }

    return NULL;
}

u8*
file_read_bytes(const char* filePath)
{
    size_t size;
    u8* result = file_read_bytes_ext(filePath, &size);
    return result;
}

i32
file_get_size(const char* filePath)
{
    FILE* file = fopen(filePath, "rb");
    if (file)
    {
	fseek(file, 0, SEEK_END);
	i32 fileLength = (i32)ftell(file);
	fclose(file);
	return fileLength;
    }

    return 0;
}

i32
platform_directory_create(const char* name)
{
    /*
      #define S_IRWXU 0000700    RWX mask for owner
      #define S_IRUSR 0000400    R for owner
      #define S_IWUSR 0000200    W for owner
      #define S_IXUSR 0000100    X for owner

      #define S_IRWXG 0000070    RWX mask for group
      #define S_IRGRP 0000040    R for group
      #define S_IWGRP 0000020    W for group
      #define S_IXGRP 0000010    X for group

      #define S_IRWXO 0000007    RWX mask for other
      #define S_IROTH 0000004    R for other
      #define S_IWOTH 0000002    W for other
      #define S_IXOTH 0000001    X for other

      #define S_ISUID 0004000    set user id on execution
      #define S_ISGID 0002000    set group id on execution
      #define S_ISVTX 0001000    save swapped text even after use
    */
    i32 result = mkdir(name, S_IRWXU);
    if (result != 0)
    {

	return 0;
    }

    return 1;
}

#elif defined(WINDOWS_PLATFORM)

char*
file_read_string_ext(const char* filePath, size_t* length)
{
    HANDLE fileHandle = CreateFileA(filePath, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if (fileHandle == INVALID_HANDLE_VALUE)
    {
	*length = 0;
	return NULL;
    }

    LARGE_INTEGER byteSize;
    BOOL isSucessed = GetFileSizeEx(fileHandle, &byteSize);

    size_t newSize = byteSize.QuadPart + 1;
    *length = byteSize.QuadPart;

    char* fileBuffer = (char*) memory_allocate(newSize);
    fileBuffer[byteSize.QuadPart] = '\0';
    BOOL isReadSuccessed = ReadFile(fileHandle, fileBuffer, byteSize.QuadPart, NULL, NULL);

    CloseHandle(fileHandle);

    return (char*) fileBuffer;
}

char*
file_read_string(const char* filePath)
{
    size_t length;
    return file_read_string_ext(filePath, &length);
}

char*
file_get_name_with_extension(const char* path)
{
    vguard_not_impl();
}

void
file_write_string(const char* filePath, char* data, size_t len)
{
    file_write_bytes(filePath, data, len);
}

void
file_write_bytes(const char* filePath, u8* data, size_t len)
{
    HANDLE fileHandle = CreateFileA(filePath, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
    if (fileHandle == INVALID_HANDLE_VALUE)
	return;

    BOOL isWriteFile = WriteFile(fileHandle, data, len, NULL, NULL);
}

i32
file_write_string_exe(const char* filePath, char* data, size_t len)
{
    HANDLE fileHandle = CreateFileA(filePath, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
    if (fileHandle == INVALID_HANDLE_VALUE)
	return 0;

    BOOL isWriteFile = WriteFile(fileHandle, data, len, NULL, NULL);
    return isWriteFile;
}

void
file_append_string(const char* filePath, char* data, size_t len)
{
    vguard_not_impl();
}

u8*
file_read_bytes_ext(const char* filePath, size_t* length)
{
    HANDLE fileHandle = CreateFileA(filePath, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if (fileHandle == INVALID_HANDLE_VALUE)
    {
	*length = 0;
	return NULL;
    }

    LARGE_INTEGER byteSize;
    BOOL isSucessed = GetFileSizeEx(fileHandle, &byteSize);

    size_t newSize = byteSize.QuadPart;
    *length = newSize;

    char* fileBuffer = (char*) memory_allocate(newSize);
    BOOL isReadSuccessed = ReadFile(fileHandle, fileBuffer, newSize, NULL, NULL);

    CloseHandle(fileHandle);

    return (u8*) fileBuffer;
}

u8*
file_read_bytes(const char* filePath)
{
    size_t size;
    u8* bytes = file_read_bytes_ext(filePath, &size);
    return bytes;
}

i32
file_get_size(const char* filePath)
{
    HANDLE fileHandle = CreateFileA(filePath, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if (fileHandle == INVALID_HANDLE_VALUE)
	return -1;

    LARGE_INTEGER byteSize;
    BOOL isSucessed = GetFileSizeEx(fileHandle, &byteSize);
    if (!isSucessed)
	return -1;

    return byteSize.QuadPart;
}

i32
platform_directory_create(const char* name)
{
    if (!CreateDirectoryA(name, NULL))
	return 0;
    return 1;
}

#else
#error "No platform support!"
#endif

/*

  #####################################
  #####################################
  Path.c
  #####################################
  #####################################

*/

static char CurrentDirectory[4096] = "\0";
static IElement** Elements = NULL;

char*
ielement(const char* directory, const char* name)
{
    i32 i, count = array_count(Elements);
    for (i = 0; i < count; ++i)
    {
	IElement* ielement = Elements[i];
	if (string_compare(ielement->Directory, directory)
	    && string_compare(ielement->Name, name))
	{
	    return ielement->AbsolutePath;
	}
    }

    i32 lastSlashIndex = string_last_index_of(name, '/');
    i32 extIndex = string_last_index_of(name, '.');

    i32 dirLength = string_length(directory);
    i32 nameLength = string_length(name);
    i32 absoluteLength = dirLength + 1 + nameLength;

    if (lastSlashIndex != -1)
    {
	--absoluteLength;
    }

    size_t headerSize = sizeof(IElement);
    i32 dirSize = dirLength * sizeof(char);
    size_t nameSize = nameLength * sizeof(char);
    size_t absolutePathSize = absoluteLength * sizeof(char);

    /*
      /home/bies/dir/file.ext

      char* AbsolutePath;       /home/bies/dir/file.ext => new
      char* Name;               file\0                  => new
      char* Directory;          /home/bies/dir/\0       => new
      char* NameWithExtension;  file.ext\0              => poap
      char* Extension;          .ext\0                  => poap
    */

    IElement* element = memory_allocate(headerSize
					+ absolutePathSize + 1
					+ nameSize + 1
					+ dirSize + 1);
    element->AbsolutePathLength = absoluteLength;
    element->DirLength = dirLength;
    element->NameLength = nameLength;
    element->AbsolutePath = ((void*)element) + headerSize;
    element->Directory = ((void*)element->AbsolutePath) + absolutePathSize + 1;
    element->Name = ((void*)element->Directory) + dirSize + 1;
    if (lastSlashIndex != -1)
    {
	element->NameWithExtension = element->AbsolutePath + lastSlashIndex + 1;
    }
    else
    {
	element->NameWithExtension = STRING_NULL;
    }
    if (extIndex != -1 && extIndex != 0)
    {
	element->Extension = element->Name + extIndex + 1;
    }
    else
    {
	element->Extension = STRING_NULL;
    }

    memset(element->AbsolutePath, '\0', (absolutePathSize + 1));
    memset(element->Directory, '\0', (dirSize + 1));
    memset(element->Name, '\0', (nameSize + 1));

    char lastChar = directory[dirLength - 1];
    memcpy(element->AbsolutePath, directory, dirSize);
    if (lastChar != '/' && lastChar != '\\')
    {
	memcpy(element->AbsolutePath + dirSize, "/", sizeof(char));
	memcpy(element->AbsolutePath + dirSize + 1, name, nameSize);
    }
    else
    {
	memcpy(element->AbsolutePath + dirSize, name, nameSize);
    }
    memcpy(element->Directory, directory, dirSize);
    memcpy(element->Name, name, nameSize);

    array_push(Elements, element);

    return element->AbsolutePath;
}

void
ielement_free_all()
{
    i32 i, count = array_count(Elements);
    for (i = 0; i < count; ++i)
    {
	IElement* element = Elements[i];
	memory_free(element);
    }

    array_free(Elements);

    Elements = NULL;
}

u8
path(const char* path)
{
#if defined(LINUX_PLATFORM)
    struct stat fileInfo;

    if (stat(path, &fileInfo) != 0)
    {
	return PATH_IS_SOMETHING;
    }

    if (S_ISDIR(fileInfo.st_mode))
    {
	return PATH_IS_DIRECTORY;
    }
    else if (S_ISREG(fileInfo.st_mode))
    {
	return PATH_IS_FILE;
    }

    return PATH_IS_SOMETHING;
#elif defined(WINDOWS_PLATFORM)
    DWORD fileAttribute = GetFileAttributes(path);
    if (fileAttribute == INVALID_FILE_ATTRIBUTES)
    {
	GERROR("GetFileAttributes failed!\n");
	vassert_break();
    }

    if (fileAttribute == FILE_ATTRIBUTE_DIRECTORY)
    {
	return PATH_IS_DIRECTORY;
    }
    else
    {
	return PATH_IS_FILE;
    }
#else
#error "Unsupported platform!"
#endif
}

#if defined(WINDOWS_PLATFORM)
//TODO(typedef): create this function
//TODO(typedef): make this function cross platform
static void
io_get_file_times_ms(const char* path, i64* accessTime, i64* creationTime, i64* writeTime)
{
    HANDLE file = CreateFileA(path, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if (file == INVALID_HANDLE_VALUE)
    {
	GERROR("Can't open file: %s\n", path);
	vassert_break();
    }
    FILETIME ct, at, wt;
    BOOL result = GetFileTime(file, &ct, &at, &wt);
    if (result == FALSE)
    {
	GERROR("Can't get file time: %s\n", path);
	vassert_break();
    }

    SYSTEMTIME systemAccessTime;
    FileTimeToSystemTime(&at, &systemAccessTime);
    SYSTEMTIME systemCreationTime;
    FileTimeToSystemTime(&ct, &systemCreationTime);
    SYSTEMTIME systemWriteTime;
    FileTimeToSystemTime(&wt, &systemWriteTime);

    *accessTime = systemAccessTime.wMilliseconds;
    *creationTime = systemCreationTime.wMilliseconds;
    *writeTime = systemWriteTime.wMilliseconds;
}
#endif

size_t
path_get_last_access_time_raw(const char* path)
{
#if defined(LINUX_PLATFORM)
    struct stat fileItemInfo;
    stat(path, &fileItemInfo);
    return fileItemInfo.st_atime;
#elif defined(WINDOWS_PLATFORM)
#warning "This function should return generic cross platform struct FileTime"
    i64 at, ct, wt;
    io_get_file_times_ms(path, &at, &ct, &wt);
    return at;
#else
#error "Unsupported platform!"
#endif
}

size_t
path_get_last_modification_time_raw(const char* path)
{
#if defined(LINUX_PLATFORM)
    struct stat fileItemInfo;
    stat(path, &fileItemInfo);
    return fileItemInfo.st_mtime;
#elif defined(WINDOWS_PLATFORM)
    i64 at, ct, wt;
    io_get_file_times_ms(path, &at, &ct, &wt);
    return wt;
#else
#error "Unsupported platform!"
#endif
}

size_t
path_get_last_creation_time_raw(const char* path)
{
#if defined(LINUX_PLATFORM)
    struct stat fileItemInfo;
    stat(path, &fileItemInfo);
    return fileItemInfo.st_ctime;
#elif defined(WINDOWS_PLATFORM)
    i64 at, ct, wt;
    io_get_file_times_ms(path, &at, &ct, &wt);
    return ct;
#else
#error "Unsupported platform!"
#endif
}

char*
path_get_home_directory()
{
#if defined(LINUX_PLATFORM)
    static struct passwd* g_UserInfo = NULL;
    if (!g_UserInfo)
    {
	g_UserInfo = getpwuid(geteuid());
    }

    return g_UserInfo->pw_dir;
#elif defined(WINDOWS_PLATFORM)
    return "C:/";
#else
#error "Unsupported platform!"
#endif
}

const char*
path_get_extension(const char* path)
{
    /*
      DOCS(typedef): this("path/to/file.txt") -> ".txt"
    */
    i32 extensionIndex = string_last_index_of(path, '.');
    return (const char*)(path + extensionIndex * sizeof(char));
}

const char*
path_get_name(const char* path)
{
    i32 extensionIndex = string_last_index_of(path, '/');
    return (const char*)(path + (extensionIndex + 1) * sizeof(char));
}

char*
path_get_name_wo_extension(const char* path)
{
    const char* nameWithExt = path_get_name(path);
    i32 ind = string_index_of(nameWithExt, '.');
    char* nameWoExt = string_substring_length(
	nameWithExt,
	string_length(nameWithExt),
	ind);
    return nameWoExt;
}

char*
path_get_directory(const char* path)
{
    //"path/one/t";
    //;0123456789;;
    i32 extensionIndex = string_last_index_of(path, '/');
    char* newPath = (char*)memory_allocate((extensionIndex + 1) * sizeof(char));
    newPath[extensionIndex] = '\0';
    memcpy(newPath, path, extensionIndex * sizeof(char));
    return newPath;
}

#if defined(LINUX_PLATFORM)
char*
path_combine(const char* left, const char* right)
{
    vguard_not_null(left );
    vguard_not_null(right);

    /*
      1. Adding paths:
      * path_combine("Image/", "a.png");
      * path_combine("Image/", "/a.png");
      * path_combine("Image/", "./a.png");

      */

    i64 leftLength = string_length(left);
    i64 rightLength = string_length(right);

    i32 isLeftSlashed = path_contains_slash(left, leftLength);

    char* leftMod  = NULL;
    char* rightMod = NULL;
    i64 leftModLen;
    i64 rightModLen;

    if (isLeftSlashed)
    {
	leftModLen = leftLength - 1;
	leftMod = string_copy(left, leftModLen);
    }
    else
    {
	leftModLen = leftLength;
	leftMod = string_copy(left, leftLength);
    }

    { // DOCS(typedef): set rightMod
	char* ptr = (char*) right;
	char c = *ptr;
	while (c == '.' || c == '/' || c == '\\' || c == ' ' || c == '\n' || c == '\t' || c == '\r')
	{
	    ++ptr;
	    c = *ptr;
	}

	// DOCS(typedef): right is empty string for us
	if (*ptr == '\0')
	{
	    if (leftMod)
		memory_free(leftMod);
	    return string(right);
	}

	rightMod = string(ptr);
	rightModLen = string_length(rightMod);
    }

    char* result = string_concat3l(
	leftMod   , "/", rightMod,
	leftModLen,  1 , rightModLen);
    // GINFO("Result: %s\n", result);

    if (isLeftSlashed)
    {
	memory_free(leftMod);
    }

    memory_free(rightMod);

    return result;
}

#elif defined(WINDOWS_PLATFORM)

char*
path_combine(const char* left, const char* right)
{
    vguard_not_null(left);
    vguard_not_null(right);

    char outPath[MAX_PATH] = {};

    PathCombineA(outPath, left, right);
    i64 resultLen = string_length(outPath);
    char* resultAllocated = memory_allocate(resultLen + 1);
    resultAllocated[resultLen] = '\0';
    memcpy(resultAllocated, outPath, resultLen);
    return resultAllocated;
}

#endif

char*
path_combine3(const char* left, const char* mid, const char* right)
{
    char* lm = path_combine(left, mid);
    char* lmr = path_combine(lm, right);
    memory_free(lm);
    return lmr;
}

char*
path_combine_directory_and_name(const char* path, char* name)
{
    char* dirPath = path_get_directory(path);
    char* result = path_combine(dirPath, name);
    memory_free((void*)dirPath);
    return result;
}

const char*
path_combine_interning(const char* left, const char* right)
{
    assert(left != NULL && "Left can't be NULL!");
    assert(right != NULL && "Right can't be NULL!");

    char* path = path_combine(left, right);
    const char* iPath = istring(path);
    memory_free(path);
    return iPath;
}

i32
path_contains_slash(const char* path, i64 pathLength)
{
    char windowsSlash = '\\';
    char normalSlash = '/';
    i64 slashInd;

#define SlashCheck(p, s)                                        \
    {                                                           \
	slashInd = string_last_index_of(p, s);                  \
	if (slashInd != -1 && (slashInd == (pathLength - 1)))	\
	{                                                       \
	    return 1;                                           \
	}                                                       \
    }

    SlashCheck(path, normalSlash);
    SlashCheck(path, windowsSlash);

    return 0;
}


const char*
path_get_current_directory()
{
    if (CurrentDirectory[0] == '\0')
    {
	getcwd(CurrentDirectory, 4096);
    }
    return (const char*)CurrentDirectory;
}

char*
path_get_absolute(char* path)
{
    const char* currentDirectory = path_get_current_directory();
    char* absolutePath = string_concat3(currentDirectory, PATH_SEPARATOR_STRING, path);
    return absolutePath;
}

i32
path_is_file_exist(const char* path)
{
#if defined(LINUX_PLATFORM)
    struct stat buf;
    i32 result = stat(path, &buf);
    return result != -1;
#elif defined(WINDOWS_PLATFORM)
    BOOL isFileExist = PathFileExistsA(path);
    return isFileExist;
#endif
}

i32
path_is_directory_exist(const char* path)
{
#if defined(LINUX_PLATFORM)
    struct stat buf;
    i32 result = stat(path, &buf);
    return result != -1;
#elif defined(WINDOWS_PLATFORM)
    DWORD result = GetFileAttributesA(path);
    return (result != INVALID_FILE_ATTRIBUTES && (result & FILE_ATTRIBUTE_DIRECTORY));
#endif
}

char*
path_get_filename(const char* path)
{
    i32 index = string_last_index_of(path, '/');
    if (index)
    {
	char* result = string_substring(path, (index + 1));
	return result;
    }

    return NULL;
}

const char*
path_get_filename_interning(const char* path)
{
    //index can be -1
    i32 index = string_last_index_of(path, '/');
    const char* iPath = istring(path + index + 1);
    return iPath;
}

char*
path_get_prev_directory(const char* currentDirectory)
{
    if (!string_compare(currentDirectory, ROOT_DIRECTORY))
    {
	i32 index = string_last_index_of(currentDirectory, '/');
	if (index != 0)
	{
	    --index;
	}

	char* prevDirectoryPath = string_substring_range(currentDirectory, 0, index);
	return prevDirectoryPath;
    }

    return NULL;
}

const char*
path_get_prev_directory_interning(const char* currentDirectory)
{
    char* prevDirectory = path_get_prev_directory(currentDirectory);
    const char* iPrevDirectory = istring(prevDirectory);
    memory_free(prevDirectory);
    return iPrevDirectory;
}

#if defined(LINUX_PLATFORM)
static i32
path_string_comparer(const struct dirent** a, const struct dirent** b)
{
    char* left = (char*)(*a)->d_name;
    char* right = (char*)(*b)->d_name;
    u32 leftLength = string_length(left);
    u32 rightLength = string_length(right);

    for (u32 i = 0; i < leftLength; i++)
    {
	char l = char_to_lower(left[i]);
	char r = char_to_lower(right[i]);

	if (l < r)
	{
	    return 1;
	}
	else if (l > r)
	{
	    return -1;
	}
    }

    return (rightLength - leftLength);
}

i32
directory_create(const char* name)
{
    return platform_directory_create(name);
}

i32
path_directory_create(const char* path)
{
    return platform_directory_create(path);
}

force_inline const char**
_directory_get(const char* directory, i32 elemCode)
{
    const char** elements = NULL;
    struct dirent** namelist = NULL;
    i32 n = scandir(directory, &namelist, 0, path_string_comparer);

    while (n > 0)
    {
	const char* dName = namelist[n - 1]->d_name;
	assert(dName);

	if (dName[0] == '.')
	{
	    --n;
	    continue;
	}

	char* absolutePath = ielement(directory, dName);
	if (path(absolutePath) == elemCode)
	{
	    array_push(elements, absolutePath);
	}

	free(namelist[n - 1]);

	--n;
    }

    free(namelist);

    return elements;
}

const char**
path_directory_get_files(const char* directory)
{
    const char** files = _directory_get(directory, PATH_IS_FILE);
    return files;
}

const char**
path_directory_get_directories(const char* directory)
{
    const char** dirs = _directory_get(directory, PATH_IS_DIRECTORY);
    return dirs;
}
#elif defined(WINDOWS_PLATFORM)

i32
path_directory_create(const char* dirPath)
{
    i32 result = platform_directory_create(dirPath);
    return result;
}

static char**
_directory_get(const char* directory, i32 elemCode)
{
    char** elements = NULL;
    WIN32_FIND_DATA findData;
    HANDLE firstFile = FindFirstFile(directory, &findData);
    if (firstFile == INVALID_HANDLE_VALUE)
    {
	return NULL;
    }

    do
    {
	if (elemCode == 0)
	{
	    if (!(findData.dwFileAttributes & elemCode))
	    {
		array_push(elements, findData.cFileName);
	    }
	}
	else
	{
	    if (findData.dwFileAttributes & elemCode)
	    {
		array_push(elements, findData.cFileName);
	    }
	}
    } while (FindNextFile(firstFile, &findData) != 0);

    return elements;
}

const char**
path_directory_get_files(const char* directory)
{
    const char** files = _directory_get(directory, 0);
    return files;
}

const char**
path_directory_get_directories(const char* directory)
{
    const char** dirs = _directory_get(directory, FILE_ATTRIBUTE_DIRECTORY);
    return dirs;
}
#endif // WINDOWS_PLATFORM

i32
path_is_inside(const char* path, const char* item)
{
    // NOTE(): Only for Linux now
    char** parentDirs = string_split((char*)path, '/');
    char** itemDirs = string_split((char*)item, '/');

    i32 parentCnt = array_count(parentDirs);
    i32 itemCnt = array_count(itemDirs);

    if (parentCnt <= 0 || itemCnt <= 0)
	return 0;

    if (parentCnt >= itemCnt)
	return 0;

    for (i32 i = 0; i < parentCnt; ++i)
    {
	char* pdir = parentDirs[i];
	char* idir = itemDirs[i];

	if (!string_compare(pdir, idir))
	    return 0;
    }

    return 1;
}

/*

  #####################################
  #####################################
  Profiler.c
  #####################################
  #####################################

*/

void
profiler_start(TimeState* state)
{
#ifdef LINUX_PLATFORM
    clock_gettime(CLOCK_REALTIME, &state->Start);
#elif WINDOWS_PLATFORM
    QueryPerformanceCounter(&state->Start);
#endif
}

void
profiler_end(TimeState* state)
{
#ifdef LINUX_PLATFORM
    clock_gettime(CLOCK_REALTIME, &state->End);
    state->Result = (1000 * 1000 * 1000 * (state->End.tv_sec - state->Start.tv_sec)) + (state->End.tv_nsec - state->Start.tv_nsec);
#elif WINDOWS_PLATFORM
    QueryPerformanceCounter(&state->End);
    state->Result = state->End.QuadPart - state->Start.QuadPart;
#endif
}

ProfilerTimeType
profiler_get_time_type(TimeState* state)
{
    if (PROFILER_NS_TO_S(state->Result))
    {
	return PROFILER_TIME_S;
    }
    else if (PROFILER_NS_TO_MS(state->Result))
    {
	return PROFILER_TIME_MS;
    }
    else if (PROFILER_NS_TO_MCS(state->Result))
    {
	return PROFILER_TIME_MCS;
    }
    else if (state->Result)
    {
	return PROFILER_TIME_NS;
    }

    assert(0 && "");
    return PROFILER_TIME_NS;
}

i64
profiler_get_nanoseconds(TimeState* state)
{
    return state->Result;
}

i64
profiler_get_microseconds(TimeState* state)
{
    return state->Result / 1000;
}

i64
profiler_get_milliseconds(TimeState* state)
{
    return state->Result / (1000 * 1000);
}

i64
profiler_get_seconds(TimeState* state)
{
    return state->Result / (1000 * 1000 * 1000);
}

f64
profiler_get_microseconds_as_float(TimeState* state)
{
    return ((f64)state->Result) / 1000;
}

f64
profiler_get_milliseconds_as_float(TimeState* state)
{
    return ((f64)state->Result) / (1000 * 1000);
}

f64
profiler_get_seconds_as_float(TimeState* state)
{
    return ((f64)state->Result) / (1000 * 1000 * 1000);
}

void
profiler_print(TimeState* state)
{
    ProfilerTimeType timeType = profiler_get_time_type(state);
    switch (timeType)
    {
    case PROFILER_TIME_NS:
	printf("%ld %s\n", profiler_get_nanoseconds(state), "ns");
	break;
    case PROFILER_TIME_MCS:
	printf("%ld %s\n", profiler_get_microseconds(state), "mcs");
	break;
    case PROFILER_TIME_MS:
	printf("%ld %s\n", profiler_get_milliseconds(state), "ms");
	break;
    case PROFILER_TIME_S:
	printf("%ld %s\n", profiler_get_seconds(state), "s");
	break;
    default:
	assert(0 && "Just a thing to delete compiler warning message, this code never ever ll be executed!");
	break;
    }
}

void
profiler_print_as_float(TimeState* state)
{
    ProfilerTimeType timeType = profiler_get_time_type(state);

    switch (timeType)
    {
    case PROFILER_TIME_NS:
	printf("%ld %s\n", profiler_get_nanoseconds(state), "ns");
	break;
    case PROFILER_TIME_MCS:
	printf("%.4f %s\n", profiler_get_microseconds_as_float(state), "mcs");
	break;
    case PROFILER_TIME_MS:
	printf("%.4f %s\n", profiler_get_milliseconds_as_float(state), "ms");
	break;
    case PROFILER_TIME_S:
	printf("%.4f %s\n", profiler_get_seconds_as_float(state), "s");
	break;
    default:
	assert(0 && "Just a thing to delete compiler warning message, this code never ever ll be executed!");
	break;
    }
}

static char g_TimeString[512];

char*
profiler_get_string(TimeState* state)
{
    ProfilerTimeType timeType = profiler_get_time_type(state);

    switch (timeType)
    {
    case PROFILER_TIME_NS:
	string_format(g_TimeString, "%ld %s", profiler_get_nanoseconds(state), "ns");
	break;
    case PROFILER_TIME_MCS:
	string_format(g_TimeString, "%ld %s", profiler_get_microseconds(state), "mcs");
	break;
    case PROFILER_TIME_MS:
	string_format(g_TimeString, "%ld %s", profiler_get_milliseconds(state), "ms");
	break;
    case PROFILER_TIME_S:
	string_format(g_TimeString, "%ld %s", profiler_get_seconds(state), "s");
	break;
    default:
	assert(0 && "Just a thing to delete compiler warning message, this code never ever ll be executed!");
	break;
    }

    return (char*)g_TimeString;
}

char*
profiler_get_string_as_float(TimeState* state)
{
    ProfilerTimeType timeType = profiler_get_time_type(state);

    switch (timeType)
    {
    case PROFILER_TIME_NS:
    {
	f64 temp = profiler_get_nanoseconds(state);
	string_format(g_TimeString, "%f %s\n", temp, "ns");
	break;
    }
    case PROFILER_TIME_MCS:
    {
	f64 temp = profiler_get_microseconds_as_float(state);
	string_format(g_TimeString, "%f %s\n", temp, "mcs");
	break;
    }
    case PROFILER_TIME_MS:
    {
	f64 temp = profiler_get_milliseconds_as_float(state);
	string_format(g_TimeString, "%f %s\n", temp, "ms");
	break;
    }
    case PROFILER_TIME_S:
    {
	f64 temp = profiler_get_seconds_as_float(state);
	string_format(g_TimeString, "%f %s\n", temp, "s");
	break;
    }
    default:
    {
	assert(0 && "Just a thing to delete compiler warning message, this code never ever ll be executed!");
	break;
    }
    }

    return (char*)g_TimeString;
}

/*

  #####################################
  #####################################
  SimpleImage.c
  #####################################
  #####################################

*/

SimpleImage*
simple_image_create(void* data, i32 width, i32 height, i32 channels)
{
    size_t widthHeight = width * height;
    size_t size = widthHeight * channels;
    SimpleImage* pSimpleImage = (SimpleImage*) memory_allocate(4 * widthHeight + sizeof(SimpleImage));
    pSimpleImage->Width  = width;
    pSimpleImage->Height = height;
    pSimpleImage->Data = (((void*)pSimpleImage) + sizeof(SimpleImage));

    switch (channels)
    {
    case 1:
    {
	RGBA* byteWritePtr = (RGBA*) pSimpleImage->Data;
	R   * byteReadPtr  = (R   *) data;
	for (i32 h = 0; h < height; ++h)
	{
	    for (i32 w = 0; w < width; ++w)
	    {
		byteWritePtr->R = byteReadPtr->R;
		byteWritePtr->G = 0;
		byteWritePtr->B = 0;
		byteWritePtr->A = 255;

		++byteWritePtr;
		++byteReadPtr;
	    }
	}

	break;
    }

    case 3:
    {
	RGBA* byteWritePtr = (RGBA*) pSimpleImage->Data;
	RGB * byteReadPtr  = (RGB *) data;
	for (i32 h = 0; h < height; ++h)
	{
	    for (i32 w = 0; w < width; ++w)
	    {
		byteWritePtr->R = byteReadPtr->R;
		byteWritePtr->G = byteReadPtr->G;
		byteWritePtr->B = byteReadPtr->B;
		byteWritePtr->A = 255;

		++byteWritePtr;
		++byteReadPtr;
	    }
	}

	break;
    }

    case 4:
    {
	memcpy(pSimpleImage->Data, data, size);
	break;
    }

    default:
    {
	GERROR("Texture channels: %d\n", channels);
	vguard(0 && "Wtf?? Number of channels is wrong!");
	break;
    }

    }

    return pSimpleImage;
}

SimpleImage*
simple_image_load_from_disk(const char* path)
{
    size_t size;
    SimpleImage* pSimpleImage = (SimpleImage*) file_read_bytes_ext(path, &size);
    pSimpleImage->Data = (((void*)pSimpleImage) + sizeof(SimpleImage));
    return pSimpleImage;
}

void
simple_image_write_to_disk(SimpleImage* pSimpleImage, const char* path)
{
    size_t size = 4 * pSimpleImage->Width * pSimpleImage->Height + sizeof(SimpleImage);
    file_write_bytes(path, (void*)pSimpleImage, size);
}

void
simple_image_destroy(SimpleImage* pSimpleImage)
{
    // NOTE(typedef): This thing huge block, ->Data included in root object
    memory_free(pSimpleImage);
}

/*

  #####################################
  #####################################
  SimpleSocket.c
  #####################################
  #####################################

*/

static i32 gSocketIsInDebugMode = 0;

u32
ip_as_integer(u8 ip3, u8 ip2, u8 ip1, u8 ip0)
{
    return ip3 << 24 | ip2 << 16 | ip1 << 8 | ip0;
}

void
ip_as_string(char str[], u32 ip)
{
    u8* ptr = (u8*)&ip;
    u8 byte3 = *(ptr+3);
    u8 byte2 = *(ptr+2);
    u8 byte1 = *(ptr+1);
    u8 byte0 = *(ptr);
    sprintf(str, "%d.%d.%d.%d", byte3, byte2, byte1, byte0);
}

void
ip_print(u32 address)
{
    char ips[32] = {};
    ip_as_string(ips, address);
    printf("ip=%s\n", ips);
}

char*
ip_to_string(u32 address)
{
    static char ips[32] = {};
    ip_as_string(ips, address);
    return (char*) ips;
}

char*
ip_to_user_string(u32 address)
{
    return ip_to_string(htonl(address));
}

#if defined(LINUX_PLATFORM)
u32
htonf(f32 f)
{
    u32 p;
    u32 sign;

    if (f < 0)
    {
	sign = 1;
	f = -f;
    }
    else
    {
	sign = 0;
    }

    p  = ((((u32)f) & 0x7fff) <<16) | (sign << 31); // whole part and sign
    p |= (u32) (((f - (i32)f) * 65536.0f)) & 0xffff; // fraction

    return p;
}

f32
ntohf(u32 p)
{
    f32 f = ((p >> 16) & 0x7fff); // whole part
    f += (p & 0xffff) / 65536.0f; // fraction

    if (((p >> 31) & 0x1) == 0x1)
    {
	f = -f; // sign bit set
    }

    return f;
}
#endif

void
socket_init()
{
#if defined(LINUX_PLATFORM)

#elif defined(WINDOWS_PLATFORM)
    WSADATA wsaData;
    i32 initializedResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (initializedResult != 0)
    {
	GERROR("WSAStartup failed with error: %d\n", initializedResult);
	vassert_break();
    }
#endif

}

Socket
socket_new(SocketSettings settings)
{
    if (settings.IpType != IpType_V4)
    {
	GERROR("IpV6 isnt supported!\n");
	vassert(0 && "Wrong ip type!");
    }

#if defined(LINUX_PLATFORM)
    i32 socketDs;
#elif defined(WINDOWS_PLATFORM)
    SOCKET socketDs;
#endif

    if (settings.Type == SocketType_UDP)
    {
	socketDs = socket(AF_INET, SOCK_DGRAM, settings.Protocol);
    }
    else if (settings.Type == SocketType_TCP)
    {
	socketDs = socket(AF_INET, SOCK_STREAM, settings.Protocol);
    }
    else
    {
	GERROR("Socket type raw isnt supported!\n");
	vassert(0 && "Wrong socket type!");
    }

    if (gSocketIsInDebugMode && settings.Type == SocketType_TCP)
    {
#if defined(LINUX_PLATFORM)
	i32 setOptionResult = setsockopt(socketDs, SOL_SOCKET, SO_REUSEADDR, &(i32){1}, sizeof(i32));
	setOptionResult = setsockopt(socketDs, SOL_SOCKET, SO_REUSEPORT, &(i32){1}, sizeof(i32));
#elif defined(WINDOW_PLATFORM)
	i32 setOptionResult = setsockopt(socketDs, SOL_SOCKET, SO_REUSEADDR, (void*) (&((i32) { 1 })), sizeof(i32));
	setOptionResult = setsockopt(socketDs, SOL_SOCKET, SO_REUSEPORT, (void*) (&(i32) { 1 }), sizeof(i32));
#endif
    }

    Socket socket = {
	.Descriptor = socketDs,
    };

    socket.Address = (struct sockaddr_in) {
	.sin_family = AF_INET,
	.sin_addr.s_addr = htonl(settings.Address.Ip),
	.sin_port = htons(settings.Address.Port),
    };

    socket.ServerAddress = (struct sockaddr_in) {
	.sin_family = AF_INET,
	.sin_addr.s_addr = htonl(settings.ServerAddress.Ip),
	.sin_port = htons(settings.ServerAddress.Port),
    };

    return socket;
}

Socket
socket_new_address(char* address, SocketType socketType)
{
    vguard_not_impl();

    const Socket invalidHandle = (Socket) {0};

    u32 ip;
    u16 port;
    if (!socket_parse_ip_port(address, &ip, &port))
    {
	return invalidHandle;
    }

    Socket socket = socket_new((SocketSettings) {
	    .Type = socketType,
	    .IpType = IpType_V4,
	    .Address = { .Ip=ip, .Port=port },
	});

    return socket;
}

i32
socket_is_invalid(Socket* pSocket)
{
    Socket invalidOne = (Socket) {0};
    if (memcmp(pSocket, &invalidOne, sizeof(Socket)) == 0)
    {
	return 1;
    }

    return 0;
}

i32
socket_is_valid(Socket* pSocket)
{
    return !socket_is_invalid(pSocket);
}


void
socket_make_async(Socket* pSocket)
{
    pSocket->IsAsync = 1;
#if defined(LINUX_PLATFORM)
    fcntl(pSocket->Descriptor, F_SETFL, O_NONBLOCK);
#elif defined(WINDOWS_PLATFORM)
    u_long flag = 1;
    ioctlsocket(pSocket->Descriptor, FIONBIO, &flag);
#endif
}

i32
socket_connect(Socket* pSocket)
{
    i32 result = connect(pSocket->Descriptor, (struct sockaddr*) &pSocket->ServerAddress, sizeof(struct sockaddr_in));
    if (result < 0)
    {
	return 0;
    }

    return 1;
}

i32
socket_connect_to(Socket* pSocket, u32 ip, u16 port)
{
    struct sockaddr_in connectTo = (struct sockaddr_in) {
	.sin_family = AF_INET,
	.sin_port = htons(port),
	.sin_addr.s_addr = htonl(ip)
    };

    i32 result = connect(pSocket->Descriptor, (struct sockaddr*) &connectTo, sizeof(struct sockaddr_in));
    if (result < 0)
	return 0;

    return 1;
}

i32
socket_bind(Socket* pSocket)
{
    i32 result = bind(pSocket->Descriptor, (struct sockaddr*) &pSocket->Address, sizeof(struct sockaddr_in));
    if (result < 0)
	return 0;
    return 1;
}

Socket
socket_accept(Socket* pSocket)
{
    struct sockaddr_in addr = {};
    i32 sockAddrLen = sizeof(addr);
#if defined(LINUX_PLATFORM)
    i32 sock = accept(pSocket->Descriptor, (struct sockaddr*) &addr, (socklen_t*) &sockAddrLen);
#elif defined(WINDOWS_PLATFORM)
    i32 sock = accept(pSocket->Descriptor, (struct sockaddr*)&addr, &sockAddrLen);
#endif

    Socket socket = {
	.Descriptor = sock,
	.ServerAddress = addr
    };

    return socket;
}

i32
socket_listen(Socket* pSocket, i32 clientsCount)
{
    i32 result = listen(pSocket->Descriptor, clientsCount);
    if (result < 0)
	return 0;
    return 1;
}

void
socket_close(Socket* pSocket)
{
#if defined(LINUX_PLATFORM)
    close(pSocket->Descriptor);
#elif defined(WINDOWS_PLATFORM)
    closesocket(pSocket->Descriptor);
#endif
}

void
socket_deinit()
{
#if defined(WINDOWS_PLATFORM)
    WSACleanup();
#endif
}

i32
socket_send_ext(i32 descriptor, void* data, size_t size, i32 flags)
{
    i32 sendedBytesCount, total = 0;

    while (total < size)
    {
	i32 byteToSend = size - total;
	sendedBytesCount = send(descriptor, data + total, byteToSend, flags);

	if (sendedBytesCount == -1)
	{
	    return 0;
	}

	total += sendedBytesCount;
    }

    return 1;
}

i32
socket_send(Socket* pSocket, void* data, size_t size)
{
    return socket_send_ext(pSocket->Descriptor, data, size, 0);
}

i32
socket_recv_ext(i32 descriptor, void* data, size_t size, i32 flags)
{
    i32 recvBytesCount, total = 0;
    while (total < size)
    {
	i32 byteToRecv = size - total;
	recvBytesCount = recv(descriptor, data + total, byteToRecv, flags);

	if (recvBytesCount == 0)
	    return 0;
	else if (recvBytesCount == -1)
	    return -1;

	total += recvBytesCount;
    }

    return total;
}

i32
socket_recv(Socket* pSocket, void* data, size_t size)
{
    return socket_recv_ext(pSocket->Descriptor, data, size, 0);
}

i32
socket_peek(Socket* pSocket, void* data, size_t size)
{
    return socket_recv_ext(pSocket->Descriptor, data, size, MSG_PEEK);
}

i32
socket_send_to_ext(i32 descriptor, void* data, size_t size, i32 flags, struct sockaddr_in* pAddress)
{
    i32 sendedBytesCount, total = 0;
    while (total < size)
    {
	i32 byteToSend = size - total;
	sendedBytesCount = sendto(descriptor, data + total, byteToSend, flags, (struct sockaddr*) pAddress, sizeof(struct sockaddr_in));

	if (sendedBytesCount == -1)
	{
	    return 0;
	}

	total += sendedBytesCount;
    }

    return 1;
}

i32
socket_recv_from_ext(i32 descriptor, void* data, size_t size, i32 flags, struct sockaddr_in* pAddress)
{
    i32 recvBytesCount, total = 0;
    u32 addrSize = sizeof(struct sockaddr_in);
    while (total < size)
    {
	i32 byteToRecv = size - total;
	recvBytesCount = recvfrom(descriptor, data + total, byteToRecv, flags, (struct sockaddr*) pAddress, &addrSize);

	if (recvBytesCount == 0)
	    return 0;
	else if (recvBytesCount == -1)
	    return -1;

	total += recvBytesCount;
    }

    return total;
}

i32
socket_send_to(Socket* pSocket, void* data, size_t size)
{
    return socket_send_to_ext(pSocket->Descriptor, data, size, 0, &pSocket->ServerAddress);
}

i32
socket_recv_from(Socket* pSocket, void* data, size_t size)
{
    return socket_recv_from_ext(pSocket->Descriptor, data, size, 0, &pSocket->ServerAddress);
}

i32
socket_peek_from(Socket* pSocket, void* data, size_t size)
{
    return socket_recv_from_ext(pSocket->Descriptor, data, size, MSG_PEEK, &pSocket->ServerAddress);
}

u32
socket_parse_ipv4(char* str, size_t length)
{
    i32 ip0, ip1, ip2, ip3;
    i32 ind = 0;
    char* sb = NULL;

    if (string_compare(str, "l") || string_compare(str, "localhost"))
    {
	return ip_as_integer(127, 0, 0, 1);
    }

    Arena* pArena = arena_create(KB(1));
    memory_set_arena(pArena);

#define ParseIpFor(ipi)						\
    if (string_is_integer(sb, string_builder_count(sb)))	\
    {								\
	ipi = string_to_i32(sb);				\
	++ind;							\
    }								\
								\
    string_builder_clear(sb);

    for (i32 i = 0; i < length; ++i)
    {
	char c = str[i];

	switch (c)
	{

	case '0': case '1': case '2': case '3': case '4':
	case '5': case '6': case '7': case '8': case '9':
	{
	    string_builder_appendc(sb, c);
	    break;
	}

	case '.':
	{
	    switch (ind)
	    {
	    case 0:
	    {
		ParseIpFor(ip0);
		break;
	    }

	    case 1:
	    {
		ParseIpFor(ip1);
		break;
	    }

	    case 2:
	    {
		ParseIpFor(ip2);
		break;
	    }

	    }

	    break;
	}

	}
    }

    if (ind == 3)
    {
	ParseIpFor(ip3);
    }

    memory_set_arena(NULL);
    arena_destroy(pArena);

#define IpNotValid(ip) ({(ip > 255 || ip < 0);})

    u32 ipAddr;
    if (IpNotValid(ip0) || IpNotValid(ip1) || IpNotValid(ip2) || IpNotValid(ip3))
    {
	ipAddr = 0;
    }
    else
    {
	ipAddr = ip_as_integer(ip0, ip1, ip2, ip3);
    }

#undef ParseIpFor
#undef IpNotValid

    return ipAddr;
}

u16
socket_parse_port(char* str, size_t length)
{
    i32 isInt = string_is_integer(str, length);
    if (!isInt)
	return 0;

#define PortNotValid(port) ({(port < 0 || port > 65535);})

    i32 port = string_to_i32(str);
    if (PortNotValid(port))
	return 0;

    return (u16) port;
}

i32
socket_parse_ip_port(char* address, u32* pIp, u16* pPort)
{
    i32 result = 0;

    char** splited = string_split(address, ':');
    if (array_count(splited) != 2)
    {
	GERROR("Socket address (%s) is not correct!\n", address);
	goto SocketParseIpPortEnd;
    }

    char* ipStr   = splited[0];
    char* portStr = splited[1];
    u32 addr = socket_parse_ipv4((char*)ipStr, string_length(ipStr));
    u16 port = socket_parse_port((char*)portStr, string_length(portStr));

    if (addr == 0 || port == 0)
    {
	GERROR("Address (%s) or port (%d) isnt correct!\n", ip_to_string(addr), port);
	goto SocketParseIpPortEnd;
    }

    *pIp = addr;
    *pPort = port;
    result = 1;

SocketParseIpPortEnd:
    array_foreach(splited, memory_free(item));
    array_free(splited);

    return result;
}

struct sockaddr_in
socket_address_v4(u32 ip, u16 port)
{
    struct sockaddr_in address = {
	.sin_family = AF_INET,
	.sin_port = htons(port),
	.sin_addr.s_addr = htonl(ip)
    };
    return address;
}

i32
socket_compare_address(Socket* a, Socket* b)
{
    i32 isEqual = (memcmp(&a->Address, &b->Address, sizeof(struct sockaddr_in)) == 0);
    return isEqual;
}

i32
socket_compare_raw_address(struct sockaddr_in* a, struct sockaddr_in* b)
{
    if (a->sin_port == b->sin_port &&
	a->sin_addr.s_addr == b->sin_addr.s_addr)
    {
	return 1;
    }

    return 0;
}

i32
socket_compare_server_address(Socket* a, Socket* b)
{
    i32 isEqual = (memcmp(&a->ServerAddress, &b->ServerAddress, sizeof(struct sockaddr_in)) == 0);
    return isEqual;
}

void
socket_set_debug_mode()
{
    gSocketIsInDebugMode = 1;
}

void
socket_set_release_mode()
{
    gSocketIsInDebugMode = 0;
}


// End of SimpleSocket.c


void
pack_example()
{
#pragma pack(push, 1)
    struct B
    {
	char c;
	i32 d;
    };
#pragma pack(pop)

    struct B a = {};
    GINFO("Size: %d\n", sizeof(a));
    vassert_break();
}

#endif //SSL_IMPLEMENTATION

#endif //SIMPLE_STANDARD_LIBRARY_H
