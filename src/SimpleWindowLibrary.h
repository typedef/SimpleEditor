#ifndef SIMPLE_WINDOW_LIBRARY_H
#define SIMPLE_WINDOW_LIBRARY_H

/*
  DOCS:
  Xlib and Win64 api abstraction for window/input/software rendering behaviour

  For implementation:
  #define SWL_IMPLEMENTATION

  Use swl_ prefix for function.
  Swl for type


  extern void swl_init();

  DOCS: Create SwlWindow with concrete set
  SwlWindow swl_window_create(SwlWindowSettings set);

  DOCS: Set window above other, work on linux, windows wm is stupid.
  void swl_window_set_above(SwlWindow* pWindow);

  DOCS: Window occupy all space except 'taskbar'.
  void swl_window_set_maximized(SwlWindow* pWindow);

  DOCS: Window occupy all space.
  void swl_window_set_fullscreen(SwlWindow* pWindow);

  DOCS: Remove header from window.
  void swl_window_set_wo_header(SwlWindow* pWindow);

  DOCS: Set background color using r,g,b as value from 0 to 255.
  void swl_window_set_background(SwlWindow* pWindow, swl_u8 r255, swl_u8 g255, swl_u8 b255);


*/

//DOCS: Default flags, enabled by default
#if !defined(SWL_DEBUG)
#define SWL_DEBUG 1
#endif

#ifndef swl_i32
#define swl_i32 int
#endif

#ifndef swl_i64
#define swl_i64 long long int
#endif

#ifndef swl_u8
#define swl_u8 unsigned char
#endif

#ifndef swl_u32
#define swl_u32 unsigned int
#endif

#ifndef swl_u64
#define swl_u64 unsigned long
#endif

#ifndef swl_f32
#define swl_f32 float
#endif

#ifndef swl_f64
#define swl_f64 double
#endif

#ifndef swl_size
#define swl_size swl_u64
#endif

#define swl_assert(cond) ({ if (!cond){ int* i=0; *i = 1; } })


/*
  ###################################
  ###################################
	   SWL_DECLARATION
  ###################################
  ###################################
*/

//////                               //
//               Input               //
//                               //////

typedef enum SwlKey
{
    SwlKey_None = -1,

    // DOCS: Numbers
    SwlKey_0 = 0,
    SwlKey_1 = 1,
    SwlKey_2 = 2,
    SwlKey_3 = 3,
    SwlKey_4 = 4,
    SwlKey_5 = 5,
    SwlKey_6 = 6,
    SwlKey_7 = 7,
    SwlKey_8 = 8,
    SwlKey_9 = 9,

    // DOCS: Numpad Numbers
    SwlKey_NP_0 = 10,
    SwlKey_NP_1 = 11,
    SwlKey_NP_2 = 12,
    SwlKey_NP_3 = 13,
    SwlKey_NP_4 = 14,
    SwlKey_NP_5 = 15,
    SwlKey_NP_6 = 16,
    SwlKey_NP_7 = 17,
    SwlKey_NP_8 = 18,
    SwlKey_NP_9 = 19,

    // DOCS: Numpad Keys
    SwlKey_NP_Lock = 20,
    SwlKey_NP_Div = 21,
    SwlKey_NP_Mul = 22,
    SwlKey_NP_Min = 23,
    SwlKey_NP_Plus = 24,
    SwlKey_NP_Enter = 25,
    SwlKey_NP_Comma = 26,

    // DOCS: Upper right panel
    SwlKey_PrintScreen = 27,
    SwlKey_ScrollLock = 28,
    SwlKey_PauseBreak = 29,

    // DOCS: Mid right panel
    SwlKey_Insert = 30,
    SwlKey_Delete = 31,
    SwlKey_Home = 32,
    SwlKey_End = 33,
    SwlKey_PageUp = 34,
    SwlKey_PageDown = 35,
    SwlKey_CapsLock = 36,

    // DOCS: Lower right panel
    SwlKey_Up = 37,
    SwlKey_Left = 38,
    SwlKey_Down = 39,
    SwlKey_Right = 40,

    // DOCS: Characters

    SwlKey_Q = 41,
    SwlKey_W = 42,
    SwlKey_E = 43,
    SwlKey_R = 44,
    SwlKey_T = 45,
    SwlKey_Y = 46,
    SwlKey_U = 47,
    SwlKey_I = 48,
    SwlKey_O = 49,
    SwlKey_P = 50,

    SwlKey_A = 51,
    SwlKey_S = 52,
    SwlKey_D = 53,
    SwlKey_F = 54,
    SwlKey_G = 55,
    SwlKey_H = 56,
    SwlKey_J = 57,
    SwlKey_K = 58,
    SwlKey_L = 59,

    SwlKey_Z = 60,
    SwlKey_X = 61,
    SwlKey_C = 62,
    SwlKey_V = 63,
    SwlKey_B = 64,
    SwlKey_N = 65,
    SwlKey_M = 66,

    // DOCS: Functional
    SwlKey_Apostrophe = 67,  /* ' */
    SwlKey_Minus = 68,
    SwlKey_Asign = 69, // +/=
    SwlKey_Tab = 70,
    SwlKey_Shift = 71,
    SwlKey_Ctrl = 72,
    SwlKey_Alt = 73,
    SwlKey_Super = 74,
    SwlKey_Space = 75,

    SwlKey_Comma = 76,
    SwlKey_Dot = 77,
    SwlKey_Slash = 78, /*/*/

    SwlKey_Shift_Right = 79,
    SwlKey_Alt_Right = 80,
    SwlKey_Ctrl_Right = 81,
    SwlKey_Super_Right = 82,
    SwlKey_Command_Right = 83,

    SwlKey_Escape = 84,

    // DOCS: Press F
    SwlKey_F1  = 85,
    SwlKey_F2  = 86,
    SwlKey_F3  = 87,
    SwlKey_F4  = 88,
    SwlKey_F5  = 89,
    SwlKey_F6  = 90,
    SwlKey_F7  = 91,
    SwlKey_F8  = 92,
    SwlKey_F9  = 93,
    SwlKey_F10 = 94,
    SwlKey_F11 = 95,
    SwlKey_F12 = 96,

    // DOCS: Extra
    SwlKey_Bracket_Left = 97,  /* [ */
    SwlKey_Bracket_Right = 98,  /* ] */
    SwlKey_Semicolon = 99,  /* ; */
    SwlKey_QuotMark = 100,  /* ' */
    SwlKey_Backslash = 101,  /* \ */
    SwlKey_Enter = 102,

    SwlKey_F13 = 103,
    SwlKey_F14 = 104,
    SwlKey_F15 = 105,
    SwlKey_F16 = 106,
    SwlKey_F17 = 107,
    SwlKey_F18 = 108,
    SwlKey_F19 = 119,
    SwlKey_F20 = 110,
    SwlKey_F21 = 111,
    SwlKey_F22 = 112,
    SwlKey_F23 = 113,
    SwlKey_F24 = 114,
    SwlKey_F25 = 115,

    SwlKey_Backspace = 116,

    SwlKey_Count,
} SwlKey;

typedef enum SwlMouseKey
{
    SwlMouseKey_Left = 0,
    SwlMouseKey_Right = 1,
    SwlMouseKey_Middle = 2,

    SwlMouseKey_Ext_0,
    SwlMouseKey_Ext_1,
    SwlMouseKey_Ext_2,
    SwlMouseKey_Ext_3,
    SwlMouseKey_Ext_4,
    SwlMouseKey_Ext_5,
    SwlMouseKey_Ext_6,
    SwlMouseKey_Ext_7,
    SwlMouseKey_Ext_8,
    SwlMouseKey_Ext_9,

    SwlMouseKey_Count,
} SwlMouseKey;

typedef enum SwlMod
{
    SwlMod_None     = -1,
    SwlMod_Shift    = 1 << 1,
    SwlMod_Ctrl     = 1 << 2,
    SwlMod_Alt      = 1 << 3,
    SwlMod_Super    = 1 << 4,
    SwlMod_CapsLock = 1 << 5,
    SwlMod_NumLock  = 1 << 6,
} SwlMod;

typedef enum SwlCursorMode
{
    SwlCursorMode_Normal = 0,
    SwlCursorMode_Hidden = 1,
    SwlCursorMode_Locked = 2,
    SwlCursorMode_Count
} SwlCursorMode;

typedef enum SwlAction
{
    SwlAction_Press = 0,
    SwlAction_Release = 1,
    SwlAction_Repeat = 2,
    SwlAction_Count = 2,
} SwlAction;


//////                               //
//              Window               //
//                               //////

typedef struct SwlSize
{
    swl_i32 Width;
    swl_i32 Height;

    swl_i32 MinWidth;
    swl_i32 MinHeight;

    swl_i32 MaxWidth;
    swl_i32 MaxHeight;
} SwlSize;

typedef struct SwlPosition
{
    swl_i32 X;
    swl_i32 Y;
} SwlPosition;

typedef struct SwlWindowSettings
{
    const char* pName;
    SwlSize Size;
    SwlPosition Position;
} SwlWindowSettings;


typedef enum SwlEventType
{
    SwlEventType_None = 0,

    SwlEventType_KeyPress,
    SwlEventType_KeyRelease,
    SwlEventType_MousePress,
    SwlEventType_MouseRelease,

    SwlEventType_Count,
} SwlEventType;

typedef struct SwlEvent
{
    swl_i32 IsHandled;
    SwlEventType Type;
} SwlEvent;

typedef struct SwlKeyPressEvent
{
    SwlEvent Base;
    SwlKey Key;
} SwlKeyPressEvent;

typedef struct SwlKeyReleaseEvent
{
} SwlKeyReleaseEvent;

typedef struct SwlMousePressEvent
{
    SwlEvent Base;
    SwlMouseKey Key;
} SwlMousePressEvent;

typedef struct SwlMouseReleaseEvent
{
} SwlMouseReleaseEvent;

typedef struct SwlCall
{
    void (*OnEvent)(SwlEvent*);

    // NOTE: ?
    /* void (*OnKeyPress)(SwlKeyPressEvent* pKeyPressEvent); */
    /* void (*OnKeyRelease)(SwlKeyReleaseEvent* pKeyReleaseEvent); */
    /* void (*OnMousePress)(SwlMousePressEvent* pMousePressEvent); */
    /* void (*OnMouseRelease)(SwlMouseReleaseEvent* pMouseReleaseEvent); */

} SwlCall;

typedef struct SwlWindow
{
    const char* pName;
    SwlSize Size;
    swl_i32 IsProcessLoopRunning;

    SwlCall Call;

    void* pBackend;
} SwlWindow;

typedef struct SwlInit
{
    swl_i32 IsInitialized;
    void* (*MemoryAllocate)(swl_size size);
    void (*MemoryFree)(void* pData);
} SwlInit;

void swl_init(SwlInit swlInit);

SwlWindow swl_window_create(SwlWindowSettings set);
void swl_window_destroy(SwlWindow* pWindow);
void swl_update(SwlWindow* pWindow);
void swl_window_set_event_call(SwlWindow* pWindow, void (*onEvent)(SwlEvent*));

/*
  DOCS: Set window above other, work on linux, windows wm is stupid
*/
void swl_window_set_above(SwlWindow* pWindow);
void swl_window_set_maximized(SwlWindow* pWindow);
void swl_window_set_fullscreen(SwlWindow* pWindow);
/*
  DOCS: Remove header from window
*/
void swl_window_set_wo_header(SwlWindow* pWindow);
/*
  DOCS: Set background color using r,g,b as value from 0 to 255.
*/
void swl_window_set_background(SwlWindow* pWindow, swl_u8 r255, swl_u8 g255, swl_u8 b255);


/*
  ###################################
  ###################################
	   SWL_IMPLEMENTATION
  ###################################
  ###################################
*/

#if defined(SWL_IMPLEMENTATION)

// DOCS: Declare it only inside SWL_IMPLEMENTATION
#if SWL_X11_BACKEND == 1

#include <X11/Xlib.h>
#include <X11/Xutil.h> // for XSizeHints

typedef struct SwlX11Backend
{
    Display* pDisplay;
    Window Window;
    GC GraphicsContext;

    // DOCS: Atoms
    Atom AtomFocused;
    Atom AtomMaxH;
    Atom AtomMaxV;
    Atom AtomHidden;
    Atom AtomWindowManager;

} SwlX11Backend;

#endif // SWL_X11_BACKEND == 1

#define SWL_OTHER_BACKEND 0

static SwlInit gSwlInit = {};

void
swl_init(SwlInit swlInit)
{
    swl_i32 backendSum = SWL_X11_BACKEND + SWL_OTHER_BACKEND;
    if (backendSum == 0)
    {
	printf("No back-end defined!\n");
	swl_assert(0);
    }
    else if (backendSum != 1)
    {
	printf("Defined more then one backend!\n");
	swl_assert(0);
    }

    swl_assert(swlInit.MemoryAllocate);
    swl_assert(swlInit.MemoryFree);

    gSwlInit.IsInitialized = 1;
    gSwlInit.MemoryAllocate = swlInit.MemoryAllocate;
    gSwlInit.MemoryFree = swlInit.MemoryFree;
}

#if SWL_DEBUG == 1
#define swl_init_was_called_check() ({ if (gSwlInit.IsInitialized != 1){swl_assert(0);}})
#else
#define swl_init_was_called_check()
#endif

#if SWL_X11_BACKEND == 1

SwlWindow
swl_window_create(SwlWindowSettings set)
{
    swl_init_was_called_check();

    SwlWindow swlWindow = {};

    const char* pDiplayName = "Display 0";
    Display* pDisplay = XOpenDisplay(NULL);
    if (pDisplay == NULL)
    {
	printf("Display creation error!\n");
	return (SwlWindow) {};
    }

    swl_i32 defaultScreen = DefaultScreen(pDisplay);
    Window windowRoot = RootWindow(pDisplay, defaultScreen);
    GC graphicsContext = DefaultGC(pDisplay, defaultScreen);

    // todo: extend in future
    swl_u64 attribute_mask = CWEventMask | CWBackPixel | CWBorderPixel;
    swl_u64 event_mask = ExposureMask
	| ButtonPressMask | ButtonReleaseMask
	| KeyPressMask | KeyReleaseMask
	| FocusChangeMask
	| EnterWindowMask | LeaveWindowMask
	| VisibilityChangeMask /*It just maximized*/
	| StructureNotifyMask
	/* | ResizeRedirectMask  do *NOT* handle it*/
	| StructureNotifyMask | PropertyChangeMask | PointerMotionMask /* for minify, maximize-hor, max-ver, */
	;

    XSetWindowAttributes attributes = {
	.event_mask = event_mask,
	.border_pixel = BlackPixel(pDisplay, defaultScreen),
	.background_pixel = WhitePixel(pDisplay, defaultScreen),
    };

    Visual* pVisual = CopyFromParent;
    swl_i32 someWeirdFlagFromX = InputOutput;
    swl_i32 borderWidth = 120;

    swl_i32 x = set.Position.X;
    if (x < 0)
	x = 150;
    swl_i32 y = set.Position.Y;
    if (y < 0)
	y = 150;
    swl_i32 w = set.Size.Width;
    swl_i32 h = set.Size.Height;

    Window window = XCreateWindow(
	pDisplay, windowRoot, x, y, w, h,
	borderWidth, CopyFromParent, someWeirdFlagFromX, pVisual,
	attribute_mask, &attributes);

    XSizeHints sizeHints = {
	.x = x,
	.y = y,
	.width = w,
	.height = h,
	.base_width = w,
	.base_height = h,
	.flags = USPosition | USSize | PBaseSize,
    };

    if (set.Size.MinWidth > 0 && set.Size.MinHeight > 0)
    {
	sizeHints.min_width  = set.Size.MinWidth;
	sizeHints.min_height = set.Size.MinHeight;
	sizeHints.flags |= PMinSize;
    }
    if (set.Size.MaxWidth > 0 && set.Size.MaxHeight > 0)
    {
	sizeHints.max_width  = set.Size.MaxWidth;
	sizeHints.max_height = set.Size.MaxHeight;
	sizeHints.flags |= PMaxSize;
    }

    XSetWMNormalHints(pDisplay, window, &sizeHints);

    char* pWindowName = (char*) set.pName;
    char* pAppClass = "example_class";
    XClassHint xClassHints = {
	.res_class = pAppClass,
	.res_name = pWindowName,
    };
    XStoreName(pDisplay, window, pWindowName);
    XSetClassHint(pDisplay, window, &xClassHints);

    XWMHints windowManagerHints = {
	.flags = InputHint | StateHint,
	.initial_state = NormalState,
	.input = 1,
    };
    XSetWMHints(pDisplay, window, &windowManagerHints);

    // note: Diplay window
    XMapRaised(pDisplay, window);
    XFlush(pDisplay);

    // gSwlInit.MemoryAllocate = swlInit.MemoryAllocate;
    // gSwlInit.FreeAllocate = swlInit.FreeAllocate;

    // DOCS: Setup backend
    SwlX11Backend back = {
	.pDisplay = pDisplay,
	.Window = window,
	.GraphicsContext = graphicsContext,

	// Atoms
	.AtomFocused = XInternAtom(pDisplay, "_NET_WM_STATE_FOCUSED", 1),
	.AtomMaxH = XInternAtom(pDisplay, "_NET_WM_STATE_MAXIMIZED_HORZ", 1),
	.AtomMaxV = XInternAtom(pDisplay, "_NET_WM_STATE_MAXIMIZED_VERT", 1),
	.AtomHidden = XInternAtom(pDisplay, "_NET_WM_STATE_HIDDEN", 1),
	.AtomWindowManager = XInternAtom(pDisplay, "_NET_WM_STATE", 1),
    };

    swlWindow.pBackend = gSwlInit.MemoryAllocate(sizeof(SwlX11Backend));
    *((SwlX11Backend*)swlWindow.pBackend) = back;
    //memcpy(swlWindow.pBackend, &back, sizeof(SwlX11Backend));

    return swlWindow;
}

void
swl_window_destroy(SwlWindow* pWindow)
{
    SwlX11Backend* pX11Backend = (SwlX11Backend*) pWindow->pBackend;
    XCloseDisplay(pX11Backend->pDisplay);
}

SwlMouseKey
_swl_x_mouse_to_swl(swl_u32 mouseBtn)
{
    static swl_u32 xToSwl[] = {
	[1] = SwlMouseKey_Left,
	[2] = SwlMouseKey_Middle,
	[3] = SwlMouseKey_Right,
	[6] = SwlMouseKey_Ext_0,
	[7] = SwlMouseKey_Ext_1,
	[8] = SwlMouseKey_Ext_2,
	[9] = SwlMouseKey_Ext_3,
	[10] = SwlMouseKey_Ext_4,
	[11] = SwlMouseKey_Ext_5,
	[12] = SwlMouseKey_Ext_6,
	[13] = SwlMouseKey_Ext_7,
	[14] = SwlMouseKey_Ext_8,
	[15] = SwlMouseKey_Ext_9,
    };

    SwlMouseKey swlMouseKey = xToSwl[mouseBtn];
    return swlMouseKey;
}

void
swl_update(SwlWindow* pWindow)
{
    SwlX11Backend* pBackend = (SwlX11Backend*) pWindow->pBackend;

    Display* pDisplay = pBackend->pDisplay;
    Window window = pBackend->Window;
    // not need
    // GC GraphicsContext = pBackend->GraphicsContext;

    XEvent event, pevent;

    if (pWindow->Call.OnEvent == NULL)
    {
	return;
    }

    while (XQLength(pDisplay))
    {
	XNextEvent(pDisplay, &event);

	switch (event.type)
	{

	case KeyPress:
	{
	    printf("Key: %d\n", event.xkey.keycode);
	    // 9 - SwlKey_Escape;
	    int counter = 0;
	    KeySym* pKeys = XGetKeyboardMapping(pDisplay, 8, 254, &counter);
	    printf("%d\n", pKeys[0]);
	    swl_u32 xKey = event.xkey.keycode;

	    static swl_u32 xToSwl[135] = {
		[9] = SwlKey_Escape,

		[10] = SwlKey_1,
		[11] = SwlKey_2,
		[12] = SwlKey_3,
		[13] = SwlKey_4,
		[14] = SwlKey_5,
		[15] = SwlKey_6,
		[16] = SwlKey_7,
		[17] = SwlKey_8,
		[18] = SwlKey_9,
		[19] = SwlKey_0,

		[20] = SwlKey_Minus,
		[21] = SwlKey_Asign,
		[22] = SwlKey_Backspace,
		[23] = SwlKey_Tab,


		[24] = SwlKey_Q,
		[25] = SwlKey_W,
		[26] = SwlKey_E,
		[27] = SwlKey_R,
		[28] = SwlKey_T,
		[29] = SwlKey_Y,
		[30] = SwlKey_U,
		[31] = SwlKey_I,
		[32] = SwlKey_O,
		[33] = SwlKey_P,

		[37] = SwlKey_Ctrl,

		[38] = SwlKey_A,
		[39] = SwlKey_S,
		[40] = SwlKey_D,
		[41] = SwlKey_F,
		[42] = SwlKey_G,
		[43] = SwlKey_H,
		[44] = SwlKey_J,
		[45] = SwlKey_K,
		[46] = SwlKey_L,

		[49] = SwlKey_Apostrophe,
		[50] = SwlKey_Shift,

		[52] = SwlKey_Z,
		[53] = SwlKey_X,
		[54] = SwlKey_C,
		[55] = SwlKey_V,
		[56] = SwlKey_B,
		[57] = SwlKey_N,
		[58] = SwlKey_M,

		[62] = SwlKey_Shift_Right,

		[64] = SwlKey_Alt,
		[65] = SwlKey_Space,
		[66] = SwlKey_CapsLock,

		[105] = SwlKey_Ctrl_Right,

		[108] = SwlKey_Alt_Right,


		[111] = SwlKey_Up,
		[113] = SwlKey_Left,
		[114] = SwlKey_Right,
		[116] = SwlKey_Down,


		[133] = SwlKey_Super,
		[134] = SwlKey_Super_Right,
	    };

	    SwlKey key = 0;

	    swl_size arrSize = sizeof(xToSwl) / sizeof(xToSwl[0]);
	    if (event.xkey.keycode < arrSize)
	    {
		key = xToSwl[xKey];
	    }

	    SwlKeyPressEvent keyEvent = {
		.Base = {
		    .Type = SwlEventType_KeyPress
		},
		.Key = key,
	    };

	    pWindow->Call.OnEvent((SwlEvent*)&keyEvent);

	    break;
	}

	case KeyRelease:
	{
	    break;
	}

	case ButtonPress:
	{
	    // 4 - forward, 5 - backward
	    swl_u32 mouseBtn = event.xbutton.button;
	    if (mouseBtn == 4 || mouseBtn == 5)
	    {//todo:
		continue;
	    }

	    SwlMouseKey swlMouseKey = _swl_x_mouse_to_swl(mouseBtn);

	    SwlMousePressEvent mouseEvent = {
		.Base = {
		    .Type = SwlEventType_MousePress
		},
		.Key = swlMouseKey,
	    };

	    pWindow->Call.OnEvent((SwlEvent*)&mouseEvent);
	    break;
	}

	case ButtonRelease:
	{
	    swl_u32 mouseBtn = event.xbutton.button;
	    if (mouseBtn == 4 || mouseBtn == 5)
	    {
		//todo:
		continue;
	    }

	    SwlMouseKey swlMouseKey = _swl_x_mouse_to_swl(mouseBtn);

	    SwlMousePressEvent mouseEvent = {
		.Base = {
		    .Type = SwlEventType_MouseRelease
		},
		.Key = swlMouseKey,
	    };

	    pWindow->Call.OnEvent(&mouseEvent.Base);
	    break;
	}

	case FocusIn:
	{
	    // we can use this as un-hidden event
	    //printf("Window focused!\n");
	    break;
	}

	case FocusOut:
	{
	    //printf("Window unfocused!\n");
	    break;
	}

	case EnterNotify:
	{
	    //printf("Enter window\n");
	    break;
	}

	case LeaveNotify:
	{
	    //printf("Leave window\n");
	    break;
	}

	case VisibilityNotify:
	{
	    break;
	}

	}

	// note: process event
	if (event.type == Expose)
	{
	}
	else if (event.type == KeyPress)
	{
	    // printf("KeyPress: %d\n", event.xkey.keycode);

	    if (event.xkey.keycode == 9)
	    {
		pWindow->IsProcessLoopRunning = 0;
		continue;
	    }
	}
	else if (event.type == KeyRelease)
	{
	    // Release_A Press_A Release_A Press_A
	    // printf("KeyRelease: %d\n", event.xkey.keycode);

	    XEvent nevent;
	    XPeekEvent(pDisplay, &nevent);

	    if (nevent.type == KeyPress
		&& nevent.xkey.time == event.xkey.time
		&& nevent.xkey.keycode == event.xkey.keycode)
	    {
		// printf("Key repeat %d ms!\n", event.xkey.time);
	    }

	}

	if (event.type == ButtonPress)
	{
	    // no extra buttons
	    // printf("ButtonPress: %d\n", event.xbutton.button);
	}
	else if (event.type == ButtonRelease)
	{
	    // no extra buttons
	    // printf("ButtonRelease: %d\n", event.xbutton.button);
	}

	if (event.type == FocusIn)
	{
	    // we can use this as un-hidden event
	    //printf("Window focused!\n");
	}
	else if (event.type == FocusOut)
	{
	    //printf("Window unfocused!\n");
	}

	if (event.type == EnterNotify)
	{
	    //printf("Enter window\n");
	}
	else if (event.type == LeaveNotify)
	{
	    //printf("Leave window\n");
	}

	if (event.type == VisibilityNotify)
	{
	    //printf("Visibility changed!\n");
	}

	if (event.type == PropertyNotify)
	{
	    XPropertyEvent ptyEvent = event.xproperty;
	    Atom atom = ptyEvent.atom;

	    if (atom == pBackend->AtomWindowManager)
	    {
		char* pAtomName = XGetAtomName(pDisplay, atom);
		// printf("Atom: %s [%d]\n", pAtomName, fi);

		swl_u64 after = 1;
		do {
		    // printf("XA_ATOM: %s\n", XGetAtomName(pDisplay, 4));
		    Atom type;
		    swl_i32 format;
		    swl_u64 length;
		    swl_u8* dp;
		    Status status = XGetWindowProperty(
			pDisplay, window, atom, 0, after, 0,
			4, &type, &format, &length, &after, &dp);

		    if (status == Success
			&& type == 4 /* XA_ATOM */
			&& dp
			&& format == 32 /*atom for type of the property*/)
		    {
			swl_i32 maxH = 0, maxV = 0;
			for (int i = 0; i < length; i++)
			{
			    Atom prop = ((Atom*)dp)[i];

			    if (prop == pBackend->AtomFocused)
			    {
				//printf("Focused by atom!\n");
			    }

			    if (prop == pBackend->AtomMaxH)
			    {
				//printf("MaxH!\n");
				maxH = 1;
			    }

			    if (prop == pBackend->AtomMaxV)
			    {
				//printf("MaxV!\n");
				maxV = 1;
			    }

			    if (prop == pBackend->AtomHidden)
			    {
				//printf("Hidden!\n");
			    }

			    if (maxH && maxV)
			    {
				//printf("Maximized\n");
			    }

			}
		    }
		    else {
			//printf("False!\n");
		    }

		}
		while (after);

	    }

	}


	if (event.type == MotionNotify)
	{
	    //printf("MotionNotify %d\n", fi);
	}

	if (event.type == ConfigureNotify)
	{
	    //printf("Resize good!\n");
	    XConfigureEvent conf = event.xconfigure;
	    //printf("p(%d, %d) s(%d, %d)\n", conf.x, conf.y, conf.width, conf.height);
	}

	if (event.type == ResizeRequest)
	{
	    //printf("Resize!\n");

	    XResizeRequestEvent resizeRequest = event.xresizerequest;
	    int type;
	    unsigned long serial;	/* # of last request processed by server */
	    Bool send_event;	/* true if this came from a SendEvent request */
	    Display *display;	/* Display the event was read from */
	    Window window;
	    int width, height;


	    /* Display* display  */
	    /* Window	/\* w *\/, */
	    /* int			/\* x *\/, */
	    /* int			/\* y *\/, */
	    /* unsigned int	/\* width *\/, */
	    /* unsigned int	/\* height *\/ */

	    long someVal = 0;
	    XSizeHints xSizeHints;
	    XGetWMNormalHints(resizeRequest.display,
			      resizeRequest.window,
			      &xSizeHints,
			      &someVal);

	    XMoveResizeWindow(resizeRequest.display, resizeRequest.window, xSizeHints.x, xSizeHints.y, resizeRequest.width, resizeRequest.height);
	    XFlush(pDisplay);
	}

	pevent = event;
    }

    XFlush(pDisplay);

}

void
swl_window_set_event_call(SwlWindow* pWindow, void (*onEvent)(SwlEvent*))
{
    pWindow->Call.OnEvent = onEvent;
}

void
swl_window_set_above(SwlWindow* pWindow)
{
    SwlX11Backend* pX11Backend = (SwlX11Backend*) pWindow->pBackend;
    Display* pDisplay = pX11Backend->pDisplay;
    Window window = pX11Backend->Window;

    Atom wm_state  =  XInternAtom(pDisplay, "_NET_WM_STATE", False);
    Atom wmStateAbove = XInternAtom(pDisplay, "_NET_WM_STATE_ABOVE", 1 );

#define _NET_WM_STATE_REMOVE        0    // remove/unset property
#define _NET_WM_STATE_ADD           1    // add/set property
#define _NET_WM_STATE_TOGGLE        2    // toggle property

    XEvent xev = {
	.xclient = {
	    .type = ClientMessage,
	    .window = window,
	    .message_type = wm_state,
	    .format = 32,
	    .data.l = {
		[0] = _NET_WM_STATE_ADD,
		[1] = wmStateAbove,
		[2] = 0,
	    }
	}
    };

    XSendEvent(pDisplay, DefaultRootWindow(pDisplay), False, SubstructureNotifyMask , &xev);
}

void
swl_window_set_maximized(SwlWindow* pWindow)
{
    SwlX11Backend* pX11Backend = (SwlX11Backend*) pWindow->pBackend;
    Display* pDisplay = pX11Backend->pDisplay;
    Window window = pX11Backend->Window;

    Atom wm_state  =  XInternAtom(pDisplay, "_NET_WM_STATE", False);
    Atom max_horz  =  XInternAtom(pDisplay, "_NET_WM_STATE_MAXIMIZED_HORZ", False);
    Atom max_vert  =  XInternAtom(pDisplay, "_NET_WM_STATE_MAXIMIZED_VERT", False);

#define _NET_WM_STATE_REMOVE        0    // remove/unset property
#define _NET_WM_STATE_ADD           1    // add/set property
#define _NET_WM_STATE_TOGGLE        2    // toggle property

    XEvent xev = {
	.xclient = {
	    .type = ClientMessage,
	    .window = window,
	    .message_type = wm_state,
	    .format = 32,
	    .data.l = {
		[0] = _NET_WM_STATE_ADD,
		[1] = max_horz,
		[2] = max_vert,
	    }
	}
    };

    XSendEvent(pDisplay, DefaultRootWindow(pDisplay), False, SubstructureNotifyMask , &xev);
}

void
swl_window_set_fullscreen(SwlWindow* pWindow)
{
    SwlX11Backend* pX11Backend = (SwlX11Backend*) pWindow->pBackend;
    Display* pDisplay = pX11Backend->pDisplay;
    Window window = pX11Backend->Window;

    Atom aWm = XInternAtom(pDisplay, "_NET_WM_STATE", False);
    Atom aFullScreen = XInternAtom(pDisplay, "_NET_WM_STATE_FULLSCREEN", False);

#define _NET_WM_STATE_REMOVE        0    // remove/unset property
#define _NET_WM_STATE_ADD           1    // add/set property
#define _NET_WM_STATE_TOGGLE        2    // toggle property

    XEvent xev = {
	.xclient = {
	    .type = ClientMessage,
	    .window = window,
	    .message_type = aWm,
	    .format = 32,
	    .data.l = {
		[0] = _NET_WM_STATE_ADD,
		[1] = aFullScreen,
	    }
	}
    };

    XSendEvent(pDisplay, DefaultRootWindow(pDisplay), False, SubstructureNotifyMask , &xev);

}

void
swl_window_set_wo_header(SwlWindow* pWindow)
{
    SwlX11Backend* pX11Backend = (SwlX11Backend*) pWindow->pBackend;
    Display* pDisplay = pX11Backend->pDisplay;
    Window window = pX11Backend->Window;

    Atom wmAtom = XInternAtom(pDisplay, "_MOTIF_WM_HINTS", True);

    if (wmAtom != None)
    {
	enum {
	    HintsType_DecorNone = 0,
	    HintsType_Decorations = (1L << 1),
	};

	typedef struct Hints
	{
	    unsigned long   flags;
	    unsigned long   functions;
	    unsigned long   decorations;
	    long            inputMode;
	    unsigned long   status;
	} Hints;

	Hints hints = {
	    .flags = HintsType_Decorations,
	    .functions = 0,
	    .decorations = HintsType_DecorNone,
	    .inputMode = 0,
	    .status = 0
	};

	XChangeProperty(pDisplay, window, wmAtom, wmAtom, 32,
			PropModeReplace,
			(swl_u8*)&hints, 4);
    }

}

void
swl_window_set_background(SwlWindow* pWindow, swl_u8 r255, swl_u8 g255, swl_u8 b255)
{
    SwlX11Backend* pX11Backend = (SwlX11Backend*) pWindow->pBackend;
    Display* pDisplay = pX11Backend->pDisplay;
    Window window = pX11Backend->Window;

    swl_f32 ratio = 65535 / 255.0f;

    XColor color = {
	.red   = ratio * r255, // 1 - 65535, 0 - 0
	.green = ratio * g255,
	.blue  = ratio * b255,
    };

    XAllocColor(pDisplay, DefaultColormap(pDisplay,0), &color);
    XSetWindowBackground(pDisplay, window, color.pixel);

    /* XColor color; */
    /* char green[] = "#00FF00"; */
    /* Colormap colormap = DefaultColormap(pDisplay, 0); */
    /* XParseColor(pDisplay, colormap, green, &color); */
    /* XAllocColor(pDisplay, colormap, &color); */

}

#endif // SWL_X11_BACKEND == 1

#endif // SWL_IMPLEMENTATION



#endif // SIMPLE_WINDOW_LIBRARY_H
