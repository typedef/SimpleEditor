#!/bin/bash

cd ..

clang -std=c99 src/main.c src/MyLibDep.c Dependencies/raylib-5.0_linux_amd64/lib/libraylib.a -IDependencies/raylib-5.0_linux_amd64/include -D_GNU_SOURCE -lm -lglfw -lGL -lX11 -o bin/app

bin/app
